---
title: LeetCode 1~100
date: 2019-02-15 20:45:40.923 -0700
categories: [LeetCode]
tags: [leetcode]
---

LeetCode Algorithm 1~100

<!--more-->

# 1. Two Sum (easy)

Given an array of integers, return **indices** of the two numbers such that they add up to a specific target.

You may assume that each input would have **exactly** one solution, and you may not use the *same* element twice.

**Example:**

```
Given nums = [2, 7, 11, 15], target = 9,

Because nums[0] + nums[1] = 2 + 7 = 9,
return [0, 1].
```



```java
class Solution {
    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int complement = target - nums[i];
            if (map.containsKey(complement)) {
                return new int[] { map.get(complement), i };
            }
            map.put(nums[i], i);
        }
        throw new IllegalArgumentException("No two sum solution");
    }
}
```



---



# 2. Add Two Numbers (medium)

You are given two **non-empty** linked lists representing two non-negative integers. The digits are stored in **reverse order** and each of their nodes contain a single digit. Add the two numbers and return it as a linked list.

You may assume the two numbers do not contain any leading zero, except the number 0 itself.

**Example:**

```
Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
Output: 7 -> 0 -> 8
Explanation: 342 + 465 = 807.
```



- 如果允许修改 L1 的话，可以做到 O(1) 的 space complexity
- 做两个 pass ，第一个 pass 直接把 L1 和 L2 对应的元素相加，不考虑进位（注意两个 List 的长度不一定相同）；第二个 pass 只做进位（注意最后一个元素的进位要多加一个 ListNode）

```java
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode p1 = l1;
        ListNode p2 = l2;
        ListNode pre1 = null;
        while (p1 != null && p2 != null) {
            p1.val += p2.val;
            pre1 = p1;
            p1 = p1.next;
            p2 = p2.next;
        }
        if (p2 != null) {
            if (pre1 != null) {
                pre2.next = p2;
            }
            else {
                l1 = p2;
            }
        }
        p1 = l1;
        while (p1 != null) {
            if (p1.val > 9) {
                p1.val -= 10;
                if (p1.next != null) {
                    p1.next.val += 1;
                }
                else {
                    p1.next = new ListNode(1);
                }
            }
            p1 = p1.next;
        }
        return l1;
    }
}
```



---



# 3. Longest Substring Without Repeating Characters (medium, 二刷)

Given a string, find the length of the **longest substring** without repeating characters.

**Example 1:**

```
Input: "abcabcbb"
Output: 3 
Explanation: The answer is "abc", with the length of 3. 
```

**Example 2:**

```
Input: "bbbbb"
Output: 1
Explanation: The answer is "b", with the length of 1.
```

**Example 3:**

```
Input: "pwwkew"
Output: 3
Explanation: The answer is "wke", with the length of 3. 
             Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
```



- 不需要 remove key from map，只需通过 **"winStart = Math.max(winStart, map.get(s.charAt(i)) + 1)"** 来保证 winStart 这个指针一直往后走

```java
class Solution {
    public int lengthOfLongestSubstring(String s) {
        int winStart = 0;
        int maxLen = 0;
        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            if (map.containsKey(s.charAt(i))) {
                maxLen = Math.max(maxLen, i - winStart);
                winStart = Math.max(winStart, map.get(s.charAt(i)) + 1);
            }
            map.put(s.charAt(i), i);
        }
        maxLen = Math.max(maxLen, s.length() - winStart);
        return maxLen;
    }
}
```



---



# 94. Binary Tree Inorder Traversal (medium)

Given a binary tree, return the *inorder* traversal of its nodes' values.

**Example:**

```
Input: [1,null,2,3]
   1
    \
     2
    /
   3

Output: [1,3,2]
```

```java
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    List<Integer> res = new LinkedList<>();
    
    public List<Integer> inorderTraversal(TreeNode root) {
        traverse(root);
        return res;
    }
    
    private void traverse(TreeNode root) {
        if (root == null) return;
        traverse(root.left);
        res.add(root.val);
        traverse(root.right);
    }
}
```

**Follow up:** Recursive solution is trivial, could you do it iteratively?

### Note:

- 递归转非递归
    - 用stack来模拟recursion时的栈。因为recursive function的arg是TreeNode，所以用了Stack\<TreeNode\>。
    - 结束recursion的要求有两个。一个是当前函数return了（最后一句traverse(root.right)必定使recursion加深一层），也即root==null；另一个是recursion深度为0，也即栈空了。所以iterative的第一个while循环，结束条件是"root==null && st.empty()"
    - recursive func里面，如果root!=null，则到第二句，recursion加深一层，即当前状态入栈，然后当前的root更新为root.left。这个地方，本来是若root不为空，跳过第一句，然后加深一层recursion，回到第一个while循环的入口，但这里做了一个优化
    - 如果root==null，则退栈，root=st.pop()
    - 退栈之后，说明"traverse(root.left)"已经运行完了，执行res.add(root.val)，再进入traverse(root.right)
- Stack\<E\>
    - Stack是Vector的子类，在java.util.Stack
    - empty()检查是否为空（Vector的接口和List不一样），peek()查看，push(e)和pop()
    - search(e)返回1-based position

```java
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> res = new LinkedList<>();
        Stack<TreeNode> st = new Stack<>();
        while (root != null || ! st.empty()) {
            while (root != null) {
                st.push(root);
                root = root.left;
            }
            root = st.pop();
            res.add(root.val);
            root = root.right;
        }
        return res;
    }
}
```

没有优化之前，可以比照有recursion和没有recursion的版本：

```java
class Solution {
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> res = new LinkedList<>();
        Stack<TreeNode> st = new Stack<>();
        while(root != null || ! st.isEmpty()) {
            if(root != null) {
                st.push(root);
                root = root.left;
            } else {
                root = st.pop();
                res.add(root.val);
                root = root.right;   
            }
        }
        return res;
    }
}
```

