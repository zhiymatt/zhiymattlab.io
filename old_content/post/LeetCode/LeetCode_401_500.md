---
title: LeetCode 401~500
date: 2019-02-15 20:45:44.923 -0700
categories: [LeetCode]
tags: [leetcode]
---

LeetCode Algorithm 401~500

<!--more-->

# 496. Next Greater Element I (easy) (二刷)

You are given two arrays **(without duplicates)** `nums1` and `nums2` where `nums1`’s elements are subset of `nums2`. Find all the next greater numbers for `nums1`'s elements in the corresponding places of `nums2`.  

The Next Greater Number of a number **x** in `nums1` is the first greater number to its right in `nums2`. If it does not exist, output -1 for this number. 

**Example 1:**

```
Input: nums1 = [4,1,2], nums2 = [1,3,4,2].
Output: [-1,3,-1]
Explanation:
    For number 4 in the first array, you cannot find the next greater number for it in the second array, so output -1.
    For number 1 in the first array, the next greater number for it in the second array is 3.
    For number 2 in the first array, there is no next greater number for it in the second array, so output -1.
```

**Example 2:**

```
Input: nums1 = [2,4], nums2 = [1,2,3,4].
Output: [3,-1]
Explanation:
    For number 2 in the first array, the next greater number for it in the second array is 3.
    For number 4 in the first array, there is no next greater number for it in the second array, so output -1.
```

**Note:**

1. All elements in `nums1` and `nums2` are unique.
2. The length of both `nums1` and `nums2` would not exceed 1000.

### Note:

Key observation:

Suppose we have a decreasing sequence followed by a greater number.

For example `[5, 4, 3, 2, 1, 6]` then the greater number `6` is the next greater element for all previous numbers in the sequence.

```java
class Solution {
    public int[] nextGreaterElement(int[] nums1, int[] nums2) {
        int[] res = new int[nums1.length];
        Stack<Integer> st = new Stack<>();
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums2.length; i++) {
            while (! st.empty() && nums2[i] > st.peek()) {
                map.put(st.pop(), nums2[i]);
            }
            st.push(nums2[i]);
        }
        for (int i = 0; i < nums1.length; i++) {
            res[i] = map.getOrDefault(nums1[i], -1);
        }
        return res;
    }
}
```



---



# 500. Keyboard Row (easy)

Given a List of words, return the words that can be typed using letters of **alphabet** on only one row's of American keyboard.

**Example:**

```
Input: ["Hello", "Alaska", "Dad", "Peace"]
Output: ["Alaska", "Dad"]
```

### Note:

- "{}" 只能够用来初始化
- Java没有for-else语法，所以需要flag
- 检查空字符串用**if (str.equals("")) {...}**
- List\<String\> to Array of String: **list.toArray(new String[0])**

```java
import java.util.*;

class Solution {
    public String[] findWords(String[] words) {
        String[] keyboardRows = {"qwertyuiop", "asdfghjkl", "zxcvbnm"};
        Map<Character, Integer> rowMap = new HashMap<>();
        for (int i = 0; i < 3; i++) {
            for (char c : keyboardRows[i].toCharArray()) {
                rowMap.put(c, i);
            }
        }
        List<String> results = new ArrayList<String>();
        for (String word : words) {
            String lowerWord = word.toLowerCase();
            if (lowerWord.equals("")) {
                results.add(lowerWord);
            }
            else {
                int r = rowMap.get(lowerWord.charAt(0));
                for (char c : lowerWord.toCharArray()) {
                    if (rowMap.get(c) != r) {
                        r = -1;
                        break;
                    }
                }
                if (r != -1) {
                    results.add(word);
                }
            }
        }
        return results.toArray(new String[0]);
    }
}
```



---


