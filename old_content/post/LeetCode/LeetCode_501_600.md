---
title: LeetCode 501~600
date: 2019-02-15 20:45:45.923 -0700
categories: [LeetCode]
tags: [leetcode]
---

LeetCode Algorithm 501~600

<!--more-->

# 501. Find Mode in Binary Search Tree (easy) [follow up]

Given a binary search tree (BST) with duplicates, find all the [mode(s)](https://en.wikipedia.org/wiki/Mode_(statistics)) (the most frequently occurred element) in the given BST.

Assume a BST is defined as follows:

- The left subtree of a node contains only nodes with keys **less than or equal to** the node's key.
- The right subtree of a node contains only nodes with keys **greater than or equal to** the node's key.
- Both the left and right subtrees must also be binary search trees.

 For example:
 Given BST `[1,null,2,2]`,

```
   1
    \
     2
    /
   2
```

return `[2]`.

**Note:** If a tree has more than one mode, you can return them in any order.

**Follow up:** Could you do that without using any extra space?  (Assume that the implicit stack space incurred due to recursion does not  count).

### Note:

- 常用的变量作为class member
- for循环map里的元素
    - **for (Map.Entry<xxx, xxx> e : map.entrySet()) {...}**, 用**e.getKey()**和**e.getValue()**来取key和value
    - **for (xxx k : map.keySet()) {...map.get(k)...}**
- 把List of Integer转为Array of int，需要用循环，先定义个固定长度的int array，再用循环放元素
- **list.size()**，因为做了封装
- "!"表示not
- **Math.max**只支持两个元素之间做比较
- class里的method引用用一个class的其它method不需要用**self.xxx**，和Python不一样
- 声明class member的时候可以用new语句对object member初始化；另外，即使java默认会对class member做初始化，最好还是手动明确地做一个初始化
- **Integer.MIN_VALUE**
- 返回空int数组：**return new int[0]**
- 对Integer数组不能赋值给int数组
- 创建一个int数组：**new int[len]**，没有constructor

```java
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    Map<Integer, Integer> count = new HashMap<>();
    
    int maxCount = 0;
    
    public int[] findMode(TreeNode root) {
        traverse(root);
        List<Integer> list = new ArrayList<>();
        for (int k : count.keySet()) {
            if (count.get(k) == maxCount) {
                list.add(k);
            }
        }
        int[] res = new int[list.size()];
        for (int i = 0; i < list.size(); ++i) {
            res[i] = list.get(i);
        }
        return res;
    }
    
    private void traverse(TreeNode root) {
        if (root != null) {
            if (! count.containsKey(root.val)) {
                count.put(root.val, 1);
            }
            else {
                count.put(root.val, count.get(root.val) + 1);
            }
            maxCount = Math.max(maxCount, count.get(root.val));
            traverse(root.left);
            traverse(root.right);
        }
    }
}
```

Follow-up solution：

```java
class Solution {
    int[] res = null;
    int prevVal = Integer.MIN_VALUE;
    int currentCount = 0;
    int maxCount = 0;
    int resInd = 0;
    
    public int[] findMode(TreeNode root) {
        if (root == null) {
            return new int[0];
        }
        inOrderTraverse(root);
        res = new int[resInd];
        currentCount = 0;
        prevVal = Integer.MIN_VALUE;
        resInd = 0;
        inOrderTraverse(root);
        return res;
    }
    
    private void inOrderTraverse(TreeNode root) {
        if (root.left != null) {
            inOrderTraverse(root.left);
        }
        if (prevVal == root.val) {
            currentCount++;
        }
        else {
            currentCount = 1;
            prevVal = root.val;
        }
        if (currentCount == maxCount) {
            if (res != null) {
                res[resInd] = root.val;
            }
            resInd++;
        }
        else if (currentCount > maxCount) {
            if (res != null) {
                res[0] = root.val;
            }
            resInd = 1;
            maxCount = currentCount;
        }
        if (root.right != null) {
            inOrderTraverse(root.right);
        }
    }
}
```



---



# 504. Base 7 (easy)

Given an integer, return its base 7 string representation.

**Example 1:**

```
Input: 100
Output: "202"
```

**Example 2:**

```
Input: -7
Output: "-10"
```

**Note:** The input will be in range of [-1e7, 1e7]. 

### Note

- 100 = 2 * 7 ^ 2 + 0 * 7 ^ 1 + 2 * 7 ^ 0 = 2 + 7 * (0 + 7 * (2))
- StringBuilder的用法（append, reverse, toString）
- integer转string，用Integer.toString(num)，不是String.toString(num)
- primitive type: **"boolean"**, not "bool"

```java
class Solution {
    public String convertToBase7(int num) {
        StringBuilder sb = new StringBuilder();
        boolean sign = true;
        if (num == 0) {
            return "0";
        }
        else if (num < 0) {
            num = -num;
            sign = false;
        }
        while (num != 0) {
            sb.append(Integer.toString(num % 7));
            num /= 7;
        }
        if (! sign) {
            sb.append("-");
        }
        return sb.reverse().toString();
    }
}
```



---



# 506. Relative Ranks (easy) (marked)

 Given scores of **N** athletes, find their relative ranks and the  people with the top three highest scores, who will be awarded medals:  "Gold Medal", "Silver Medal" and "Bronze Medal".

**Example 1:**

```
Input: [5, 4, 3, 2, 1]
Output: ["Gold Medal", "Silver Medal", "Bronze Medal", "4", "5"]
Explanation: The first three athletes got the top three highest scores, so they got "Gold Medal", "Silver Medal" and "Bronze Medal". 
For the left two athletes, you just need to output their relative ranks according to their scores.
```

**Note:**

1. N is a positive integer and won't exceed 10,000.
2. All the scores of athletes are guaranteed to be unique.

### Note:

- 因为score是unique的，所以可以用score --> ranking来求每个athlete的ranking
- sort [(score, index from 0 to ~)]来求每一个score的ranking
- **java.util.Arrays**有4类sorting：
    - **sort(T[] a)**, sort into ascending order
    - **sort(T[] a, int fromIndex, int toIndex)**, sort subarray
    - **sort(T[] a, Comparator<? super T> c)**
    - **sort(T[] a, int fromIndex, int toIndex, Comparator<? super T> c)**
- Comparator: negative int means o1 less than o2, zero means o1 equal to o2, positive int means o1 greater than o2. Ascending order的话，直接**return o1 - o2**
- Java 8引入的Lambda函数：**input -> body**，多个input用小括号。可省略类型**(x, y) -> (...)**，可以不省略**(int x, int y) -> (...)**

```java
class Solution {
    public String[] findRelativeRanks(int[] nums) {
        // [][0] score, [][1] original index; we want score -> rank mapping
        int[][] tmp = new int[nums.length][2];
        for (int i = 0; i < nums.length; i++) {
            tmp[i][0] = nums[i];
            tmp[i][1] = i;
        }
        String[] res = new String[nums.length];
        Arrays.sort(tmp, (x, y) -> (y[0] - x[0]));
        // iterate based on ranking
        for (int i = 0; i < nums.length; i++) {
            if (i == 0) {
                res[tmp[i][1]] = "Gold Medal";
            }
            else if (i == 1) {
                res[tmp[i][1]] = "Silver Medal";
            }
            else if (i == 2) {
                res[tmp[i][1]] = "Bronze Medal";
            }
            else {
                res[tmp[i][1]] = Integer.toString(i + 1);
            }
        }
        return res;
    }
}
```



---



# 507. Perfect Number (easy)

We define the Perfect Number is a **positive** integer that is equal to the sum of all its **positive** divisors except itself.  

Now, given an integer n, write a function that returns true when it is a perfect number and false when it is not. 

**Example:**

```
Input: 28
Output: True
Explanation: 28 = 1 + 2 + 4 + 7 + 14
```

**Note:** The input number **n** will not exceed 100,000,000. (1e8) 

### Note:

- iterate [2, sqrt(num)]，acc += 除数和商；1另外最后处理，完全平方数用if语句剔除。corner case 0和1直接用条件判断剔除

```java
class Solution {
    public boolean checkPerfectNumber(int num) {
        int acc = 0;
        for (int i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) {
                acc += i;
                if (i != num / i) {
                    acc += num / i;
                }
            }
        }
        acc++;
        // 0 is positive???
        return (num > 1 && acc == num);
    }
}
```



---



# 508. Most Frequent Subtree Sum (medium)

Given the root of a tree, you are asked to find the most frequent  subtree sum. The subtree sum of a node is defined as the sum of all the  node values formed by the subtree rooted at that node (including the  node itself). So what is the most frequent subtree sum value? If there  is a tie, return all the values with the highest frequency in any order. 

**Examples 1**
 Input: 

```
  5
 /  \
2   -3
```

 return [2, -3, 4], since all the values happen only once, return all of them in any order. 

**Examples 2**
 Input: 

```
  5
 /  \
2   -5
```

 return [2], since 2 happens twice, however -5 only occur once. 

**Note:** You may assume the sum of values in any subtree is in the range of 32-bit signed integer. 

### Note:

- Map有getOrDefault(key, defaultVal) method，相当于map.containsKey(key) ? map.get(key) : defaultVal

```java
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    Map<Integer, Integer> counter = new HashMap<>();
    int maxFreq = -1;
    int maxFreqCount = 0;
    
    public int[] findFrequentTreeSum(TreeNode root) {
        if (root == null) {
            return new int[0];
        }
        treeSum(root);
        int[] res = new int[maxFreqCount];
        int ind = 0;
        for (Integer k : counter.keySet()) {
            if (counter.get(k) == maxFreq) {
                res[ind++] = k;
            }
        }
        return res;
    }
    
    private int treeSum(TreeNode root) {
        int sum = root.val;
        if (root.left != null) sum += treeSum(root.left);
        if (root.right != null) sum += treeSum(root.right);
        int count = counter.getOrDefault(sum, 0) + 1;
        counter.put(sum, count);
        if (count > maxFreq) {
            maxFreq = count;
            maxFreqCount = 1;
        }
        else if (count == maxFreq) {
            maxFreqCount++;
        }
        return sum;
    }
}
```



---



# 509. Fibonacci Number (easy)

The **Fibonacci numbers**, commonly denoted `F(n)` form a sequence, called the **Fibonacci sequence**, such that each number is the sum of the two preceding ones, starting from `0` and `1`. That is,

```
F(0) = 0,   F(1) = 1
F(N) = F(N - 1) + F(N - 2), for N > 1.
```

Given `N`, calculate `F(N)`.

```java
class Solution {
    public int fib(int N) {
        if (N == 0) return 0;
        int a = 0;
        int b = 1;
        for (int i = 1; i < N; i++) {
            int preB = b;
            b = a + b;
            a = preB;
        }
        return b;
    }
}
```

