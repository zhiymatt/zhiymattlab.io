---
title: LeetCode 101~200
date: 2019-02-15 20:45:41.923 -0700
categories: [LeetCode]
tags: [leetcode]
---

LeetCode Algorithm 101~200

<!--more-->



# 102. Binary Tree Level Order Traversal (medium)

Given a binary tree, return the *level order* traversal of its nodes' values. (ie, from left to right, level by level).

For example:
Given binary tree `[3,9,20,null,null,15,7]`,

```
    3
   / \
  9  20
    /  \
   15   7
```

return its level order traversal as:

```
[
  [3],
  [9,20],
  [15,7]
]
```

### Note:

- **List<List<Integer>>**声明的时候要写到最里面
- Interface **Queue<E>**
    - 可以用LinkedList、PriorityQueue来实现
    - offer(e), peek(), poll()分别对应add(e), element(), remove()
    - offer/peek/poll当为空的时候会返回false或者null，而add/element/remove会抛出一个异常
    - 从java.utils.Collection继承了isEmpty, hashCode, contains, addAll, clear, toArray这些methods
- 每到新的一层，先记住当前层的元素个数，用于for循环(配合poll)，这样就可以只用一个queue

```java
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> res = new LinkedList<>();
        if (root == null) {
            return res;
        }
        Queue<TreeNode> q = new LinkedList<>();
        q.offer(root);
        while (! q.isEmpty()) {
            int levelLen = q.size();
            List<Integer> levelRes = new LinkedList<>();
            for (int i = 0; i < levelLen; i++) {
                TreeNode node = q.poll();
                levelRes.add(node.val);
                if (node.left != null) q.add(node.left);
                if (node.right != null) q.add(node.right);
            }
            res.add(levelRes);
        }
        return res;
    }
}
```



---



# 126. Word Ladder II (hard) (二刷)

Given two words (*beginWord* and *endWord*), and a dictionary's word list, find all shortest transformation sequence(s) from *beginWord* to *endWord*, such that:

1. Only one letter can be changed at a time
2. Each transformed word must exist in the word list. Note that *beginWord* is *not* a transformed word.

**Note:**

- Return an empty list if there is no such transformation sequence.
- All words have the same length.
- All words contain only lowercase alphabetic characters.
- You may assume no duplicates in the word list.
- You may assume *beginWord* and *endWord* are non-empty and are not the same.

**Example 1:**

```
Input:
beginWord = "hit",
endWord = "cog",
wordList = ["hot","dot","dog","lot","log","cog"]

Output:
[
  ["hit","hot","dot","dog","cog"],
  ["hit","hot","lot","log","cog"]
]
```

**Example 2:**

```
Input:
beginWord = "hit"
endWord = "cog"
wordList = ["hot","dot","dog","lot","log"]

Output: []

Explanation: The endWord "cog" is not in wordList, therefore no possible transformation.
```

 ### Note:

- 用collections来initialize另一个collections，例如**List gfg = new ArrayList<>(collection);**
- 可以用Arrays.asList和List.Of来帮忙初始化一个list，但是这种方法需要多创建一个新的list，所以最好的方法还是创建list之后一个个元素地添加
- array用.length，String用.length()，collections用.size()
- 代码里用了newVisited和ifCont，因为要找到所有的最短路径
- 这里的backtracing技巧经常用到。尽量用递归，因为可以简化很多。如果用非递归的话，加入neighbors**之前**，插入一个特殊符号**#**，用来表示某一个节点搜索完了

```java
class Solution {
    public List<List<String>> findLadders(String beginWord, String endWord, List<String> wordList) {
        List<List<String>> res = new LinkedList<>();
        Set<String> wordSet = new HashSet<>(wordList);
        if (! wordSet.contains(endWord)) {
            return res;
        }
        wordSet.remove(beginWord);
        int wordLen = beginWord.length();
        
        Set<String> beginSet = new HashSet<>();
        beginSet.add(beginWord);
        Set<String> endSet = new HashSet<>();
        endSet.add(endWord);
        Set<String> visited = new HashSet<>();
        visited.add(endWord);
        
        Map<String, List<String>> adjList = new HashMap<>();
        
        boolean isForward = true;
        boolean ifCont = true;
        
        while (ifCont) {
            if (beginSet.isEmpty()) {
                return res;
            }
            Set<String> newBeginSet = new HashSet<>();
            Set<String> newVisited = new HashSet<>();
            for (String bw : beginSet) {
                char[] bwca = bw.toCharArray();
                for (char c = 'a'; c <= 'z'; c++) {
                    for (int i = 0; i < wordLen; i++) {
                        char oldC = bwca[i];
                        bwca[i] = c;
                        String nextWord = String.valueOf(bwca);
                        if (wordSet.contains(nextWord)) {
                            if (endSet.contains(nextWord) || ! visited.contains(nextWord)) {
                                String from = isForward ? bw : nextWord;
                                String to = isForward ? nextWord : bw;
                                if (! adjList.containsKey(from)) {
                                    adjList.put(from, new LinkedList<String>());
                                }
                                adjList.get(from).add(to);
                            }
                            if (endSet.contains(nextWord)) {
                                ifCont = false;
                            }
                            if (! visited.contains(nextWord)) {
                                newVisited.add(nextWord);
                                newBeginSet.add(nextWord);
                            }
                        }
                        bwca[i] = oldC;
                    }
                }
            }
            beginSet = newBeginSet;
            visited.addAll(newVisited);
            if (beginSet.size() > endSet.size()) {
                Set<String> tmp = beginSet;
                beginSet = endSet;
                endSet = tmp;
                isForward = ! isForward;
            }
        }
        
        Stack<String> st = new Stack<>();
        st.add(beginWord);
        Stack<String> path = new Stack<>();
        while (! st.empty()) {
            String currentWord = st.pop();
            if (currentWord.equals("#")) {
                path.pop();
                continue;
            }
            path.push(currentWord);
            if (currentWord.equals(endWord)) {
                res.add(new ArrayList<String>(path));
            }
            st.push(String.valueOf("#"));
            if (adjList.containsKey(currentWord)) {
                for (String nextWord : adjList.get(currentWord)) {
                    st.push(nextWord);
                }
            }
        }
        return res;
    }
}
```



---



# 127. Word Ladder (medium)

Given two words (*beginWord* and *endWord*), and a dictionary's word list, find the length of shortest transformation sequence from *beginWord* to *endWord*, such that:

1. Only one letter can be changed at a time.
2. Each transformed word must exist in the word list. Note that *beginWord* is *not* a transformed word.

**Note:**

- Return 0 if there is no such transformation sequence.
- All words have the same length.
- All words contain only lowercase alphabetic characters.
- You may assume no duplicates in the word list.
- You may assume *beginWord* and *endWord* are non-empty and are not the same.

**Example 1:**

```
Input:
beginWord = "hit",
endWord = "cog",
wordList = ["hot","dot","dog","lot","log","cog"]

Output: 5

Explanation: As one shortest transformation is "hit" -> "hot" -> "dot" -> "dog" -> "cog", return its length 5.
```

**Example 2:**

```
Input:
beginWord = "hit"
endWord = "cog"
wordList = ["hot","dot","dog","lot","log"]

Output: 0

Explanation: The endWord "cog" is not in wordList, therefore no possible transformation.
```

 ### Note:

- public static boolean disjoint(Collection\<?\> c1, Collection\<?\> c2) 用来判断两个collections有没有相交
- 尽量用String的charAt。用toCharArray的话会创建一个新的char array
- String的长度用 .length()
- 这里用了双向广搜
- 这里的visited的candidates只包括wordList
- 注意看这里，用c from 'a' to 'z'避免了求set intersection。做这种跟string的题目的时候，尽量引入常数系数
- 改变String的一个char：先创建一个charArray，改变某个char，再通过String.valueOf(newCharArray)来生成一个新的String

```java
class Solution {
    public int ladderLength(String beginWord, String endWord, List<String> wordList) {
        Set<String> wordSet = new HashSet<>();
        for (String str : wordList) {
            wordSet.add(str);
        }
        if (! wordSet.contains(endWord)) {
            return 0;
        }
        
        Set<String> visited = new HashSet<>();
        Set<String> beginSet = new HashSet<>();
        Set<String> endSet = new HashSet<>();
        beginSet.add(beginWord);
        endSet.add(endWord);
        visited.add(endWord);
        int wordLen = beginWord.length();
        
        int res = 1;
        
        while (true) {
            if (beginSet.isEmpty()) {
                return 0;
            }
            Set<String> newBeginSet = new HashSet<>();
            for (String bWord : beginSet) {
                char[] bWordCA = bWord.toCharArray();
                for (char c = 'a'; c <= 'z'; c++) {
                    for (int i = 0; i < wordLen; i++) {
                        char oldC = bWordCA[i];
                        bWordCA[i] = c;
                        String newBWord = String.valueOf(bWordCA);
                        if (wordSet.contains(newBWord)) {
                            if (endSet.contains(newBWord)) {
                                return res + 1;
                            }
                            if (! visited.contains(newBWord)) {
                                newBeginSet.add(newBWord);
                                visited.add(newBWord);
                            }
                        }
                        bWordCA[i] = oldC;
                    }
                }
            }
            beginSet = newBeginSet;
            if (beginSet.size() > endSet.size()) {
                Set<String> tmp = beginSet;
                beginSet = endSet;
                endSet = tmp;
            }
            res++;
        }
    }
}
```



---



# 133. Clone Graph (medium) (二刷)

Given the head of a graph, return a deep copy (clone) of the graph. Each node in the graph contains a `label` (`int`) and a list (`List[UndirectedGraphNode]`) of its `neighbors`. There is an edge between the given node and each of the nodes in its neighbors.

OJ's undirected graph serialization (so you can understand error output):

Nodes are labeled uniquely.

As an example, consider the serialized graph `{0,1,2#1,2#2,2}`.

The graph has a total of three nodes, and therefore contains three parts as separated by `#`.

1. First node is labeled as `0`. Connect node `0` to both nodes `1` and `2`.
2. Second node is labeled as `1`. Connect node `1` to node `2`.
3. Third node is labeled as `2`. Connect node `2` to node `2` (itself), thus forming a self-cycle.

Visually, the graph looks like the following:

```
       1
      / \
     /   \
    0 --- 2
         / \
         \_/
```

### Node:

- Object有一个hashCode() method，所以Object都是hashable的
- 注意这里的两个if语句都放在函数入口处，这样可以简化代码（尽量采取这种写法）
- 这里有个坑，graph可以有self-cycle，所以不能简单地用一个visited的set来避免dfs访问之前访问过的节点
- 应该用一个HashMap<label, UndirectedGraphNode>。如果containsKey，这就说明之前访问过这个节点。因为这里是要clone，所以不是像普通的dfs一样，看到访问过的点就不继续搜索，而是要把当做一个已经解决过的子问题（像search的题目一样）来处理
- 注意这里HashMap的key是Integer，而不是UndirectedGraphNode，要不然还要分是origin还是clone

Recursive DFS：

```java
/**
 * Definition for undirected graph.
 * class UndirectedGraphNode {
 *     int label;
 *     List<UndirectedGraphNode> neighbors;
 *     UndirectedGraphNode(int x) { label = x; neighbors = new ArrayList<UndirectedGraphNode>(); }
 * };
 */
public class Solution {
    Map<Integer, UndirectedGraphNode> map = new HashMap<>();
    
    public UndirectedGraphNode cloneGraph(UndirectedGraphNode node) {    
        if (node == null) {
            return null;
        }
        if (map.containsKey(node.label)) {
            return map.get(node.label);
        }
        else {
            UndirectedGraphNode clone = new UndirectedGraphNode(node.label);
            map.put(clone.label, clone);
            for (UndirectedGraphNode nei : node.neighbors) {
                clone.neighbors.add(cloneGraph(nei));
            }
            return clone;
        }
    }
}
```

Iterative BFS:

```java
/**
 * Definition for undirected graph.
 * class UndirectedGraphNode {
 *     int label;
 *     List<UndirectedGraphNode> neighbors;
 *     UndirectedGraphNode(int x) { label = x; neighbors = new ArrayList<UndirectedGraphNode>(); }
 * };
 */
public class Solution {
    public UndirectedGraphNode cloneGraph(UndirectedGraphNode node) {
        if (node == null) {
            return null;
        }
        
        Map<Integer, UndirectedGraphNode> map = new HashMap<>();
        Queue<UndirectedGraphNode> queue = new LinkedList<>();
        
        UndirectedGraphNode root = new UndirectedGraphNode(node.label);
        map.put(root.label, root);
        
        queue.offer(node);
        
        while (! queue.isEmpty()) {
            node = queue.poll();
            for (UndirectedGraphNode nei : node.neighbors) {
                if (! map.containsKey(nei.label)) {
                    queue.offer(nei);
                    map.put(nei.label, new UndirectedGraphNode(nei.label));
                }
                map.get(node.label).neighbors.add(map.get(nei.label));
            }
        }
        return root;
    }
}
```



---



# 144. Binary Tree Preorder Traversal (medium)

Given a binary tree, return the *preorder* traversal of its nodes' values.

**Example:**

```
Input: [1,null,2,3]
   1
    \
     2
    /
   3

Output: [1,2,3]
```

```java
...
```

**Follow up:** Recursive solution is trivial, could you do it iteratively?

### Note:

- 和LeetCode 94 Inorder Traversal一样地分析
- iterative的解法和LeetCode 94一样，只是"res.add(root.val)"这句换了位置

```java
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> res = new LinkedList<>();
        Stack<TreeNode> st = new Stack<>();
        while (root != null || ! st.empty()) {
            while (root != null) {
                res.add(root.val);
                st.push(root);
                root = root.left;
            }
            root = st.pop().right;
        }
        return res;
    }
}
```



---



# 145. Binary Tree Postorder Traversal (hard)

Given a binary tree, return the *postorder* traversal of its nodes' values.

**Example:**

```
Input: [1,null,2,3]
   1
    \
     2
    /
   3

Output: [3,2,1]
```

**Follow up:** Recursive solution is trivial, could you do it iteratively?

### Note:

- Postorder其实是和preorder反过来（看Leetcode 144. Preorder Traversal）：
    - res.addFirst而不是res.add
    - left和right的遍历顺序反过来
- 只有LinkedList实现了addFirst，List没有addFirst，所以reference要是LinkedList类型的

```java
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public List<Integer> postorderTraversal(TreeNode root) {
        LinkedList<Integer> res = new LinkedList<>();
        Stack<TreeNode> st = new Stack<>();
        while (root != null || ! st.empty()) {
            while (root != null) {
                res.addFirst(root.val);
                st.push(root);
                root = root.right;
            }
            root = st.pop();
            root = root.left;
        }
        return res;   
    }
}
```

