---
title: Cmder Configuration
date: 2018-12-11 13:08:40.923 -0700
categories: [Windows]
tags: [windows, wsl, cmder]
---

Config Cmder [Windows 10] for WSL and Powershell

<!--more-->

# Git Bash  on Windows

```
"C:\Program Files\Git\bin\sh.exe" --login -i
```

# WSL with zsh

```
%windir%\system32\bash.exe ~ -c zsh -cur_console:p5m:/mnt
```