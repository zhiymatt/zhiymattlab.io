---
title: Flink Tutorial (2)
date: 2018-12-24 00:08:40.923 -0700
categories: [Flink]
tags: [flink]
---

Flink Tutorial (2): Doc-Links.

<!--more-->

---

### Concepts 
-   Programming Model

	https://ci.apache.org/projects/flink/flink-docs-release-1.7/concepts/programming-model.html
-   Distributed Runtime

	https://ci.apache.org/projects/flink/flink-docs-release-1.7/concepts/runtime.html
	
### Tutorial
-   API Tutorials
	-   DataStream API

		https://ci.apache.org/projects/flink/flink-docs-release-1.7/tutorials/datastream_api.html
		
		ps:
		1. [Maven Tutorial](http://www.runoob.com/maven/maven-tutorial.html)
        2. The flink-clients dependency is only necessary to invoke the Flink program locally (for example to run it standalone for testing and debugging). If you intend to only export the program as a JAR file and run it on a cluster, you can skip that dependency.
        3.  Fold【KeyedStream → DataStream: 一个有初始值的分组数据流的滚动折叠操作. 合并当前元素和前一次折叠操作的结果，并产生一个新的值.】Fold操作不断更新并产生新数据。

        	下面的fold函数就是当我们输入一个 (1,2,3,4,5)的序列, 将会产生一下面的句子:"start-1", "start-1-2", "start-1-2-3", ...
        	
			```java
			val result: DataStream[String] = 
				keyedStream.fold("start")((str, i) => { str + "-" + i })
			```
-   Setup Tutorials
	-   Local Setup

		https://ci.apache.org/projects/flink/flink-docs-release-1.7/tutorials/local_setup.html
		
	-   Running Flink on Windows

		https://ci.apache.org/projects/flink/flink-docs-release-1.7/tutorials/flink_on_windows.html

-   Examples

	https://ci.apache.org/projects/flink/flink-docs-release-1.7/examples/