---
title: Shell Command
date: 2018-11-22 23:08:40.923 -0700
categories: [Shell]
tags: [shell]
---

输出时间； 查找

<!--more-->

输出当前时间：

```sh
echo $(date '+%Y %b %d %H:%M')
```

recursively在所有文件中查找某个pattern：

```sh
grep -rnw '/path/to/somewhere/' -e 'pattern'
```