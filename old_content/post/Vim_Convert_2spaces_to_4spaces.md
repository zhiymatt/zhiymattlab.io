---
title: "Vim: Convert from 2 spaces to 4 spaces"
date: 2019-01-30 10:11:40.923 -0700
categories: [Vim]
tags: [vim]
---

A general way of changing the indent is by changing the tabstop.

<!--more-->

Paste your file into an empty buffer, then:

```
:set ts=2 sts=2 noet
:retab!
```

This changes every 2 spaces to a TAB character, then:

```
:set ts=4 sts=4 et
:retab
```

This changes every TAB to 4 spaces.

The advantage of this method is that you can also use it the other way around, to convert from 4 to 2 spaces for example.