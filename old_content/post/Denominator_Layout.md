---
title: Denominator Layout
date: 2018-12-11 10:02:40.923 -0700
categories: [ML]
tags: [ml]
---

Layout for matrix calculus. There are two layout conventions: Numerator-layout notation & Denominator-layout notation

<!--more-->

<img src="/image/denominator_layout.png" style="width:100%" class="leftalign">
