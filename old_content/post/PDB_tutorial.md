---
title: pdb -- interactive source code debugger for Python.
date: 2018-10-29 22:02:40.923 -0700
categories: [Python]
tags: [python, pdb, debug]
---

用pdb来debug python程序



<!--more-->



# 启动pdb，并以单步调试的方式运行

  ```python
  python -m pdb err.py
  ```
  -m mod : run library module as a script (terminates option list)



# debugger的一些常用命令：

  **h(elp) [command]**：

  ​	无参数时，列出所有的command；"h pdb"给出完成的document；"h some_cmd"给出某个命令的文档。



  **w(here)**：

  ​	打印stack trace。



  **b(reak) [([filename:]lineno | function) [, condition]]**：

  ​	创建一个breakpoint，在filename的lineno或者function入口处；file在sys.path里找。



  **tbreak [([filename:]lineno | function) [, condition]]**：

  ​	temporary breakpoint。



  **cl(ear) [filename:lineno | bpnumber [bpnumber ...]]**：

  ​	清除某处breakpoint；不给出参数的话，就是清楚所有的断点。



  **disable [bpnumber [bpnumber ...]]**：

  ​	disable一系列断点，参数是变长参数。



  **enable [bpnumber [bpnumber ...]]**：

  ​	略。



  **condition bpnumber [condition]**：

  ​	给在bpnumber的断点设置新的condition；如果不给出condition的话，就是清楚这个断点的condition的意思。



  **commands [bpnumber]**：

  ​	给在bpnumber的断点设置一系列的命令（后面再输入）；输入"end"来结束。

```
(Pdb) commands 1
(com) p some_variable
(com) end
(Pdb)
```

​	要去除的话，输入commands，然后直接输入"end"。

​	如果不给出bpnumber，则默认是最后一个command。



**s(tep)**:

​	执行一步。会进到调用的function里面。



**n(ext)**:

​	执行一步。一个function call看作是一步。



**unt(il) [lineno]**:

​	不给出lineno，则运行到比当前行号要大的行才停下来；

​	给出行号，则运行到大于等于给定的行号。



**r(eturn)**:

​	运行到当前function return为止。



**c(ont(inue))**:

​	运行到遇到breakpoint为止。



**l(ist) [first[, last]]**:

​	列出source code。

​	不给出参数的话，显示当前行周围的11行；

​	给一个参数，则给出那一行的附近11行；

​	给两个参数，显示两行之间的代码；如果后一个参数比前一个参数小，则表示显示多少多少行。



**ll | longlist**:

​	显示当前function或者frame的所有代码。



**a(rgs)**:

​	打印当前函数的argument list。



**p expression**：

​	计算并打印一个表达式。



**pp expression**:

​	同上，不过用了pprint模块来pretty print。



**whatis expression**:

​	打印expression结果的type。



**source expression**:

​	打印给定object的source code。



**interact**:

​	启动interactive interpreter，whose global namespace contains all the (global and local) names found in the current scope。



**restart [args ...]**：

​	重新启动当前的program。args会传递给sys.argv。History, breakpoints, actions, debugger options会保留下来。



**q(uit)**：

​	退出pdb。