---
title: 在Hugo Post里嵌入HTML页面
date: 2018-11-13 16:28:40.923 -0700
categories: [Blog]
tags: [blog, hugo, ipynb]
---

可用于嵌入静态的ipynb（先转换成html）

<!--more-->



1. 把HTML页面的后缀改为`.hugohtml`并放在`content/hugohtml/`目录下，let's say，这个文件叫`xxx.hugohtml` （不改后缀的话似乎找不到这个资源）

2. 在`config.toml`里加入`hugohtml`这一项resources

    ```
    [[resources]]
      name = "hugohtml-file-:counter"
      src = "**.hugohtml"
      [resources.params]
        icon = "hugohtml"
    ```

3. 在`layouts/shortcodes`目录下新建`import_hugohtml.html`文件，内容为：

    ```
    <script>
         function resizeIframe(obj) {
            obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
         }
    </script>
    <iframe name="" frameborder="0" scrolling="yes" marginwidth="0" marginheight="0"  width="100%" height="500%" onload="resizeIframe(this)" src="/hugohtml/{{ .Get 0 }}"></iframe>
    ```

4. 在要插入到的`.md`文件中，加入（把四个斜杠去掉）

    ```
    \{\{< import_hugohtml "xxxx.hugohtml" >\}\}
    ```

5. 效果如下

    {{< import_hugohtml "data_loading_tutorial.hugohtml" >}}