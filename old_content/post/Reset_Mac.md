---
title: Reset a Mac to Factory Settings
date: 2018-12-21 0:08:40.923 -0700
categories: [Mac]
tags: [mac]
---

<!--more-->

1. Restart your **Mac**

2. While the startup disc is waking up, hold down the **Command+R** keys simultaneously. You're Mac will boot into macOS Recover. 

3. Select **Disk Utility**. 

4. Click on **Continue**.

5. Select your **Startup disk** (it is probably named "Macintosh HD" or something similar).

6. Click on **Erase** from the buttons at the top of the Disk Utility window.

7. Enter a **name** for the file to be destroyed (Like Macintosh HD or something).

8. If your Mac is using HFS+, select **Mac OS Extended (Journaled)** from the format list. If your Mac is using APFS, select **APFS** from the format list. *See Troubleshooting for more information on which format to select.* 

9. If Scheme is available, select **GUID Partition Map**.

10. Click **Erase**.

11. After the process is complete, select **Quit Disk Utility** from the Disk Utility drop-down menu in the upper left corner of the screen.

12. Click on **Reinstall macOS** (or Reinstall OS X where applicable) to reinstall the operating system that came with your Mac.

13. ......