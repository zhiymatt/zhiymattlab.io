---
title: Change Vultr Server Root Password
date: 2018-12-18 23:08:40.923 -0700
categories: [Else]
tags: [else]
---

 <span></span>

<!--more-->



#### Centos6

- 点击vultr后台面板View Console按钮，点击组合键ctrl+alt+del重启vps
- 在出现grub提示符时，迅速按下任意键，进入grub启动菜单
- 输入字母a进入grub命令行，输入空格+single再按回车键
- vps会重启，出现提示符#，此时输入passwd root，根据提示输入新的root密码

#### Debian, Ubuntu, CentOS 7

- 前面几步相同
- 找到grub菜单里的内核kernel一行，通常以”linux /boot/”开头，结尾添加 init=”/bin/bash”
- 按CTRL-X或F10重启
- 输入”mount -rw -o remount /” 接着输入 “passwd”修改新的密码