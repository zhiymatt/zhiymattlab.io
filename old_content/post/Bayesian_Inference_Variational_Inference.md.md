---
title: Bayesian Inference, Variational Inference, MCMC Method
date: 2019-01-31 11:34:40.923 -0700
categories: [ML]
tags: [ml, mcmc, bayesian inference, variational inference]
---

<span></span>

<!--more-->



---



# Bayesian Inference for Mixture Models

**Statistical inference** is the process of using data analysis to deduce properties of an underlying probability distribution. 

### Finite Mixtures

A finite mixture of $k$ densities of the same distribution is a convex combination,

$$f(x|k, \rho, \theta) = \sum\_{i=1}^k \rho\_i f(x|\theta\_i), $$

of densities $f(x | \theta\_i)$, where$\rho = (\rho\_1, \dots, \rho\_k)$, such that $\sum\_{i=1}^k \rho\_i = 1​$.

Note that $E[x^r] = \sum\_{i=1}^k \rho\_i E[X^r|\theta\_i]$

Computationally intensive methods must be considered for inference in mixture models: MCMC methods, EM algorithms ...

The Bayesian approach using MCMC methods allow us to transform the complex structure of a mixture model in a set of simple structures using latent variables.

### Bayesian Inference for finite mixtures

**Bayesian inference** is a method of statistical **inference** in which **Bayes**' theorem is used to update the probability for a hypothesis as more evidence or information becomes available. 

Assume we have n observations $x = (x\_1, \dots, x\_n)​$ sampled i.i.d. from a finite mixture distribution with density, 

$$f(x|\rho, \theta) = \sum\_{i=1}^k \rho\_i f(x|\theta\_i),$$

where k is finite and known.

We wish to make Bayesian inference for the model parameters $(\rho, \theta)$. The likelihood is,

$$l(\rho, \theta|x) = \prod\_{j=1}^n \sum\_{i=1}^k \rho\_i f(x\_j | \theta\_i),​$$

In order to simplify the likelihood, we can introduce latent variables $z\_j $ such that:

$$x\_j |z\_j = i \sim f(x|\theta)\ and\ P(z\_j = i) = \rho\_i.$$

These auxiliary variables allow us to identify the mixture component each observation has been generated from.

Therefore, for each sample of data $x=(x\_1, \dots, x\_n)​$, we assume a missing data set $z = (z\_1, \dots, z\_n)​$, which provide the labels indicating the mixture components from which the observations have been generated.

// **Assume a missing variable z to simplify the computation complexity of inference using Bayesian theorem.**

Using this missing data set, the likelihood simplifies to:

$$l(\rho, \theta|x, z)=\prod\_{j=1}^n \rho\_{z\_j} f(x\_j | \theta\_{z\_j}) = \prod\_{i=1}^k \rho\_i^{n\_i} [\prod\_{j:z\_j=i} f(x\_j | \theta\_i)]$$

where $n\_i = \#\{z\_j = i\}$ and $\sum n\_i = n$.

Then, the posterior probability that the observation $x\_j$ has been generated from the $i$-th component is:

$$P(z\_j = i | x\_j, \rho, \theta) = \frac{\rho\_i f(x\_j | \theta\_i)}{\sum\_{i=1}^k \rho\_i f(x\_j | \theta\_i)}$$

#### Example (Bayesian inference for Gaussian mistures)

using the missing data $z$, the likelihood simplifies to:

$$ l(\rho, \mu, \phi | x, z) \propto \sum\_{i=1}^k (\rho\_i \phi\_i)^{n\_i} \exp(-\frac{\phi\_i}{2} \sum\_{j:z\_j = i} (x\_j - \mu\_i)^2),$$

where  $n\_i = \#\{z\_j = i\}.$

And we have that:

$$ P(z\_j = i | x\_j, \rho, \mu, \phi) = \frac{\rho\_i \phi\_i exp\{-\frac{\theta\_i}{2}(x\_j - \mu\_i)^2\}}{\sum^k\_{i=1}\rho\_i \phi\_i exp\{-\frac{\theta\_i}{2}(x\_j - \mu\_i)^2\}}$$

#### MCMC algorithm

1. Set initial values $\eta^{(0)}, \mu^{(0)}\ and\  \phi^{(0)}$.
2. Update $z$ sampling from $z^{(j+1)} \sim z|x, \eta^{(j)}, \mu^{(j)}, \phi^{(j)}$.
3. Update $\eta$ sampling from $\eta^{(j+1)} \sim \eta | x, z^{(j+1)}$.
4. Update $\phi\_i$ sampling from $\phi^{(j+1)} \sim \phi | x, z^{(j+1)}$.
5. Update $\mu\_i$ sampling from $\mu\_i^{(j+1)} \sim \mu\_i | x, z^{(j+1)}$.
6. Update $\mu^{j+1}$ and arrange $\rho^{(j+1)} y \phi ^{(j+1)}$ with this order.
7. $j = j + 1$. Go to 2.

#### MCMC v.s. EM

EM is an optimisation technique: given a likelihood with useful  latent variables, it returns a local maximum, which may be a global  maximum depending on the starting value.

MCMC is a simulation method: given a likelihood with or without  latent variables, and a prior, it produces a sample that is  approximately distributed from the posterior distribution. The first  values of that sample usually depend on the starting value, which means  they are often discarded as burn-in (or warm-up) stage. 



---



# Variational Inference

### Set up

- Assume that $x = x\\_{1:m}$ are observations and $z = z\_{1:m}$ are hidden variables. Also assume that additional parameters $\alpha$ that are fixed.

- These assumptions are general -- the hidden variables might include the "parameters", e.g., in a traditional inference setting. (In that case, $\alpha$ are the hyperparameters.)

- **Posterior distribution** (p.s.: after experience)

    $$p(z|x, \alpha) = \frac{p(z, x|\alpha)}{\int\_z{p(z, x|\alpha)}}$$

### Multinormal Distribution

In probability theory, the multinomial distribution is a generalization of the binomial distribution. For example, it models the probability of counts for rolling a k-sided die n times. For n independent trials each of which leads to a success for exactly one of k categories, with each category having a given fixed success probability, the multinomial distribution gives the probability of any particular combination of numbers of successes for the various categories.

When k is 2 and n is 1, the multinomial distribution is the Bernoulli distribution. When k is 2 and n is bigger than 1, it is the binomial distribution. When k is bigger than 2 and n is 1, it is the categorical distribution. 

If for $x=(x\_1, x\_2, \dots, x\_n)$:

- $x\_i \ge (1 \le i \le n); x\_1 + x\_2 + \dots + x\_n = N$
- $m\_1, m\_2, \dots, m\_n \ge 0; m\_1 + m\_2 + m\_n = N$

then:

$$ p(x\_1 = m\_1, x\_2 = m\_2, \dots, x\_n = m\_n = \frac{N!}{m\_1!m\_2!\dotsm\_n!}p\_1^{m\_1}p\_2^{m\_2}\dots p\_n^{m\_n}$$

where:

$$p\_i \ge 0 (1 \le i \le n), p\_1 + p\_2 + \dots + p\_n = 1$$

Marked as:

$$X \sim PN(N: p\_1, p\_2, \dots, p\_n)$$

### Main Idea

**The main idea behind variational methods is to pick a family distributions over the latent variables with its own variational parameters,**

$$q(z_{1:m}|\nu)$$

Then, find the setting of the parameters that makes $q$ close to the posterior of interest.

Use `$q$` with the fitted parameters as a proxy for the posterior, e.g., to form preditions about feature data or to investigate the posterior distribution of the hidden variables.

Typically, the true posterior is not in the variational family.



# References

[Bayesian inference for mixture models](http://halweb.uc3m.es/esp/Personal/personas/causin/eng/2011-2012/Bayes/chapter_12.pdf)

[Variational Inference](https://www.cs.princeton.edu/courses/archive/fall11/cos597C/lectures/variational-inference-i.pdf)