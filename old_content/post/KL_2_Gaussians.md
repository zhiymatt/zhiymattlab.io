---
title: KL divergence between two univariate Gaussians
date: 2019-01-31 10:29:40.923 -0700
categories: [ML]
tags: [ml, kl div]
---



<!--more-->

KL

<div>$$
\begin{align}
KL(p, q) &= - \int p(x) \log q(x) dx + \int p(x) \log p(x) dx \\
&=\frac{1}{2} \log (2 \pi \sigma_2^2) + \frac{\sigma_1^2 + (\mu_1 - \mu_2)^2}{2 \sigma_2^2} - \frac{1}{2} (1 + \log 2 \pi \sigma_1^2) \\
&= \log \frac{\sigma_2}{\sigma_1} + \frac{\sigma_1^2 + (\mu_1 - \mu_2)^2}{2 \sigma_2^2} - \frac{1}{2}
\end{align}
$$</div>

Gaussian / Normalization Distribution

<div>$$
f(x | \mu, \sigma^2) = \frac{1}{\sqrt{2\pi \sigma^2}} \exp(-\frac{(x-\sigma)^2}{2\sigma_2})
$$</div>
