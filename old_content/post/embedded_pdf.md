---
title: Embed PDF file in Hugo .md post
date: 2018-10-17 05:15:40.923 -0700
categories: [Blog]
tags: [blog, hugo, pdf]
---

A tutorial on embedding pdf in hugo md file.

<!--more-->

<script>
var url = "/pdf/test_embedded_pdf.pdf";
</script>

# Sol 1: using pdf.js

1. include url of the pdf file in the beginning of your .md file, 

	```html
    <script>
    var url = "/pdf/xxx.pdf";
    </script>
  ```

    otherwise it may be encoded as HTML code mistakenly.

    ```html
    <p><script>
    var url = &ldquo;/pdf/xxx.pdf&rdquo;;
    </script></p>
    ```

2. create display_pdf.js file in the parent/js folder as the .md file

    ```javascript
    document.write('<div><button id="prev">Previous</button><button id="next">Next</button>&nbsp; &nbsp;<span>Page: <span id="page_num"></span> / <span id="page_count"></span></span></div>\<div id="wrapper"><canvas id="the-canvas"></canvas></div>')
    
    var loadJS = function (url, implementationCode, location) {
        // url is URL of external file, implementationCode is the code
        // to be called from the file, location is the location to 
        // insert the <script> element
    
        var scriptTag = document.createElement('script')
        scriptTag.src = url
    
        scriptTag.onload = implementationCode
        scriptTag.onreadystatechange = implementationCode
    
        location.appendChild(scriptTag)
    }
    var yourCodeToBeCalled = function () {
        var pdfjsLib = window['pdfjs-dist/build/pdf']
        pdfjsLib.GlobalWorkerOptions.workerSrc = 'https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.0.550/pdf.worker.min.js'
    
        var pdfDoc = null,
            pageNum = 1,
            pageRendering = false,
            pageNumPending = null,
            scale = 2.25,
            canvas = document.getElementById('the-canvas'),
            ctx = canvas.getContext('2d')
    
        /**
         * Get page info from document, resize canvas accordingly, and render page.
         * @param num Page number.
         */
        function renderPage (num) {
            pageRendering = true
            // Using promise to fetch the page
            pdfDoc.getPage(num).then(function (page) {
                var viewport = page.getViewport(scale)
                canvas.height = viewport.height
                canvas.width = viewport.width
    
                canvas.style.width = '100%'
                canvas.style.height = '100%'
                wrapper.style.width = Math.floor(viewport.width / scale) + 'pt'
                wrapper.style.height = Math.floor(viewport.height / scale) + 'pt'
    
                // Render PDF page into canvas context
                var renderContext = {
                    canvasContext: ctx,
                    viewport: viewport
                }
                var renderTask = page.render(renderContext)
    
                // Wait for rendering to finish
                renderTask.promise.then(function () {
                    pageRendering = false
                    if (pageNumPending !== null) {
                        // New page rendering is pending
                        renderPage(pageNumPending)
                        pageNumPending = null
                    }
                })
            })
    
            // Update page counters
            document.getElementById('page_num').textContent = num
        }
    
        /**
         * If another page rendering in progress, waits until the rendering is
         * finised. Otherwise, executes rendering immediately.
         */
        function queueRenderPage (num) {
            if (pageRendering) {
                pageNumPending = num
            } else {
                renderPage(num)
            }
        }
    
        /**
         * Displays previous page.
         */
        function onPrevPage () {
            if (pageNum <= 1) {
                return
            }
            pageNum--
            queueRenderPage(pageNum)
        }
        document.getElementById('prev').addEventListener('click', onPrevPage)
    
        /**
         * Displays next page.
         */
        function onNextPage () {
            if (pageNum >= pdfDoc.numPages) {
                return
            }
            pageNum++
            queueRenderPage(pageNum)
        }
        document.getElementById('next').addEventListener('click', onNextPage)
    
        /**
         * Asynchronously downloads PDF.
         */
        pdfjsLib.getDocument(url).then(function (pdfDoc_) {
            pdfDoc = pdfDoc_
            document.getElementById('page_count').textContent = pdfDoc.numPages
    
            // Initial/first page rendering
            renderPage(pageNum)
        })
    }
    loadJS('https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.0.550/pdf.min.js', yourCodeToBeCalled, document.body)
    ```

4. include following lines in your .md file. beware of the first "../" prefix.

    ```javascript
    <script src="../../js/display_pdf.js"></script>
    ```

### result:

<script src="../../js/display_pdf.js"></script>

-----

# Sol 2: using native html tag

Beware: this solution is not gonna display the .pdf file correctly on your mobile devices! (only display the first page)


    ```
    <object data="/pdf/test_embedded_pdf.pdf" type="application/pdf" width="800" height="1000">
        <p>It appears you don't have a PDF plugin for this browser.
        No biggie... you can <a href="test.pdf">click here to
        download the PDF file.</a></p>
    </object>
    ```

### result:
<object data="/pdf/test_embedded_pdf.pdf" type="application/pdf" width="800" height="1000">
    <p>It appears you don't have a PDF plugin for this browser.
    No biggie... you can <a href="test_embedded_pdf.pdf">click here to
    download the PDF file.</a></p>
</object>

