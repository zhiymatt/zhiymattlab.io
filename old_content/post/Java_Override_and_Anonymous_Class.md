---
title: "Java: @Override and Anonymous Class"
date: 2018-12-24 03:08:40.923 -0700
categories: [Java]
tags: [java]
---

<span></span>

<!--more-->

# Override

java.lang.Override是J2SE 5.0中標準的Annotation型態之一，它對編譯器說明某個方法必須是重新定義父類別中的方法，編譯器得知這項資訊後，在編譯程式時如果發現該方法並非重新定義父類別中的方法，就會回報錯誤。

# Anonymous Class

```java
KeyedStream<WikipediaEditEvent, String> keyedEdits = edits
    .keyBy(new KeySelector<WikipediaEditEvent, String>() {
        @Override
        public String getKey(WikipediaEditEvent event) {
            return event.getUser();
        }
    });
```

That is a Java anonymous  class. It is very common in Android and in general with **event listeners and that kind of stuff**.

For more details you can read [this link](http://www.roseindia.net/javatutorials/anonymous_innerclassestutorial.shtml) (probably not the best one):

> *The anonymous inner classes is very useful in some situation. For example consider a situation where you need to create the instance of an object without creating subclass of a class and also performing additional tasks such as method overloading.*