---
title: Env Vars
date: 2019-01-07 10:37:40.923 -0700
categories: [Windows]
tags: [windows, env]

---

Windows 10 system and user variables

<!--more-->

# User Variables

HADOOP_HOME
```
C:\Users\miylo\opt\spark-2.4.0-bin-hadoop2.7
```

JAVA_HOME
```
C:\Program Files\Java\jdk1.8.0_191
```

MAVEN_HOME
```
C:\Users\miylo\opt\apache-maven-3.6.0
```

Path
```
C:\Users\miylo\Anaconda3;C:\Users\miylo\Anaconda3\Library\mingw-w64\bin;C:\Users\miylo\Anaconda3\Library\usr\bin;C:\Users\miylo\Anaconda3\Library\bin;C:\Users\miylo\Anaconda3\Scripts;
```

SPARK_HOME
```
C:\Users\miylo\opt\spark-2.4.0-bin-hadoop2.7
```

# System Variables

Path
```
C:\Users\miylo\opt\spark-2.4.0-bin-hadoop2.7\bin;C:\Users\miylo\opt\apache-maven-3.6.0\bin;
```