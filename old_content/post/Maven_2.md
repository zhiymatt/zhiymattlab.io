---
title: Maven Tutorial (2)
date: 2018-12-28 01:48:40.923 -0700
categories: [Java]
tags: [java, maven]
---

Maven Tutorial (2)

<!--more-->

# Settings

1. 找到M2_HOME的位置
	```bash
	dpkg -L maven
	```
	
2. 编辑{M2_HOME}/conf/settings.xml

# Maven本地资源库

Maven的本地资源库是用来存储所有项目的依赖关系(插件jar和其他文件，这些文件被Maven下载)到本地文件夹。很简单，当你建立一个Maven项目，所有相关文件将被存储在你的Maven本地仓库。

默认情况下，Maven的本地资源库默认为 .m2 目录文件夹：

- Unix/Mac OS X – ~/.m2
- Windows – C:\Documents and Settings\{your-username}\.m2

# Maven中央存储库

当你建立一个 Maven 的项目，Maven 会检查你的 pom.xml 文件，以确定哪些依赖下载。首先，Maven 将从本地资源库获得 Maven 的本地资源库依赖资源，如果没有找到，然后把它会从默认的 Maven 中央存储库 – https://search.maven.org/ 查找下载。

用这个repository：https://mvnrepository.com/

# 声明Remote Repository的URL

在**pom.xml**里面添加存储某一个library的repository。Example:

```xml
<project ...>
	<repositories>
		<repository>
			<id>java.net</id>
			<url>https://maven.java.net/content/repositories/public/</url>
		</repository>
	</repositories>
</project>
```

现在，Maven的依赖库查询顺序更改为：

1. 在 Maven 本地资源库中搜索，如果没有找到，进入第 2 步，否则退出。
2. 在 Maven 中央存储库搜索，如果没有找到，进入第 3 步，否则退出。
3. 在java.net Maven的远程存储库搜索，如果没有找到，提示错误信息，否则退出。

# Maven依赖机制/案例

1. 需要知道 log4j 的 Maven 坐标(访问 Maven 中心储存库，搜索想要下载的jar)，例如：

  ```xml
    <groupId>log4j</groupId>
    <artifactId>log4j</artifactId>
    <version>1.2.14</version>
  ```

2. 它会自动下载 log4j 的1.2.14 版本库。如果“version”标签被忽略，它会自动升级库时当有新的版本时。声明 Maven 的坐标转换成 pom.xml 文件。

	```xml
    <dependencies>
        <dependency>
    	<groupId>log4j</groupId>
    	<artifactId>log4j</artifactId>
    	<version>1.2.14</version>
        </dependency>
    </dependencies>
	```
3. 当 Maven 编译或构建，log4j 的 jar 会自动下载，并把它放到 Maven 本地存储库
    所有由 Maven 管理

# 定制库到Maven本地资源库/案例

这两种情况需要手动发出Maven命令包括一个 jar 到 Maven 的本地资源库：

- 要使用的 jar 不存在于 Maven 的中心储存库中。
- 之前创建了一个自定义的 jar ，而另一个 Maven 项目需要使用。

#### 案例

1. 下载 “kaptcha”，将其解压缩并将 kaptcha-version.jar 复制到其他地方，比如：C盘。发出下面的命令：

    ```bash
    mvn install:install-file -Dfile=c:\kaptcha-{version}.jar -DgroupId=com.google.code -DartifactId=kaptcha -Dversion={version} -Dpackaging=jar
    ```

    现在，“kaptcha” jar被复制到 Maven 本地存储库。

2. 安装完毕后，就在 pom.xml 中声明 kaptcha 的坐标。

    ```xml
    <dependency>
        <groupId>com.google.code</groupId>
        <artifactId>kaptcha</artifactId>
        <version>2.3</version>
    </dependency>
    ```

3. 构建它，现在 “kaptcha” jar 能够从你的 Maven 本地存储库检索了。 

# 使用Maven创建Java项目 

1. 从 Maven 模板创建一个项目

	在终端（* UNIX或Mac）或命令提示符（Windows）中，浏览到要创建 Java 项目的文件夹。键入以下命令：

	```bash
	mvn archetype:generate -DgroupId={project-packaging} -DartifactId={project-name}-DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false
	```
	
	这告诉 Maven 来从 maven-archetype-quickstart 模板创建 Java 项目。如果忽视 archetypeArtifactId 选项，一个巨大的 Maven 模板列表将列出。

2. Maven目录布局

	使用 mvn archetype:generate + maven-archetype-quickstart 模板, 以下项目的目录结构被创建。

    ```
    NumberGenerator
       |-src
       |---main
       |-----java
       |-------com
       |---------yiibai   
       |-----------App.java
       |---test|-----java
       |-------com
       |---------yiibai
       |-----------AppTest.java
       |-pom.xml
    ```

    很简单的，所有的源代码放在文件夹 /src/main/java/, 所有的单元测试代码放入 /src/test/java/.

    附加的一个标准的 pom.xml 被生成。这个POM文件类似于 Ant build.xml 文件，它描述了整个项目的信息，一切从目录结构，项目的插件，项目依赖，如何构建这个项目等，请阅读POM官方指南。

3. Eclipse IDE

    为了使它成为一个 Eclipse 项目，在终端进入到 “NumberGenerator” 项目，键入以下命令：

    ```bash
    mvn eclipse:eclipse
    ```

    执行以上命令后，它自动下载更新相关资源和配置信息（需要等待一段时间），并产生 Eclipse IDE所要求的所有项目文件。要导入项目到Eclipse IDE中，选择 “File -> Import… -> General->Existing Projects into Workspace”

4. 更新POM

    默认的 pom.xml 太简单了，很多时候，你需要添加编译器插件来告诉 Maven 使用哪个 JDK 版本是用来编译项目。（默认JDK1.4，这的确太旧了点）

    ```xml
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-compiler-plugin</artifactId>
            <version>2.3.2</version>
            <configuration>
                <source>1.6</source>
                <target>1.6</target>
            </configuration>
        </plugin>
    ```

    从更新JUnit 3.8.1到最新的 4.11。

    ```
    <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.11</version>
        <scope>test</scope>
    </dependency>
    ```

    **Maven 坐标**

    上面的XML代码片段被称为“Maven坐标”，如果你需要 JUnit 的 jar，你需要找出其相应的 Maven 坐标。它适用于所有其他的依赖，如Spring，Hibernate，Apache 普通的等，只要到Maven中心储存库，并找出哪些是依赖正确的 Maven 坐标。

    **pom.xml – 更新版本**

    ```xml
    <project xmlns="http://maven.apache.org/POM/4.0.0" 
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 
        http://maven.apache.org/maven-v4_0_0.xsd">
        <modelVersion>4.0.0</modelVersion>
        <groupId>com.mkyong</groupId>
        <artifactId>NumberGenerator</artifactId>
        <packaging>jar</packaging>
        <version>1.0-SNAPSHOT</version>
        <name>NumberGenerator</name>
        <url>http://maven.apache.org</url>
        <dependencies>
            <dependency>
                <groupId>junit</groupId>
                <artifactId>junit</artifactId>
                <version>4.11</version>
                <scope>test</scope>
            </dependency>
        </dependencies>

        <build>
          <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>2.3.2</version>
                <configuration>
                    <source>1.6</source>
                    <target>1.6</target>
                </configuration>
            </plugin>
          </plugins>
        </build>

    </project>
    ```

    在终端，再次发出同样的命令 mvn eclipse:eclipse ,Maven将从Maven中心储存库下载插件项目依赖关系（JUnit），它会自动保存到你的本地仓库。

5. 更新业务逻辑

    测试驱动开发（TDD），先更新单元测试，以确保应用程序（APP）对象有一个方法来生成包含恰好36位字母表的唯一密钥。

    **AppTest.java**

    ```java
    package com.yiibai;

    import org.junit.Assert;
    import org.junit.Test;

    public class AppTest {

        @Test
        public void testLengthOfTheUniqueKey() {

            App obj = new App();
            Assert.assertEquals(36, obj.generateUniqueKey().length());

        }
    }
    ```

    完成业务逻辑。

	**App.java**

    ```java
    package com.yiibai;

    import java.util.UUID;

    // Generate a unique number
    
    public class App 
    {

        public static void main( String[] args )
        {
            App obj = new App();
            System.out.println("Unique ID : " + obj.generateUniqueKey());
        }

        public String generateUniqueKey(){

            String id = UUID.randomUUID().toString();
            return id;

        }
    }
    ```

6. Maven 打包

    现在，我们将使用Maven这个项目，并输出编译成一个 “jar” 的文件。 请参考 pom.xml 文件，包元素定义应该包应该输出什么。
    
    **pom.xml**

    ```java
    <project ...>
        <modelVersion>4.0.0</modelVersion>
        <groupId>com.yiibai</groupId>
        <artifactId>NumberGenerator</artifactId>	
        <packaging>jar</packaging>	
        <version>1.0-SNAPSHOT</version>
    ```

	在终端输入 mvn package :

	```bash
	mvn package
	```

	它编译，运行单元测试并打包项目成一个 jar 文件，并把它放在 project/target 文件夹。
	
# 使用Maven创建Web应用程序项目

略

# Maven POM

POM代表项目对象模型。它是 Maven 中工作的基本单位，这是一个 XML 文件。它始终保存在该项目基本目录中的 pom.xml 文件。
POM 包含的项目是使用 Maven 来构建的，它用来包含各种配置信息。
POM 也包含了目标和插件。在执行任务或目标时，Maven 会使用当前目录中的 POM。它读取POM得到所需要的配置信息，然后执行目标。部分的配置可以在 POM 使用如下：

- project dependencies
- plugins
- goals
- build profiles
- project version
- developers
- mailing list

创建一个POM之前，应该要先决定项目组(groupId)，它的名字(artifactId)和版本，因为这些属性在项目仓库是唯一标识的。

要注意的是，每个项目只有一个POM文件。

- 所有的 POM 文件要项目元素必须有三个必填字段: `groupId`，`artifactId`，`version`
- 在库中的项目符号是：`groupId:artifactId:version` 
- pom.xml 的根元素是 `project`，它有三个主要的子节点。

| 节点       | 描述                                                         |
| ---------- | ------------------------------------------------------------ |
| groupId    | 这是项目组的编号，这在组织或项目中通常是独一无二的。 例如，一家银行集团`com.company.bank`拥有所有银行相关项目。 |
| artifactId | 这是项目的ID。这通常是项目的名称。 例如，`consumer-banking`。 除了`groupId`之外，`artifactId`还定义了`artifact`在存储库中的位置。 |
| `version`  | 这是项目的版本。与`groupId`一起使用，`artifact`在存储库中用于将版本彼此分离。 例如：`com.company.bank:consumer-banking:1.0`，`com.company.bank:consumer-banking:1.1` |

# 超级POM

所有的POM继承自父类(尽管明确界定)。这个基础的 POM 被称为超级 POM，并包含继承默认值。 Maven使用有效的POM(超级POM加项目配置的配置)执行有关目标。它可以帮助开发人员指定最低配置的详细信息写在 pom.xml 中。虽然配置可以很容易被覆盖。 一个简单的方法来看看超级POM的默认配置，通过运行下面的命令：mvn help:effective-pom 创建一个 pom.xml 。 在下面的例子中，已经创建了一个 pom.xml 在C:\MVN\ 项目文件夹中。 现在，打开命令控制台，进入包含 pom.xml 文件夹并执行以下 mvn 命令。

```bash
mvn help:effective-pom
```

Maven将开始处理，并显示有效的 effective-pom 。

在控制台显示结果：有效POM，继承，插值，应用配置文件。

在上面的pom.xml 中，可以看到默认的项目源文件夹结构，输出目录，插件，资料库，报表目录，Maven将使用它们来执行预期的目标。

Maven pom.xml 无须手动写入。

Maven提供了大量的原型插件以创建项目，包括项目结构 和 pom.xml

# Maven 构建生命周期

构建生命周期阶段的目标是执行顺序是一个良好定义的序列。
这里使用一个例子，一个典型的 [Maven](http://www.yiibai.com/maven) 构建生命周期是由下列顺序的阶段： 

| 阶段     | 处理     | 描述                                     |
| -------- | -------- | ---------------------------------------- |
| 准备资源 | 资源复制 | 资源复制可以进行定制                     |
| 编译     | 执行编译 | 源代码编译在此阶段完成                   |
| 包装     | 打包     | 创建JAR/WAR包如在 pom.xml 中定义提及的包 |
| 安装     | 安装     | 这一阶段在本地/远程Maven仓库安装程序包   |

可用于注册必须执行一个特定的阶段之前或之后的目标，有之前处理和之后阶段。

当 Maven 开始建立一个项目，它通过定义序列阶段步骤和执行注册的每个阶段的目标。 Maven有以下三种标准的生命周期： 

-  clean 		
-  default(或 build) 		
-  site 		

目标代表一个特定的任务，它有助于项目的建设和管理。可以被绑定到零个或多个生成阶段。一个没有绑定到任何构建阶段的目标，它的构建生命周期可以直接调用执行。

执行的顺序取决于目标和构建阶段折调用顺序。例如，考虑下面的命令。清理和打包（mvn clean）参数的构建阶段，而 dependency:copy-dependencies package 是一个目标。 

```bash
mvn clean dependency:copy-dependencies package
```

在这里，清洁的阶段，将首先执行，然后是依赖关系：复制依赖性的目标将被执行，并终于将执行包阶段。 

### 清洁(Clean)生命周期 

当执行命令 mvn clean 命令后，Maven 调用清洁的生命周期由以下几个阶段组成： 

- pre-clean 		
- clean 		
- post-clean 		

Maven 清洁目标（clean:clean）被绑定清洁干净的生命周期阶段。clean:clean 目标删除 build 目录下的构建输出。因此，当 mvn clean 命令执行时，Maven会删除编译目录。 

目标清洁生命周期在上述阶段，我们可以自定义此行为。
在下面的示例中，我们将附加 maven-antrun-plugin:run 对目标进行预清洁，清洁和清洁后这三个阶段。这将使我们能够调用的信息显示清理生命周期的各个阶段。
现在来创建了一个 pom.xml 文件在 **C:\MVN**项目文件夹中，具体内容如下：

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0"
   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
   xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
   http://maven.apache.org/xsd/maven-4.0.0.xsd">
<modelVersion>4.0.0</modelVersion>
<groupId>com.companyname.projectgroup</groupId>
<artifactId>project</artifactId>
<version>1.0</version>
<build>
<plugins>
   <plugin>
   <groupId>org.apache.maven.plugins</groupId>
   <artifactId>maven-antrun-plugin</artifactId>
   <version>1.1</version>
   <executions>
      <execution>
         <id>id.pre-clean</id>
         <phase>pre-clean</phase>
         <goals>
            <goal>run</goal>
         </goals>
         <configuration>
            <tasks>
               <echo>pre-clean phase</echo>
            </tasks>
         </configuration>
      </execution>
      <execution>
         <id>id.clean</id>
         <phase>clean</phase>
         <goals>
          <goal>run</goal>
         </goals>
         <configuration>
            <tasks>
               <echo>clean phase</echo>
            </tasks>
         </configuration>
      </execution>
      <execution>
         <id>id.post-clean</id>
         <phase>post-clean</phase>
         <goals>
            <goal>run</goal>
         </goals>
         <configuration>
            <tasks>
               <echo>post-clean phase</echo>
            </tasks>
         </configuration>
      </execution>
   </executions>
   </plugin>
</plugins>
</build>
</project>
```

现在，打开命令控制台，到该文件夹包含 pom.xml 并执行以下 mvn 命令。 

```bash
mvn post-clean
```

Maven将开始处理并显示清理生命周期的所有阶段。 

```
[INFO] Scanning for projects...
[INFO] ------------------------------------------------------------------
[INFO] Building Unnamed - com.companyname.projectgroup:project:jar:1.0
[INFO]    task-segment: [post-clean]
[INFO] ------------------------------------------------------------------
[INFO] [antrun:run {execution: id.pre-clean}]
[INFO] Executing tasks
     [echo] pre-clean phase
[INFO] Executed tasks
[INFO] [clean:clean {execution: default-clean}]
[INFO] [antrun:run {execution: id.clean}]
[INFO] Executing tasks
     [echo] clean phase
[INFO] Executed tasks
[INFO] [antrun:run {execution: id.post-clean}]
[INFO] Executing tasks
     [echo] post-clean phase
[INFO] Executed tasks
[INFO] ------------------------------------------------------------------
[INFO] BUILD SUCCESSFUL
[INFO] ------------------------------------------------------------------
[INFO] Total time: < 1 second
[INFO] Finished at: Sat Jul 07 13:38:59 IST 2012
[INFO] Final Memory: 4M/44M
[INFO] ------------------------------------------------------------------
```

你可以尝试调整 mvn 清洁命令，该命令将显示清洁前什么都不会被执行。 

### 默认（或生成）生命周期

这是 Maven 主要的生命周期，用于构建应用程序。它有以下 23 个阶段。 

| 生命周期阶段          | 描述                                                      |
| --------------------- | --------------------------------------------------------- |
| validate              | 验证项目是否正确，并且所有必要的信息可用于完成构建过程    |
| initialize            | 建立初始化状态，例如设置属性                              |
| generate-sources      | 产生任何的源代码包含在编译阶段                            |
| process-sources       | 处理源代码，例如，过滤器值                                |
| generate-resources    | 包含在包中产生的资源                                      |
| process-resources     | 复制和处理资源到目标目录，准备打包阶段                    |
| compile               | 编译该项目的源代码                                        |
| process-classes       | 从编译生成的文件提交处理，例如：Java类的字节码增强/优化   |
| generate-test-sources | 生成任何测试的源代码包含在编译阶段                        |
| process-test-sources  | 处理测试源代码，例如，过滤器任何值                        |
| test-compile          | 编译测试源代码到测试目标目录                              |
| process-test-classes  | 处理测试代码文件编译生成的文件                            |
| test                  | 运行测试使用合适的单元测试框架（JUnit）                   |
| prepare-package       | 执行必要的任何操作的实际打包之前准备一个包                |
| package               | 提取编译后的代码，并在其分发格式打包，如JAR，WAR或EAR文件 |
| pre-integration-test  | 完成执行集成测试之前所需操作。例如，设置所需的环境        |
| integration-test      | 处理并在必要时部署软件包到集成测试可以运行的环境          |
| pre-integration-test  | 完成集成测试已全部执行后所需操作。例如，清理环境          |
| verify                | 运行任何检查，验证包是有效的，符合质量审核规定            |
| install               | 将包安装到本地存储库，它可以用作当地其他项目的依赖        |
| deploy                | 复制最终的包到远程仓库与其他开发者和项目共享              |

有涉及到Maven 生命周期值得一提几个重要概念： 

- **当一个阶段是通过 Maven命令调用，例如：mvn compile，只有阶段到达并包括这个阶段才会被执行**。 		
- 不同的 Maven 目标绑定到 Maven生命周期的不同阶段这是这取决于包类型(JAR/WAR/EAR)。  		

在下面的示例中，将附加 Maven 的 antrun 插件：运行目标构建生命周期的几个阶段。这将使我们能够回显的信息显示生命周期的各个阶段。

更新后的pom.xml 文件： 

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
  http://maven.apache.org/xsd/maven-4.0.0.xsd">
<modelVersion>4.0.0</modelVersion>
<groupId>com.companyname.projectgroup</groupId>
<artifactId>project</artifactId>
<version>1.0</version>
<build>
<plugins>
<plugin>
<groupId>org.apache.maven.plugins</groupId>
<artifactId>maven-antrun-plugin</artifactId>
<version>1.1</version>
<executions>
   <execution>
      <id>id.validate</id>
      <phase>validate</phase>
      <goals>
         <goal>run</goal>       </goals>
      <configuration>
         <tasks>
            <echo>validate phase</echo>
         </tasks>
      </configuration>
   </execution>
   <execution>
      <id>id.compile</id>
      <phase>compile</phase>
      <goals>
         <goal>run</goal>
      </goals>
      <configuration>
         <tasks>
            <echo>compile phase</echo>
         </tasks>
      </configuration>
   </execution>
   <execution>
      <id>id.test</id>
      <phase>test</phase>
      <goals>
         <goal>run</goal>
      </goals>
      <configuration>
         <tasks>
            <echo>test phase</echo>
         </tasks>
      </configuration>
   </execution>
   <execution>
         <id>id.package</id>
         <phase>package</phase>
         <goals>
            <goal>run</goal>
         </goals>
         <configuration>
         <tasks>
            <echo>package phase</echo>
         </tasks>
      </configuration>
   </execution>
   <execution>
      <id>id.deploy</id>
      <phase>deploy</phase>
      <goals>
         <goal>run</goal>
      </goals>
      <configuration>
      <tasks>
         <echo>deploy phase</echo>
      </tasks>
      </configuration>
   </execution>
</executions>
</plugin>
</plugins>
</build>
</project>
```

现在，打开命令控制台，进入包含 pom.xml 并执行以下 mvn 命令。 

```
mvn compile
```

编译阶段，Maven 将开始构建生命周期的阶段处理并显示。 

```
[INFO] Scanning for projects...
[INFO] ------------------------------------------------------------------
[INFO] Building Unnamed - com.companyname.projectgroup:project:jar:1.0
[INFO]    task-segment: [compile]
[INFO] ------------------------------------------------------------------
[INFO] [antrun:run {execution: id.validate}]
[INFO] Executing tasks
     [echo] validate phase
[INFO] Executed tasks
[INFO] [resources:resources {execution: default-resources}]
[WARNING] Using platform encoding (Cp1252 actually) to copy filtered resources,
i.e. build is platform dependent!
[INFO] skip non existing resourceDirectory C:\MVN\project\src\main\resources
[INFO] [compiler:compile {execution: default-compile}]
[INFO] Nothing to compile - all classes are up to date
[INFO] [antrun:run {execution: id.compile}]
[INFO] Executing tasks
     [echo] compile phase
[INFO] Executed tasks
[INFO] ------------------------------------------------------------------
[INFO] BUILD SUCCESSFUL
[INFO] ------------------------------------------------------------------
[INFO] Total time: 2 seconds
[INFO] Finished at: Sat Jul 07 20:18:25 IST 2012
[INFO] Final Memory: 7M/64M
[INFO] ------------------------------------------------------------------
```

### 网站的生命周期

Maven的网站插件通常用于创建新的文档，创建报告，部署网站等。

阶段：

- pre-site
- site
- post-site
- site-deploy

略

# References

[Maven教程](https://www.yiibai.com/maven)