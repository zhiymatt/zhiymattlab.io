---
title: Hugo Blog @ Gitlab
date: 2018-10-15
categories: [Blog]
tags: [blog, hugo]
---

Create your personal blog.

<!--more-->

1. Install Hugo on Archinux or Manjaro

    ```
    sudo pacman -Syu hugo
    ```

2. Create your own gitlab repo and clone all the files from https://gitlab.com/pages/hugo

3. Custom your website

4. start local server

    ```
    hugo server
    ```


