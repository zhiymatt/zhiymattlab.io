---
title: Denoising Criterion for Variational Auto-Encoder Framework
date: 2019-02-01 20:13:40.923 -0700
categories: [ML, Paper]
tags: [ml, paper, vae, dvae]
---

arXiv:1511.06406

<!--more-->

---

# Abstract

Denoising autoencoders (DAE) are trained to reconstruct their clean inputs with noise injected at the input level, while variational autoencoders (VAE) are trained with noise injected in their stochastic hidden layer, with a regularizer that encourages this noise injection. In this paper, we show that injecting noise both in input and in the stochastic hidden layer can be advantageous and we propose a modified variational lower bound as an improved objective function in this setup. When input is corrupted, then the standard VAE lower bound involves marginalizing the encoder conditional distribution over the input noise, which makes the training criterion intractable. Instead, we propose a modified training criterion which corresponds to a tractable bound when input is corrupted. Experimentally, we find that the proposed denoising variational autoencoder (DVAE) yields better average log-likelihood than the VAE and the importance weighted autoencoder on the MNIST and Frey Face datasets.



---



# Introduction

Variational inference has been a core component of approximate Bayesian inference along with the Markov chain Monte Carlo (MCMC) method. It has been popular to many researchers and practitioners because **the problem of learning an intractable posterior distribution is formulated as an optimization problem which has many advantages compared to MCMC**:

- can take advantage of many advanced optimization tools
- the training by optimization is usually faster than the MCMC sampling
- unlike MCMC where it is difficult to decide when to finish the sampling, the stopping criterion in variational inference is clear.

VAE:

- flexible enough to **accurately model the true posterior distribution**
- each dimension of the latent variable is assumed to be independent each other (i.e., there are factorized) and modeled by a **univariate Gaussian distribution** whose parameters are obtained by a nonlinear projection of the input using a neural network

Denoising criterion:

- the input is corrupted by adding some noise and the model is asked to recover the original input.
- plays an important role in achieving good generalization performance



---

# Background

**Variational inference** is an approximate inference method where the goal is to approximate the intractable posterior distribution `$p(z|x)$`, by a tractable approximate distribution `$q_\phi(z)$`. `$x$` is the observation and `$z \in R^D$` is the model parameters or latent variables. 

To keep it tractable, the approximate distributions are limited to a restricted family of distributions `$q_\phi\in Q$` parameterized by variational parameters `$\phi$`. For example, in the mean-field variational inference, the distributions in `$Q$` treat all dependent variables as independent, i.e., `$q(z) = \prod q_d(z_d)$`.

The basic idea of obtaining the optimal approximate distribution `$q_{\phi^*} \in Q$` is to find the variational parameter `$\phi^*$` that minimizes the KL divergence between the approximate distribution and the target distribution.

The KL divergence itself involves the intractable target distribution, instead of directly minimizing the KL divergence, we can bypass it by decomposing the marginal log-likelihood as follows:

`$$\log p(x) = E_{q_\phi(z)}[\log \frac{p(x, z)}{q_\phi(z)}] + KL(q_\phi(z) || p(z|x)).​$$`

Because the marginal log-likelihood `$\log p(x)$` is independent of the variational distribution `$q_\phi$` (which means  `$\log p(x)$`  is fixed when we only change `$q_\phi$`) and the KL term is non-negative, **instead of minimizing the KL divergence, we can maximize the first term**, called the variational lower bound, which is the same as minimizing the KL divergence term.

### Variational Autoencoders

With the VAE, the posterior distribution is defined as `$p_\theta(z|x) \propto  p_\theta (x|z) p(z)$`.

We use a parameterized distribution to define the observation model `$p_\theta(x|z)$`. A typical choice for the parameterized distribution is to use a neural network where the input is `$z$` and output a parametric distribution over `$x$`, such as the Gaussian or Bernoulli, depending on the data type.

- Generative network --> `$p_\theta (x|z)$`

- Inference network, recognition network --> `$p_\phi (z|x)$`

<div>
$$
\begin{align}
\log p_\theta (x) &\ge E_{q_\phi} (z|x) [\log \frac{p_\theta (x, z)}{q_\phi (z|x)}] \\
&= E_{q_\phi} (z|x) [\log p_\theta (x|z)]  - KL(q_\phi (z|x) || p(z))
\end{align}
$$
</div>


Note that in the above Eqn., we can interpret the first term as a reconstruction accuracy through an autoencoder with noise (because `$q_\phi$` is an approximation) injected in the hidden layer that is the output of the inference network, and the second term as a regularizer which enforces the approximate posterior to be close to the prior and maximizes the entropy of the injected noise.

The earlier approaches to train this type of model were based on the variational EMGalgorithm. However, with the VAE it is possible to apply the backpropagation on the variational parameter `$\phi$` by using the re-parameterization trick, considering `$z$` as a function of i.i.d. noise and of the output of the encoder.

---

# Denoising Criterion in Variational Framework

With the denoising autoencoder criterion, the input is corrupted according to some noise distribution, and the model needs to learn to reconstruct the original input or maximize the log-probability of the clean input `$x$`, given the corrupted input `$\tilde{x}$`

**Proposition 1.** Let `$q_\phi (z|\tilde{x})$` be a Gaussian distribution such that `$q_\phi (z|\tilde{x}) = N(z|\mu_\phi (\tilde{x}), \sigma_\phi (\tilde{x}))$` where `$\mu_\phi(\tilde{x})$` and `$\sigma_\phi (\tilde{x})$` are non-linear functions of `$\tilde{x}$`. Let `$p(\tilde{x}|x)$` be a known corruption distribution around `$x$`. Then, 
`$$E_{p(\tilde{x}|x)} [q_\phi (z|\tilde{x})] = \int_{\tilde{x}} q_\phi (z|\tilde{x}) p(\tilde{x}|x) d\tilde{x}$$`
is a mixture of Gaussian.

Depending on whether the distribution is over a continuous or discrete variables, the integral in the above Equation can be replaced by a summation. It is instructive to consider the distribution over discrete domain to see that Equation has a form of mixture of Gaussian.

We can see this corruption procedure as adding a stochastic layer to the bottom (early stage of the neural nets) of the inference network. For example, we can define a corruption network `$p_\pi(\tilde{x}|x)$` which is a neural network where the input is `$x$` and the output is stochastic units (e.g., Gaussian or Bernoulli distributions). Then, it is also possible to learn the parameter `$\pi$` of the corruption network by backpropagation using the reparameterization trick. 



```python
# The (almost) only difference between vanilla VAE and denoising VAE is that 
# we need to add noise to the input of the denoising VAE encoder in training time

# Add noise to X		
X_noise = X + noise_factor * tf.random_normal(tf.shape(X))		
X_noise = tf.clip_by_value(X_noise, 0., 1.)		
z_mu, z_logvar = Q(X_noise)

# There no explicit parameter \pi in the implementation. The parameters of the decoder imply param \pi
```

### The Denoising Variational Lower Bound

Integrating the denoising criterion into the variational auto-encoding framework is equivalent to having  a stochastic layer at the bottom of the inference network.

Then estimating the variational lower bound becomes intractable because `$E_{p(\tilde{x}|x)}[q_\phi (z|\tilde{x})]$` requires integrating out the noise `$\tilde{x}$` for a corruption distribution (cuz we don't know the distribution of `$x$`).

**Lemma 1.** *Consider an approximate posterior distribution of the following form:*

<div>$$q_\Phi (z|x) = \int_{z'} q_\varphi (z|z') q_\psi (z'|x) dz'$$</div>

*here, we use `$\Phi = \{\varphi, \psi\}$`. Then, given `$p_\theta (x,z) = p_\theta (x|z) p(z)$`, we obtain the following inequality:*

<div>$$\log p_\theta(x) \ge E_{q_\Phi} (z|x) [\log \frac{p_\theta (x, z)}{q_\varphi (z|z')}] \ge E_{q_\Phi} (z|x) [\log \frac{p_\theta (x, z)}{q_\Phi (z|x)}] $$</div>

Note that `$q_\psi (z'|x)$` can be either parametric or non-parametric distribution.

**Prove**: 

<img src="/image/DVAE_lemma_0.png" style="width:100%" class="leftalign">
<img src="/image/DVAE_lemma_1.png" style="width:100%" class="leftalign">

**Jensen's Inequality**: *Measure-theoretic and probabilistic form*

*Let `$(\Omega, A, \mu)$` be a probability space, such that `$\mu(\Omega) = 1$`. If `$g$` is a real-valued function that is `$\mu-integrable$`, and if `$\varphi$` is a convex function on the real line, then:*

<div>$$\phi(\int_\Omega g\ d\mu) \le \int_\Omega \phi \circ g\ d\mu$$</div>

**Theorem 1.** *Consider an approximate posterior distribution of the following form*

<div>$$q_\Phi (z|x) = \int_{z^1 \dots z^{L-1}} q_{\phi^L} (z|z^{L-1})\dots q_{\phi^1} (z^1|x)\ dz^1 \dots dz^{L-1}$$</div>

*Then, given `$p_\theta (x, z) = p_\theta (x|z) p(z)$`, we obtain the following inequality:*

<div>$$\log p_\theta (x) \ge E_{q_\Phi (z|x)} [\log \frac{p_\theta (x, z)}{\prod^{L-1}_{i=1} q_{\phi^i} (z^{i+1} |z^i)}] \ge E_{q_\Phi (z|x)} [\log \frac{p_\phi (x, z)}{q_{\Phi} (z |z)}]$$</div>

*where `$z=z^L$` and `$x = z^1$`.*

Theorem 1 illustrates that **adding more stochastic layers gives tighter lower bound**.

We now use Lemma 1 to derive the *denoising variational lower bound*. For the approximate distribution `$\tilde{q}_\phi (z|x) = \int q_\phi (z|\tilde{x}) p(\tilde{x}|x) d\tilde{x}$`, we can write the standard variational lower bound as follows:

<div>$$\log p_\theta (x) \ge E_{\tilde{q}_\phi (z|x)} [\log \frac{p_\theta (x, z)}{q_\phi (z|\tilde{x})}] \stackrel{def}{=} \cal{L}_{dvae} \ge E_{\tilde{q}_\phi (z|x)} [\log \frac{p_\theta (x, z)}{\tilde{q}_\phi (z|x)}] \stackrel{def}{=} \cal{L}_{cvae}$$
</div>
Note that the `$p_\theta(x, z)$` in the numerator of the above equation is a function of `$x$` not `$\tilde{x}$`. **That is given corrupted input `$\tilde{x}$` (in the denominator), the `$\cal{L}_{dvae}$` objective tries to reconstruct  the original input `$x$` not the corrupted input `$\tilde{x}$`**. This denoising criterion is different from the popular data augmentation approach where the model tries to reconstruct the corrupted input.

<div>$$\log p_\theta (x) \ge \cal{L}_{dvae} \ge \cal{L}_{cvae}$$</div>

Note that the above does not necessarily mean that `$\cal{L}_{dvae} \ge \cal{L}_{vae}$` where `$\cal{L}_{vae}$` is the lower bound of VAE with Gaussian distribution in the inference network. This is because `$\tilde{q}_\theta (z|x)$` depends on a corruption distribution while `$q_\theta(z|x)$` does not. `$\cal{L}_{dvae}$` can be a tighter lower bound or looser lower bound that `$\cal{L}_{vae}$` (depend on the chosen corruption distribution).

Note also the `$\tilde{q}_\theta (z|x)​$` has the capacity to cover a much broader class of distributions than `$q_\theta (z|x)$`.

For `$\cal{L}_{dvae}$`, it is important to choose a sensible corruption distribution.

**Proposition 2.** *Maximizing `$\cal{L}_{dvae}$` is equivalent to minimizing the following objective

<div>$$E_{p(\tilde{x}|x)} [ KL(\tilde{q}_\phi (z|\tilde{x}) || p(z|x))].$$</div>

*In other words, `$\log p_\theta (x) = \cal{L}_{dvae} + E_{p(\tilde{x}|x)} [ KL(\tilde{q}_\phi (z|\tilde{x}) || p(z|x))]$`.*

This illustrates that maximizing `$\cal{L}_{dvae}$` is equivalent to minimizing the expectation of the KL between the true posterior distribution and approximate posterior distribution *over all noised inputs* from `$p(\tilde{x}|x)$`. **Resulting in a more robust training of the inference network to unseen data points**.



### Training Procedure

The training procedure of DVAE can be seen as a special case of optimizing the following objective which can be easily approximated be Monte Carlo sampling.

<div>$$\cal_{dvae} = E_{q(z|\tilde{x})} E_{p(\tilde{x}|x)} [log \frac{p_\theta (x,z)}{q_\theta (z|\tilde{x})}]  \approx \frac{1}{MK} \sum_{m=1}^M \sum_{k=1}^K \log \frac{p_\theta (x, z^{(k|m)})}{q_\theta (z^{(k|m)} |\tilde{x}^{(m)})}$$</div>

Where `$\tilde{x}^{(m)} \sim p(\tilde{x}|x)$` and `$z^{(k|m)} \sim q_\theta (z|\tilde{x}^{(m)})$`. `$M$` is sample size for each data point `$x$` in the training set. `$K$` is the sample size of samples `$z$`.

# Notes

The model is more sensitive to the *level* of the noise rather than the type. 

The choice of sample sizes `$M$`: increasing the sample size helps to converge faster in terms of the number of epochs and converge ot better log-likelihood. However,  increasing sample size requires more computation. Thus, in practice using `$M=1$` seems a reasonable choice.

# Conclusions

The main result of the paper was to introduce the denoising variational lower bound which, provided a sensible corruption function, can be tighter than the standard variational lower bound on noisy inputs. "We" claimed that **this training criterion makes it possible to learn more flexible and robust approximate posterior distributions such as the misture of Gaussian than the standard training method without corruption** (For example, suppose that `$p(z|x)$` consists of multiple modes. Then, `$\tilde(q)_\theta (z|x$` has the potential of modeling more than a single mode, whhereas it is impossible to model multiple modes of `$p(z|x)$` from `$q_\theta(z|x)$` regardless of which lower bound of `$\log p_\theta (x)$` is used as the objective function) (*Refer to Proposition 1.*)

---

# References

[Denoising Criterion for Variational Auto-Encoding Framework](https://arxiv.org/abs/1511.06406)