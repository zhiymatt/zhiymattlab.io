---
title: Start or Stop lightdm
date: 2019-01-19 20:45:40.923 -0700
categories: [Linux]
tags: [linux, lightdm]
---

LightDM is a cross-desktop display manager, stands for The Light Display Manager.

<!--more-->

```
# to stop
sudo service lightdm stop
# to start
sudo service lightdm start
```