---
title: Restart SSH Server in WSL
date: 2019-02-02 20:22:40.923 -0700
categories: [Windows]
tags: [windows, wsl, ssh]
---

<!--more-->

sudo service ssh --full-restart