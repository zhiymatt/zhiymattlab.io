---
title: Boot a Linux System without Graphical Server
date: 2018-12-27 00:47:40.923 -0700
categories: [Linux]
tags: [linux]
---

Text-only Booting

<!--more-->

At the GRUB2 screen, hit **E**, scroll down to the **linux** line and add **text** / **3** to the end. For example:

```
linux /boot/vmlinuz-~~~ root=UUID=~~~ ro quiet 3
```

Then, hit **F10** or **Ctrl + X** to boot.