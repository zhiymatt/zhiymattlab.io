---
title: Intro to Reinforcement Learning (1)
date: 2018-12-13 00:10:40.923 -0700
categories: [RL]
tags: [rl]
---

Introduction to Reinforcement Learning: Part 1

<!--more-->

# Intro

### RL in an offline setting

In an offline setting, the experience is acquired a priori, then it is used as a **batch** for learning (hence the offline setting is also called **batch RL**).

This is in contrast to the online setting where data becomes available in a **sequencetial order** and is used to progressively update the behavior of the agent.

In both cases, the core learning algorithms are **essentially the same** but the main difference is that in an online setting, the agent can **influence how it gathers experience so that it is the most useful for learning**.
eee
### Exploration / exploitation dilemma

#Formal Framework

### RL setting

Discrete time stochastic control process:

- 5-tuple Markovian (*S*, *A*, *T*, *R*, $\gamma$) $\in$ Markov Decision Process (MDP)

### Categories of policies

- stationary or non-stationary policies
- deterministic or stochastic policies

###