---
title: 在JPanel上显示BufferedImage并持续repaint
date: 2018-11-21 11:09:40.923 -0700
categories: [Java]
tags: [java, swing, ui]
---

用Java Swing，在JPanel上显示BufferedImage，并且在鼠标移动的时候repaint

<!--more-->

# Part 1：在JPanel上显示BufferedImage

``` java
public class YourPanel extends JPanel {
  
    // your other code...

    /** 
     * Draws the component. 
     *
     * @param g  Where to draw to. 
     */
     
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        // do your drawing
        // e.g. - if you have an image that you want to draw...
        // this draws the image at coordinate (0, 0) = upper left corner in your panel
        Image image = ... 
        g.drawImage(image, 0, 0, this);
  }
}
```

把上面的`Image image = ...`改为`BufferedImage image = ...`即可。



# Part 2：在鼠标移动的时候repaint

参考下面`MovingEyes`的代码。

这里主要使用了MVC (model / view / controller) model：

- The view may read values from the model.
- The view *may not* update the model.
- The controller will update the model.
- The controller will repaint / revalidate the view.

**Model**: The Eye class is the model in this simple example. （相当于内部的数据结构）

**View**: The MovingEyes class is the class that defines the JFrame. （用来展示）

**Controller**: The EyeballListener class is the controller class. （控制什么时候repaint）

``` java
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
 
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class MovingEyes implements Runnable {
 
    private static final int drawingWidth = 400;
    private static final int drawingHeight = 400;
    private static final int eyeballHeight = 150;
    private static final int eyeballWidthMargin = 125;
 
    private DrawingPanel drawingPanel;
 
    private Eye[] eyes;
 
    private JFrame frame;
 
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new MovingEyes());
    }
 
    public MovingEyes() {
        this.eyes = new Eye[2];
        this.eyes[0] = new Eye(new Point(eyeballWidthMargin, eyeballHeight));
        this.eyes[1] = new Eye(new Point(drawingWidth - eyeballWidthMargin,
                eyeballHeight));
    }
 
    public void run() {
        frame = new JFrame("Moving Eyes by Gilbert Le Blanc");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 
        drawingPanel = new DrawingPanel(drawingWidth, drawingHeight);
        frame.add(drawingPanel);
 
        frame.setSize(800, 600);
     
        frame.setLocationRelativeTo(null);
     
        // frame.setResizable(false);
        // frame.pack();
        // frame.setLocationByPlatform(true);
        frame.setVisible(true);
    }
 
    public class DrawingPanel extends JPanel {
 
        private static final long serialVersionUID = -2977860217912678180L;
 
        private static final int eyeballOuterRadius = 50;
        private static final int eyeballInnerRadius = 20;
 
        public DrawingPanel(int width, int height) {
            this.addMouseMotionListener(new EyeballListener(this,
                    eyeballOuterRadius - eyeballInnerRadius - 5));
            this.setBackground(Color.WHITE);
            this.setPreferredSize(new Dimension(width, height));
        }
 
        // images
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
 
            g.setColor(Color.BLACK);
 
            for (Eye eye : eyes) {
                drawCircle(g, eye.getOrigin(), eyeballOuterRadius);
                fillCircle(g, eye.getEyeballOrigin(), eyeballInnerRadius);
            }
        }
 
        private void drawCircle(Graphics g, Point origin, int radius) {
            g.drawOval(origin.x - radius, origin.y - radius, radius + radius,
                    radius + radius);
        }
 
        private void fillCircle(Graphics g, Point origin, int radius) {
            g.fillOval(origin.x - radius, origin.y - radius, radius + radius,
                    radius + radius);
        }
 
    }
 
    public class Eye {
        private final Point origin;
        private Point eyeballOrigin;
 
        public Eye(Point origin) {
            this.origin = origin;
            this.eyeballOrigin = origin;
        }
 
        public Point getEyeballOrigin() {
            return eyeballOrigin;
        }
 
        public void setEyeballOrigin(Point eyeballOrigin) {
            this.eyeballOrigin = eyeballOrigin;
        }
 
        public Point getOrigin() {
            return origin;
        }
 
    }
 
    public class EyeballListener extends MouseMotionAdapter {
 
        private final double eyeballDistance;
 
        private final DrawingPanel drawingPanel;
 
        public EyeballListener(DrawingPanel drawingPanel, double eyeballDistance) {
            this.drawingPanel = drawingPanel;
            this.eyeballDistance = eyeballDistance;
        }
 
 
        // mouse detector
        public void mouseMoved(MouseEvent event) {
            Point p = event.getPoint();
            for (Eye eye : eyes) {
                Point origin = eye.getOrigin();
                double theta = Math.atan2((double) (p.y - origin.y),
                        (double) (p.x - origin.x));
                int x = (int) Math.round(Math.cos(theta) * eyeballDistance)
                        + origin.x;
                int y = (int) Math.round(Math.sin(theta) * eyeballDistance)
                        + origin.y;
                eye.setEyeballOrigin(new Point(x, y));
            }
 
            drawingPanel.repaint();
        }
 
    }
 
}
```