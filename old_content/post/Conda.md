---
title: Conda
date: 2019-02-08 00:08:40.923 -0700
categories: [Python]
tags: [python, conda]
---

Some Conda commands

<!--more-->

---



# Update Conda

```sh
conda update conda
conda update anaconda
conda update anaconda-navigator
```



# Envs

```sh
conda create -n xxxx python=3.6
conda remove -n xxxx --all
```



# Clean

```sh
conda clean -p      //删除没有用的包
conda clean -t      //tar打包
conda clean -y -all //删除所有的安装包及cache
```



# Update Package

```sh
conda update xxx   #更新xxx文件包
pip install --upgrade --ignore-installed tensorflow
```

