---
title: Java Diamond Operator
date: 2018-12-24 00:10:40.923 -0700
categories: [Java]
tags: [java]
---

The Diamond Operator reduces some of Java's verbosity surrounding generics by having the compiler [infer parameter types](http://blogs.sun.com/darcy/resource/JavaOne/J1_2009-TS-4060.pdf) for constructors of generic classes. 

<!--more-->

The [original proposal](http://mail.openjdk.java.net/pipermail/coin-dev/2009-February/000009.html) for adding the Diamond Operator to the Java language was made in February 2009 and includes this simple example:

For example, consider the following assignment statement:

```Java
Map<String, List<String>> anagrams = new HashMap<String, List<String>>();
```

This is rather lengthy, so it can be replaced with this:

```Java
Map<String, List<String>> anagrams = new HashMap<>();
```