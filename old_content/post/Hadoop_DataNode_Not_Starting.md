---
title: "Hadoop: DataNode Not Starting"
date: 2019-01-31 18:30:40.923 -0700
categories: [Hadoop]
tags: [hadoop]
---

Because reformatted HDFS while the dfs is running.

<!--more-->

Run "jps" and didn't see a DataNode instance.

Need to start the running dfs and remove dfs data, then restart the hdfs.

Reference:

https://dzone.com/articles/hadoop-datanode-not-starting