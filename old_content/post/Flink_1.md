---
title: Flink Tutorial (1)
date: 2018-12-18 03:42:40.923 -0700
categories: [Flink]
tags: [flink]
---

Flink Tutorial (1): Installation and Demo.

<!--more-->

# Installation

### Install JDK

- install jdk

    ```shell
    sudo apt-get install openjdk-8-jdk
    # not "sudo apt-get install default-jdk",
    # because Flink only support java 8.x
    ```

- [Deepin] remove "Picked up _JAVA_OPTIONS ..." warning:

    ```shell
    sudo vim /etc/profile
    # add "unset _JAVA_OPTIONS" in the bottom
    ```

### Install Flink

- go to https://flink.apache.org/downloads.html

- download latest Hadoop/Scala combination (e.g. Flink 1.7.0 with Hadoop® 2.8 Scala 2.12)

- unzip

    ```shell
    tar xzf flink-*.tgz   # Unpack the downloaded archive
    cd flink-1.7.0
    ```
    


# Demo

### Start a Local Flink Cluster

- start daemon

    ```shell
    ./bin/start-cluster.sh  # Start Flink
    ```

- visit Flink dashboard: http://localhost:8081/, make sure there is a single available TaskManager instance

- check log

    ```shell
    tail log/flink-*-standalonesession-*.log
    ```

### Run a Demo

- check if port 9000 is available
  
    ```shell
    netstat -anlp | grep 9000
    ```

- start local server in terminal tab 1 via

    ```shell
    nc -l -p 9000
    # not "nc -l 9000". The official tutorial misses the "-p" param.
    ```

- check if it's listening

    ```shell
    netstat -anlp | grep 9000
    ```

- Submit the Flink program in terminal tab 2 via

    ```shell
    ./bin/flink run examples/streaming/SocketWindowWordCount.jar --port 9000
    ```

- in terminal tab 1, type:

    ```
    lorem ipsum
    ipsum ipsum ipsum
    bye
    ```

    Words are counted in time windows of 5 seconds (processing time, tumbling windows) and are printed to stdout. Monitor the TaskManager’s output file and write some text in nc (input is sent to Flink line by line after hitting ):

- check the results

    ```shell
    tail -f log/flink-*-taskexecutor-*.out
    # lorem : 1
    # bye : 1
    # ipsum : 4
    ```
    
---
---
---

references:

https://ci.apache.org/projects/flink/flink-docs-release-1.7/tutorials/local_setup.html

