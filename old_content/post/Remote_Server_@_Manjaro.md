---
title: Remote Server @ Manjaro
date: 2018-10-15
categories: [Linux, ML]
tags: [linux, ml, manjaro, ssh]
---

A tutorial on how to turn your PC into a remote server (e.g. jupyter notebook).

<!--more-->

# Part 1: ssh server setting

1. See current sshd servises:

    ```
    ps -ef | grep sshd
    ```

2. Start remote sshd servises:

    ```
    systemctl start sshd.service
    ```

3. Stop remote sshd servises:

    ```
    systemctl stop sshd.service
    ```

4. Change root password:

    ```
    login as root, then "passwd"
    ```

5. Change sshd port:

    ```
    vim /etc/ssh/sshd_config
    ```

6. Show ip address:

    ```
    ip addr
    ```


# Part 2: Remote Jupyter Notebook (server side)

1. Generate config file

    ```
    jupyter notebook --generate-config
    ```

2. Open ipython

    ```
    In [1]: from notebook.auth import passwd
    In [2]: passwd()
    Enter password:
    Verify password:
    Out[2]: 'sha1:xxx:xxx'
    ```
    

3. Edit config

    ```
    vim ~/.jupyter/jupyter_notebook_config.py
    ```

    ```
    c.NotebookApp.ip='*'
    c.NotebookApp.password = u'sha:ce...刚才复制的那个密文'
    c.NotebookApp.open_browser = False
    c.NotebookApp.port =8888 #随便指定一个端口
    ```
    
4. Start Jupyter Notebook Server

    ```
    jupyter notebook
    ```

    注意:如果notebook为5.6.0以下版本,c.NotebookApp.ip = '*'，为5.7.0，则设置为c.NotebookApp.ip = '0.0.0.0'

# Part 3: Remote Jupyter Notebook (client side)

1. SSH to remote jupyter server (bind local 1234 to remote 8888)

    ```
    ssh username@address_of_remote -p port -L 127.0.0.1:1234:127.0.0.1:8888  
    ```
    
    or just open "address_of_remote:port" in browser

# Part 4: Check CUDA

    ```
    import tensorflow as tf

    tf.Session()

    with tf.device('/gpu:0'):
        a = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[2, 3], name='a')
        b = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[3, 2], name='b')
        c = tf.matmul(a, b)

    with tf.Session() as sess:
        print (sess.run(c))

    from tensorflow.python.client import device_lib
    print(device_lib.list_local_devices())
    ```