---
title: tensorflow-gpu @ Manjaro
date: 2018-10-15
categories: [ML]
tags: [ml, gpu, manjaro, tensorflow]
---

A tutorial on how install Tensorflow with GPU support on Manjaro (a dist of ArchLinux).

<!--more-->

1. Download Anaconda for Python3 from official website and install;

    ```
    # After installation, export conda
    source /home/zhiymatt/anaconda3/etc/profile.d/conda.sh
    ```

2. Create conda env and install tensorflow-gpu:

    ```
    conda create -n py36 python=3.6 # tensorflow 1.11 require python 3.6
    conda install tensorflow-gpu # conda will download dependencies: cuda 9.2 & cudnn 7
    ```

3. Install cuda 10 on Manjaro:

    ```
    sudo pacman -S cuda # version 10.x is ok.
    ```

4. Install nvidia gpu driver and bumblebee on Manjaro:

    ```
    sudo mhwd -a pci nonfree 0300 # 0300 means graphical card driver
    ```

5. Enable bumblebeed service, refer to https://wiki.archlinux.org/index.php/Bumblebee#Usage;

6. Reboot system and type "optirun nvidia-smi" to see if the driver has been successfully installed & bumblebeed service is running.

7. run tensorflow/python with nvidia gpu

    ```
    conda activate py36
    optirun python # use optirun to enable gpu for the following process
    ```

8. test tensorflow gpu on iteractive python:

    ```python
    import tensorflow as tf
    
    Using GPUs
    Supported devices
    On a typical system, there are multiple computing devices. In TensorFlow, the supported device types are CPU and GPU. They are represented as strings. For example:
    
    "/cpu:0": The CPU of your machine.
    "/device:GPU:0": The GPU of your machine, if you have one.
    "/device:GPU:1": The second GPU of your machine, etc.
    If a TensorFlow operation has both CPU and GPU implementations, the GPU devices will be given priority when the operation is assigned to a device. For example, matmul has both CPU and GPU kernels. On a system with devices cpu:0 and gpu:0, gpu:0 will be selected to run matmul.
    
    Logging Device placement
    To find out which devices your operations and tensors are assigned to, create the session with log_device_placement configuration option set to True.
    
    # Creates a graph.
    
    a = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[2, 3], name='a')
    b = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[3, 2], name='b')
    c = tf.matmul(a, b)
    
    # Creates a session with log_device_placement set to True.
    
    sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
    
    # Runs the op.
    
    print(sess.run(c))
    ```

9. test tensorflow-gpu on jupyter notebook

    ```
    conda install jupyter # install jupyter for py36 env
    jupyter notebook
    ```

    ```python
    # Creates a graph.
    
    a = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[2, 3], name='a')
    b = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[3, 2], name='b')
    c = tf.matmul(a, b)
    
    # Creates a session with log_device_placement set to True.
    
    sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
    
    # Runs the op.
    
    print(sess.run(c))
    
    # Creates a graph.
    
    a = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[2, 3], name='a')
    b = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[3, 2], name='b')
    c = tf.matmul(a, b)
    
    # Creates a session with log_device_placement set to True.
    
    sess = tf.Session()
    
    # Runs the op.
    
    options = tf.RunOptions(output_partition_graphs=True)
    metadata = tf.RunMetadata()
    c_val = sess.run(c, options=options, run_metadata=metadata)
    
    print(metadata.partition_graphs)
    ```


