---
title: Custom CSS Style & Insert Left-Aligned Image
date: 2018-11-17 21:46:40.923 -0700
categories: [Blog]
tags: [blog, hugo]
---

自定义CSS style，以及插入left-aligned、30%-size的image

<!--more-->

# Step 1: 自定义CSS style

找到`/layouts/partials/head_custom.html`文件。这个文件会导入到每个page的head里，所以可以在里面加入自己的代码：

``` html
<style>

    img.leftalign
    {
        display: inline-block;
        vertical-align: top;
        float: none;
    }
    
</style>
```

也可以把style写到一个外部的CSS文件里面，然后在这个html文件里面link。类似这样：
``` html
<link rel="stylesheet" href="/static/css/style.css">
```

# Step 2: insert image

用html的img tag来插入图片，因为html更好定制。style里只要设置width为30%就行了，height会自动等比例缩放。class设为`leftalign`，图片靠左显示。

```html
<img src="/image/JavaYCXYBYXYXQB.png" style="width:30%" class="leftalign">
```

效果如下：
<img src="/image/JavaYCXYBYXYXQB.png" style="width:30%" class="leftalign">

因为这里的`img.leftalign`用了`display: inline-block`，所以如果想要图片单独占一行的话，需要手动换行。
