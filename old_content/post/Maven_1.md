---
title: Maven Tutorial (1)
date: 2018-12-25 04:08:40.923 -0700
categories: [Java]
tags: [java, maven]
---

Maven Quick Tutorial: common commands, references, ...

<!--more-->

# Common Commands

<p style="margin-left: 24pt;"><span style="color: #c00000;"><span style="font-family: Courier New; font-size: 16pt;"><strong>mvn<span style="color: blue;"> archetype:create </span></strong></span><span style="color: #ff0066;"><strong>创建</strong><span style="color: red;"> <span style="color: #006600;"><strong>Maven 项目</strong><span style="font-family: 宋体;"><br></span><span style="color: #c00000;"><span style="font-family: Courier New; font-size: 16pt;"><strong>mvn<span style="color: blue;"> compile </span></strong></span><span style="color: #ff0066;"><strong>编译<span style="color: #006600;">主程序</span></strong>源代码<span style="color: #c00000;">，<span style="color: #ff0066;"><strong>不会编译<span style="color: #006600;">test目录的源代码</span></strong>。第一次运行时，会下载相关的依赖包，可能会比较费时</span><span style="font-family: 宋体;"><br></span><span style="font-family: Courier New; font-size: 16pt;"><strong>mvn<span style="color: blue;"> test-compile </span></strong></span><span style="color: #ff0066;"><strong>编译<span style="color: #006600;">测试代码</span></strong><span style="color: #c00000;">，</span>compile</span>之后会生成<span style="color: blue;">target文件夹</span>，<span style="color: #ff0066;"><strong>主程序</strong></span>编译在<span style="color: blue;">classes</span>下面，<span style="color: #ff0066;"><strong>测试程序</strong></span>放在<span style="color: blue;">test-classes</span>下</span><span style="font-family: 宋体;"><br></span><span style="color: #c00000;"><strong><span style="font-family: Courier New; font-size: 16pt;">mvn<span style="color: blue;"> test </span></span><span style="color: #ff0066;">运行</span></strong>应用程序中的单元测试</span>
							</span><span style="font-family: 宋体;"><br></span><strong><span style="font-family: Courier New; font-size: 16pt;">mvn<span style="color: blue;"> site </span></span><span style="color: #ff0066;">生成</span></strong>项目相关信息的网站</span></span><span style="font-family: 宋体;"><br></span><span style="color: #c00000;"><strong><span style="font-family: Courier New; font-size: 16pt;">mvn<span style="color: blue;"> clean </span></span><span style="color: #ff0066;">清除</span></strong>目标目录中的生成结果</span></span><span style="font-family: 宋体;"><br></span><span style="color: #c00000;"><strong><span style="font-family: Courier New; font-size: 16pt;">mvn<span style="color: blue;"> package </span></span><span style="color: #ff0066;">依据项目</span></strong>生成<span style="color: red;"> jar 文件</span>，打包之前会进行编译，测试</span><span style="font-family: 宋体;"><br></span><span style="color: #c00000;"><strong><span style="font-family: Courier New; font-size: 16pt;">mvn<span style="color: blue;"> install</span></span><span style="color: #ff0066;">在本地</span></strong> Repository </span>中<span style="color: red;">安装 jar</span><span style="font-family: 宋体;">。</span></span></span><span style="font-family: 宋体;"><br></span><span style="color: #c00000;"><span style="font-family: Courier New; font-size: 16pt;"><strong>mvn<span style="color: blue;"> eclipse:eclipse </span></strong></span><span style="color: #ff0066;"><strong>生成</strong><span style="color: red;">
					<span style="color: #006600;"><strong>Eclipse 项目文件</strong></span>及<span style="color: #006600;"><strong>包引用定义</strong></span><span style="font-family: 宋体;"><br></span><span style="color: #c00000;"><span style="font-family: Courier New; font-size: 16pt;"><strong>mvn<span style="color: blue;"> deploy </span></strong></span>在<span style="color: #ff0066;"><strong>整合</strong></span>或者<span style="color: #ff0066;"><strong>发布</strong></span>环境下执行，将最终版本的包拷贝到远程</span>
				</span>的</span>repository</span>，使得其他的开发者或者工程可以共享。</p>

# References

[Maven简单使用](https://www.cnblogs.com/sunddenly/p/4341542.html)