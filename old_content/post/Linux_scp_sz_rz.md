---
title: "Linux: sz rz sftp scp  "
date: 2019-01-31 18:38:40.923 -0700
categories: [Shell]
tags: [shell, linux, scp, sz, rz, sftp]
---

Linux下几种文件传输命令 sz rz sftp scp

<!--more-->

# sftp

Secure Ftp 是一个基于SSH安全协议的文件传输管理工具。由于它是基于SSH的，会在传输过程中对用户的密码、数据等敏感信息进行加密，因此可以有效的防止用户信息在传输的过程中被窃取，比FTP有更高的安全性。在功能方面与FTP很类似，不仅可以传输文件数据，而且可以进行远程的文件管理（如建立，删除，查看文件列表等操作）。Sftp与ftp虽然只有一字之差，但基于的传输协议却是不同的。因此不能用sftp client去连接ftp server 也不能用 ftp client 去连接 sftp server。

建立连接：sftp user@host

从本地上传文件：put localpath

下载文件：get remotepath

与远程相对应的本地操作，只需要在命令前加上”l” 即可，方便好记。

例如：lcd lpwd lmkdir

# scp

SCP ：secure copy (remote file copy program) 也是一个基于SSH安全协议的文件传输命令。与sftp不同的是，它只提供主机间的文件传输功能，没有文件管理的功能。

复制local_file 到远程目录remote_folder下

scp local_file remote_user@host:remote_folder

复制local_folder 到远程remote_folder（需要加参数 -r 递归）

scp –r local_folder remote_user@host:remote_folder

以上命令反过来写就是远程复制到本地

# sz/rz

sz/rz 是基于ZModem传输协议的命令。对传输的数据会进行核查，并且有很好的传输性能。使用起来更是非常方便，但前提是window端需要有能够支持ZModem的telnet或者SSH客户端，例如secureCRT。

首先需要在secureCRT中可以配置相关的本地下载和上传目录，然后用rz、sz命令即可方便的传输文件数据。

下载数据到本地下载目录：sz filename1 filename2 …

上传数据到远程：执行rz –be 命令，客户端会弹出上传窗口，用户自行选择(可多选)要上传的文件即可。

# References
原文：https://blog.csdn.net/lijianhua_showit/article/details/24292903 