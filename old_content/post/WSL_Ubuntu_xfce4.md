---
title: Ubuntu@WSL with GUI
date: 2018-12-20 23:08:40.923 -0700
categories: [Linux, Windows]
tags: [linux, windows, ubuntu, xfce4, wsl]
---

install Ubuntu Windows subsystem with xfce4 GUI

<!--more-->

---

# Install WSL/Ubuntu and xfce4

The tutorial will tell you how to run desktop environment inside Windows Subsystem for Linux. And you don't need to build a developement environment with virtual machines any more. :)

### Screenshot

<img src="/image/wsl_tutorial/wsl.png" style="width:100%" class="leftalign">

#### Prerequisites

Your PC must be running a 64-bit version of **Windows 10 Anniversary Update build 14393 or later**.

To find your PC's CPU architecture and Windows version/build number, open **Settings>System>About**. Look for the OS Build and System Type fields.

<img src="/image/wsl_tutorial/system.png" style="width:100%" class="leftalign">

### Installation

In order to run Bash on Windows, you will need to manually:

#### 1.Turn-on Developer Mode

<img src="/image/wsl_tutorial/developer.png" style="width:100%" class="leftalign">

#### 2.Enable the “Windows Subsystem for Linux (beta)” feature

<img src="/image/wsl_tutorial/windows_features.png" style="width:100%" class="leftalign">

### After enabling Windows Subsystem for Linux

#### 1.Restart your computer

#### 2.Run bash

<img src="/image/wsl_tutorial/bash.png" style="width:100%" class="leftalign">

After you have accepted the License, the Ubuntu user-mode image will be downloaded and a “Bash on Ubuntu on Windows” shortcut will be added to your start menu.

### Install VcXsrv

Install the lastest version of [VcXsrv](https://sourceforge.net/projects/vcxsrv/).

### Upgrade ubuntu

```bash
sudo apt-get update
sudo apt-get upgrade
```

### Install xfce desktop

```bash
sudo apt-get install xfce4-terminal
sudo apt-get install xfce4
```

### Specify the display server

Add DISPLAY=:0.0 to your `~/.bashrc`, and don't forget to run `source ~/.bashrc`. :)

```bash
export DISPLAY=:0.0
export LIBGL_ALWAYS_INDIRECT=1
```

### Open display server

Open **XLaunch**, choose “One large window” or “One large window without titlebar” and set the “display number” to 0.
Other settings leave as default and finish the configuration.

<img src="/image/wsl_tutorial/vcxsrv.png" style="width:100%" class="leftalign">

### Run xfce desktop

Execute the following command inside “Bash on Ubuntu on Windows”.

```bash
startxfce4
```

### Fix powerline fonts rendering

Install the lastest version of [Hack](https://github.com/source-foundry/Hack#linux) fonts.

### Fix Unicode fonts rendering

```bash
sudo apt-get install fonts-noto
sudo apt-get install fonts-noto-hinted
sudo apt-get install fonts-noto-mono
sudo apt-get install fonts-noto-unhinted
```

### Fix Chinese fonts rendering

```bash
sudo apt-get install fonts-noto-cjk
```

### Fix mkdir command has wrong permissions

Add the following shell code to your bashrc

```bash
if grep -q Microsoft /proc/version; then
    if [ "$(umask)" == '0000' ]; then
        umask 0022
    fi
fi
```

### Install Chinese input method

#### 1.Install fcitx

```bash
sudo apt-get install fcitx
sudo apt-get install fcitx-pinyin
```

#### 2.Add the following command to your bashrc file

```bash
export XMODIFIERS=@im=fcitx
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
```

#### 3.Relogin

## Install drop-down terminal

```bash
sudo apt-get install guake
```

### How to shutdown wsl

#### 1.Close VcXsrv

#### 2.Exit “Bash on Ubuntu on Windows”

### References

- [wsl-tutorial](https://github.com/QMonkey/wsl-tutorial)
- [Microsoft wsl install guide](https://msdn.microsoft.com/en-us/commandline/wsl/install_guide)
- [Run any Desktop Environment in WSL](https://github.com/Microsoft/BashOnWindows/issues/637)
- [Bash on windows getting dbus and x server working](https://www.reddit.com/r/Windows10/comments/4rsmzp/bash_on_windows_getting_dbus_and_x_server_working/)

---

# Customize xfce4

#### Adapta Theme

```bash
sudo add-apt-repository -y ppa:tista/adapta
sudo apt update
sudo apt install adapta-gtk-theme
```

#### fonts

```bash
sudo apt install fonts-roboto
sudo apt install fonts-noto
```

set fonts in "settings/Appearance/Style"

#### Papirus Icons

```bash
sudo add-apt-repository -y ppa:papirus/papirus
sudo apt update
sudo apt install papirus-icon-theme
```

to remove the PPA

```bash
sudo add-apt-repository --remove ppa:papirus/papirus
```

#### Tilix Terminal Emulator

```bash
sudo apt install tilix
```

#### Adding a startup terminal

If you want to open a terminal when Xfce desktop loads up, you can add an item for launching a terminal in [ Applications ] » [ Settings ] » [ Session and Startup ].

#### References:

- [customizing-xfce-desktop-for-ubuntu-wsl](https://token2shell.com/howto/x410/customizing-xfce-desktop-for-ubuntu-wsl)

---

# Install CH Language Support and fcitx/sogoupinyin

#### install CH language pack and font-manager

```bash
sudo apt-get install language-pack-zh-hans
sudo apt-get install font-manager
```

#### config language environment

```bash
sudo fc-cache  -fv
sudo dpkg-reconfigure locales # choose zh_CN.UTF-8
```

#### install fcitx/sogoupinyin

```bash
sudo apt install fcitx
sudo apt-get -f install # repair dependency
dpkg -i sogoupinyin_xxx_amd64.deb # download and install sogoupinyin
```

#### References

- [wsl-xfce](https://hpdell.github.io/uncategorized/wsl-xfce/)

---


# VCXSrv Setting

My **WSL_Ubuntu_xfce4.xlaunch**

```
<?xml version="1.0" encoding="UTF-8"?>
<XLaunch WindowMode="Nodecoration" ClientMode="NoClient" LocalClient="False" Display="0" LocalProgram="xcalc" RemoteProgram="xterm" RemotePassword="" PrivateKey="" RemoteHost="" RemoteUser="" XDMCPHost="" XDMCPBroadcast="False" XDMCPIndirect="False" Clipboard="True" ClipboardPrimary="True" ExtraParams="-keyhook" Wgl="True" DisableAC="False" XDMCPTerminate="False"/>
```

Other Params

```
# It looks like -multiwindow mode triggers the static color visual in both the internal x2go xserver and the external vcxsrv
# But it also looks like -multiwindow mode is how x2go client allows resizing of the remote desktop
# ...so I'm looking for a way to allow resizing of remote desktop w/out triggering static color visual.
# 
# ...output of vcxsrv.exe's usage note:

Usage...
Vcxsrv [:<display>] [option]

:display-number
	Vcxsrv runs as the given display-number, which defaults to 0.
	To run multiple instances, use unique display-numbers.

-a #                   default pointer acceleration (factor)
-ac                    disable access control restrictions
-audit int             set audit trail level
-auth file             select authorization file
-br                    create root window with black background
+bs                    enable any backing store support
-bs                    disable any backing store support
-cc int                default color visual class
-nocursor              disable the cursor
-core                  generate core dump on fatal error
-dpi [auto|int]        screen resolution set to native or this dpi
-dpms                  disables VESA DPMS monitor control
-deferglyphs [none|all|16] defer loading of [no|all|16-bit] glyphs
-f #                   bell base (0-100)
-fc string             cursor font
-fn string             default font name
-fp string             default font path
-help                  prints message with these options
-I                     ignore all remaining arguments
-nolisten string       don't listen on protocol
-noreset               don't reset after last client exists
-background [none]     create root window with no background
-reset                 reset after last client exists
-pn                    accept failure to listen on all ports
-nopn                  reject failure to listen on all ports
-r                     turns off auto-repeat
r                      turns on auto-repeat 
-render [default|mono|gray|color] set render color alloc policy
-retro                 start with classic stipple
-seat string           seat to run on
-t #                   default pointer threshold (pixels/t)
-terminate             terminate at server reset
-to #                  connection time out
-tst                   disable testing extensions
-wm                    WhenMapped default backing-store
-wr                    create root window with white background
+xinerama              Enable XINERAMA extension
-xinerama              Disable XINERAMA extension
-dumbSched             Disable smart scheduling, enable old behavior
-schedInterval int     Set scheduler interval in msec
+extension name        Enable extension
-extension name        Disable extension
-query host-name       contact named host for XDMCP
-broadcast             broadcast for XDMCP
-multicast [addr [hops]] IPv6 multicast for XDMCP
-indirect host-name    contact named host for indirect XDMCP
-port port-num         UDP port number to send messages to
-from local-address    specify the local address to connect from
-once                  Terminate server after one session
-class display-class   specify display class to send in manage
-cookie xdm-auth-bits  specify the magic cookie for XDMCP
-displayID display-id  manufacturer display ID for request
[+-]accessx [ timeout [ timeout_mask [ feedback [ options_mask] ] ] ]
                       enable/disable accessx key sequences
-ardelay               set XKB autorepeat delay
-arinterval            set XKB autorepeat interval


VcXsrv Device Dependent Usage:

-[no]clipboard
	Enable [disable] the clipboard integration. Default is enabled.
-[no]clipboardprimary
	[Do not] map the PRIMARY selection to the windows clipboard.
	The CLIPBOARD selection is always mapped if -clipboard is enabled.
	Default is mapped.
-clipupdates num_boxes
	Use a clipping region to constrain shadow update blits to
	the updated region when num_boxes, or more, are in the
	updated region.
-depth bits_per_pixel
	Specify an optional bitdepth to use in fullscreen mode
	with a DirectDraw engine.
-[no]emulate3buttons [timeout]
	Emulate 3 button mouse with an optional timeout in
	milliseconds.
-engine engine_type_id
	Override the server's automatically selected engine type:
		1 - Shadow GDI
		2 - Shadow DirectDraw
		4 - Shadow DirectDraw4 Non-Locking
-fullscreen
	Run the server in fullscreen mode.
-hostintitle
	In multiwindow mode, add remote host names to window titles.
-ignoreinput
	Ignore keyboard and mouse input.
-[no]keyhook
	Grab special Windows keypresses like Alt-Tab or the Menu key.
-lesspointer
	Hide the windows mouse pointer when it is over any
	VcXsrv window.  This prevents ghost cursors appearing when
	the Windows cursor is drawn on top of the X cursor
-logfile filename
	Write log messages to <filename>.
-logverbose verbosity
	Set the verbosity of log messages. [NOTE: Only a few messages
	respect the settings yet]
		0 - only print fatal error.
		1 - print additional configuration information.
		2 - print additional runtime information [default].
		3 - print debugging and tracing information.
-[no]multimonitors or -[no]multiplemonitors
	Use the entire virtual screen if multiple
	monitors are present.
-multiwindow
	Run the server in multi-window mode.
-nodecoration
	Do not draw a window border, title bar, etc.  Windowed
	mode only.
-nounicodeclipboard
	Do not use Unicode clipboard even if on a NT-based platform.
-refresh rate_in_Hz
	Specify an optional refresh rate to use in fullscreen mode
	with a DirectDraw engine.
-resize=none|scrollbars|randr	In windowed mode, [don't] allow resizing of the window. 'scrollbars'
	mode gives the window scrollbars as needed, 'randr' mode uses the RANR
	extension to resize the X screen.  'randr' is the default.
-rootless
	Run the server in rootless mode.
-screen scr_num [width height [x y] | [[WxH[+X+Y]][@m]] ]
	Enable screen scr_num and optionally specify a width and
	height and initial position for that screen. Additionally
	a monitor number can be specified to start the server on,
	at which point, all coordinates become relative to that
	monitor. Examples:
	 -screen 0 800x600+100+100@2 ; 2nd monitor offset 100,100 size 800x600
	 -screen 0 1024x768@3        ; 3rd monitor size 1024x768
	 -screen 0 @1 ; on 1st monitor using its full resolution (the default)
-silent-dup-error
	If another instance of VcXsrv with the same display number is running
	exit silently and don't display any error message.
-swcursor
	Disable the usage of the Windows cursor and use the X11 software
	cursor instead.
-[no]trayicon
	Do not create a tray icon.  Default is to create one
	icon per screen.  You can globally disable tray icons with
	-notrayicon, then enable it for specific screens with
	-trayicon for those screens.
-[no]unixkill
	Ctrl+Alt+Backspace exits the X Server.
-[no]wgl
	Enable the GLX extension to use the native Windows WGL interface for hardware-accelerated OpenGL
-swrastwgl
	Enable the GLX extension to use the native Windows WGL interface based on the swrast interface for accelerated OpenGL
-[no]winkill
	Alt+F4 exits the X Server.
-xkblayout XKBLayout
	Equivalent to XKBLayout in XF86Config files.
	For example: -xkblayout de
-xkbmodel XKBModel
	Equivalent to XKBModel in XF86Config files.
-xkboptions XKBOptions
	Equivalent to XKBOptions in XF86Config files.
-xkbrules XKBRules
	Equivalent to XKBRules in XF86Config files.
-xkbvariant XKBVariant
	Equivalent to XKBVariant in XF86Config files.
	For example: -xkbvariant nodeadkeys
(EE) Server terminated successfully (0). Closing log file.
```

#### References

- [VCXSrv.0](https://gist.github.com/stowler/9921780)

---

# Other Configs

#### 设置启动项：fcitx、xfce4-panel

---

# Other Problems

每次启动xfce4都弹出对话框，提示：没有发现正在运行的xfce4-panel实例。

然后弹出对话框，提示：因为面板运行于全屏模式下，所以作为普通用户，您不被允许修改面板配置。

删除~/.config中相关目录后仍然出现上面的提示。

虽然退出时选择了保存会话(session)，但下次启动xfce4时，使用的仍是~/.cache/sessions/中上次那个没有面板的session。

将~/.cache/sessions/里面的文件删除即可。

#### References

http://www.voidcn.com/article/p-chfvvjpb-bkv.html

https://www.reddit.com/r/bashonubuntuonwindows/comments/7s3jav/on_high_resolution_monitor_x_apps_are_blurry/