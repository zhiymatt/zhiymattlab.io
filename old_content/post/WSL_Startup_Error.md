---
title: WSL/Ubuntu Startup Error
date: 2018-12-23 23:08:40.923 -0700
categories: [Windows]
tags: [windows, wsl]
---

Fix "WslRegisterDistribution failed with error: 0x800703fa"

<!--more-->

Open CMD/Powershell:

```bash
sc stop LxssManager
sc start LxssManager
```