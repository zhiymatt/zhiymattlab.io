---
title: Cite Github Repository @ LaTeX [BibTeX]
date: 2018-12-11 12:08:40.923 -0700
categories: [Latex]
tags: [latex]
---

<span></span>

<!--more-->

And respective BibTeX entry:

```

@misc{Charles2013,
  author = {Charles, P.W.D.},
  title = {Project Title},
  year = {2013},
  publisher = {GitHub},
  journal = {GitHub repository},
  howpublished = {\url{https://github.com/charlespwd/project-title}},
  commit = {4f57d6a0e4c030202a07a60bc1bb1ed1544bf679}
}

```

