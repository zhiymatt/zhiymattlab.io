---
title: Hosts (Windows)
date: 2018-12-18 22:08:40.923 -0700
categories: [Windows]
tags: [windows, hosts]
---

Fix 0x80072F7D error @ Windows Store by modifying hosts file

<!--more-->

Add "23.56.181.94 storeedgefd.dsx.mp.microsoft.com" to "C:\Windows\System32\drivers\etc\hosts".

在Proxy Setting里面，关闭“使用设置脚本”