---
title: Java Syntax
date: 2019-02-06 22:22:40.923 -0700
categories: [Java]
tags: [java]
---

<span></span>

<!--more-->

检查一个引用是不是指向null：

```java
if (ref == null) {
    //do something
}
```

Java不会自动cast。



---



Python的逻辑运算和Java的对应:

| Python | Java |
| ------ | ---- |
| and    | &&   |
| or     | \|\| |
| not    | !    |



---



这种写法，默认 collection 里面的元素类型是 Object （所有 Java 类的祖先）：

```java
SortedSet set = new TreeSet(); 
```



---



Iterator 用法（*.hasNext()*，*.next()*）：

```java
Iterator it = set.iterator();
while (it.hasNext()) {
    Object element = it.next();
    // do something
}
```



---



```java
(Math.max(12.123, 12.456));
```



---



Java 1.5 以下，不能直接往 Collections 中放入原始类型值（char、int），因为集合只接受对象。因此，需要将这些原始类型的值转换成对象（Character、Integer），然后将这些转换的对象放入集合中。

为了让代码简练，Java 1.5引入了具有在原始类型和对象类型自动转换的装箱和拆箱机制。

```java
Map<Integer, Integer> map = new HashMap<>();

int i = 0;
int j = 100;

map.put(i, j);
```



---



检查一个字符串是不是null或为空


```java
String s = /* whatever */;

if ("".equals(s)) {
    // it's null or empty
}

if (s == null || s.isEmpty())
{
    // it's null or empty
}
```