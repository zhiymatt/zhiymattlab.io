---
title: Java Exceptions
date: 2019-02-06 21:05:40.923 -0700
categories: [Java]
tags: [java, exception]
---

e.g.: 
```java
throw new IllegalArgumentException("info");
```

<!--more-->

# Class IllegalArgumentException

Thrown to indicate that a method has been passed an illegal or inappropriate argument.

```java
public class IllegalArgumentException extends RuntimeException
```

