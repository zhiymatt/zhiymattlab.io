---
title: Java入门（2）
date: 2018-11-15 05:15:40.923 -0700
categories: [Java]
tags: [java]
---



Java 入门，part 2

<!--more-->



# Java 变量类型

Java支持的变量类型：

- 类变量：独立于方法之外，且用static修饰
- 实例变量：独立于方法之外，不过没有static修饰
- 局部变量：类的方法里的变量

### 局部变量

- 访问修饰符不可用于局部变量
- 局部变量在栈上分配
- 局部变量没有默认值；被声明后，必须经过初始化才可以被使用（否则会在编译时报错）

### 实例变量

- 实例变量可以声明在使用前或者使用后
- 访问修饰符可修饰实例变量
- 一般情况下应该把实例变量设为私有。通过使用访问修饰符可以使实例变量对子类可见
- 实例变量具有默认值。数值型变量的默认值是0，布尔型变量的默认值是false，引用类型变量的默认值是null。变量的值可以在声明时指定，也可以在构造方法中指定
- 实例变量可以直接通过变量名访问。但在静态方法以及其他类中，就应该使用完全限定名

### 类变量（静态变量）

- 类变量即静态变量，在类中以static声明

- 无论一个类创建了多少个对象，类只拥有类变量的一份拷贝

- 静态变量除了被声明为常量外很少使用。常量是声明为public/private，final和static类型的变量。常量初始化之后不可改变

- 静态变量储存在静态存储区。经常被声明为常量，很少单独用static声明变量；如一般`public static final PI = 3.14159`。如果是常量，一般用全大写

- 为了对类的使用者可见，一般声明为public

- 默认值和实例变量类似

---



# Java 修饰符

主要有两类修饰符，访问修饰符、非访问修饰符

### 访问控制修饰符

Java中，可以使用访问控制符来保护对类、变量、方法和构造方法的访问

- **default** (即缺省）: 在同一包内可见，不使用任何修饰符。使用对象：类、接口、变量、方法。
- **private** : 在同一类内可见。使用对象：变量、方法。 **注意：不能修饰类（外部类）**。要用公共的getter、setter方法被外部类访问
- **public** : 对所有类可见。使用对象：类、接口、变量、方法
- **protected** : 对同一包内的类和所有子类可见。使用对象：变量、方法。 **注意：不能修饰类（外部类）**。

我们可以通过以下表来说明访问权限：

| 修饰符      | 当前类 | 同一包内 | 子孙类(同一包) | 子孙类(不同包) | 其他包 |
| ----------- | ------ | -------- | -------------- | -------------- | ------ |
| `public`    | Y      | Y        | Y              | Y              | Y      |
| `protected` | Y      | Y        | Y              | Y/N \*         | N      |
| `default`   | Y      | Y        | Y              | N              | N      |
| `private`   | Y      | N        | N              | N              | N      |

\*: 子类与基类在同一包中：被声明为 protected 的变量、方法和构造器能被同一个包中的任何其他类访问；子类与基类不在同一包中：那么在子类中，子类实例可以访问其从基类继承而来的 protected 方法，而不能访问基类实例的protected方法。

### 访问控制和继承

 请注意以下方法继承的规则： 

- 父类中声明为 public 的方法在子类中也必须为 public
- 父类中声明为 protected 的方法在子类中要么声明为 protected，要么声明为 public，不能声明为 private
- 父类中声明为 private 的方法，不能够被继承

### 非访问修饰符

- static 修饰符
- final 修饰符
  - 被final修饰的实例变量必须显式指定初始值
  - final 方法可以被子类继承，但不能被子类修改。主要目的是防止该方法的内容被修改
  - final 类不能被继承
- abstract 修饰符
  - 抽象类不能用来实例化对象，声明抽象类的唯一目的是为了未来对该类进行扩充
  - abstract和final不能一起用来修饰一个类
  - 变量不可以是abstract的；方法可以是abstract的，也可以不是abstract的；如`abstract void m(); // 没有body`
- synchronized 修饰符
  - synchronized声明的方法同一时间只能被一个线程访问
- transient 修饰符
  - 序列化的对象包含被transient修饰的实例变量时，JVM跳过该特定的变量
- volatile 修饰符
  - volatile 修饰的成员变量在每次被线程访问时，都强制从共享内存中重新读取该成员变量的值。而且，当成员变量发生变化时，会强制线程将变化值回写到共享内存。这样在任何时刻，两个不同的线程总是看到某个成员变量的同一个值
  - 一个 volatile 对象引用可能是 null



---



### Java 运算符

| 操作符 | 描述                                                         | 例子                           |
| ------ | ------------------------------------------------------------ | ------------------------------ |
| <<     | 按位左移运算符。左操作数按位左移右操作数指定的位数。         | A << 2得到240，即 1111 0000    |
| >>     | 按位右移运算符。左操作数按位右移右操作数指定的位数。         | A >> 2得到15即 1111            |
| >>>    | 按位右移补零操作符。左操作数的值按右操作数指定的位数右移，移动得到的空位以零填充。 | A>>>2得到15即0000 1111         |

##### 条件运算符（?:）

##### instanceof 运算符

```java
( Object reference variable ) instanceof  (class/interface type)

String name = "James";
boolean result = name instanceof String; // 由于 name 是 String 类型，所以返回真
```



---



# Java 循环

``` java
while( 布尔表达式 ) {
  //循环内容
}
```

``` java
do {
       //代码语句
}while(布尔表达式);
```

``` java
for(初始化; 布尔表达式; 更新) {
    //代码语句
}
```

```java
for(声明语句 : 表达式)
{
   //代码句子
}

/* 
声明语句：声明新的局部变量，该变量的类型必须和数组元素的类型匹配。其作用域限定在循环语句块，其值与此时数组元素的值相等。
表达式：表达式是要访问的数组名，或者是返回值为数组的方法。
*/

String [] names ={"James", "Larry", "Tom", "Lacy"};
for( String name : names ) {
    System.out.print( name );
    System.out.print(",");
}
```

- break
- continue



---



# Java 条件语句

- if
- else if
- else



# Java switch case

``` java
switch(expression){
    case value :
       //语句
       break; //可选
    case value :
       //语句
       break; //可选
    //你可以有任意数量的case语句
    default : //可选
       //语句
}
```

- switch 语句中的变量类型可以是： byte、short、int 或者 char。从 Java SE 7 开始，switch 支持字符串 String 类型了，同时 case 标签必须为字符串常量或字面量
- switch 语句可以拥有多个 case 语句。每个 case 后面跟一个要比较的值和冒号
- case 语句中的值的数据类型必须与变量的数据类型相同，而且只能是常量或者字面常量
- 当变量的值与 case 语句的值相等时，那么 case 语句之后的语句开始执行，直到 **break** 语句出现才会跳出 switch 语句。
- 当遇到 break 语句时，switch 语句终止。程序跳转到 switch 语句后面的语句执行。case 语句不必须要包含 break 语句。如果没有 break 语句出现，程序会继续执行下一条 case 语句，直到出现 break 语句。
- switch 语句可以包含一个 default 分支，该分支一般是 switch 语句的最后一个分支（可以在任何位置，但建议在最后一个）。default 在没有 case 语句的值和变量值相等的时候执行。default 分支不需要 break 语句。

**switch case 执行时，一定会先进行匹配，匹配成功返回当前 case 的值，再根据是否有 break，判断是否继续输出，或是跳出判断。**

``` java
switch(grade)
{
    case 'A' :
        System.out.println("优秀"); 
        break;
    case 'B' :
    case 'C' :
        System.out.println("良好");
        break;
    case 'D' :
        System.out.println("及格");
    case 'F' :
        System.out.println("你需要再努力努力");
        break;
    default :
        System.out.println("未知等级");
}
```



---



# Java Number & Math 类

- number
  - 一般地，当需要使用数字的时候，我们通常使用内置数据类型，如：**byte、int、long、double** 等。 
  - 所有的包装类**（Integer、Long、Byte、Double、Float、Short）**都是抽象类 Number 的子类。
  - 这种由编译器特别支持的包装称为装箱，所以当内置数据类型被当作对象使用的时候，编译器会把内置类型装箱为包装类。相似的，编译器也可以把一个对象拆箱为内置类型。Number 类属于 java.lang 包。

- Math

  - Java 的 Math 包含了用于执行基本数学运算的属性和方法，如初等指数、对数、平方根和三角函数。

  - Math 的方法都被定义为 static 形式，通过 Math 类可以在主函数中直接调用。

```
1 	xxxValue()
将 Number 对象转换为xxx数据类型的值并返回。
2 	compareTo()
将number对象与参数比较。
3 	equals()
判断number对象是否与参数相等。
4 	valueOf()
返回一个 Number 对象指定的内置数据类型
5 	toString()
以字符串形式返回值。
6 	parseInt()
将字符串解析为int类型。
7 	abs()
返回参数的绝对值。
8 	ceil()
返回大于等于( >= )给定参数的的最小整数。
9 	floor()
返回小于等于（<=）给定参数的最大整数 。
10 	rint()
返回与参数最接近的整数。返回类型为double。
11 	round()
它表示四舍五入，算法为 Math.floor(x+0.5)，即将原来的数字加上 0.5 后再向下取整，所以，Math.round(11.5) 的结果为12，Math.round(-11.5) 的结果为-11。
12 	min()
返回两个参数中的最小值。
13 	max()
返回两个参数中的最大值。
14 	exp()
返回自然数底数e的参数次方。
15 	log()
返回参数的自然数底数的对数值。
16 	pow()
返回第一个参数的第二个参数次方。
17 	sqrt()
求参数的算术平方根。
18 	sin()
求指定double类型参数的正弦值。
19 	cos()
求指定double类型参数的余弦值。
20 	tan()
求指定double类型参数的正切值。
21 	asin()
求指定double类型参数的反正弦值。
22 	acos()
求指定double类型参数的反余弦值。
23 	atan()
求指定double类型参数的反正切值。
24 	atan2()
将笛卡尔坐标转换为极坐标，并返回极坐标的角度值。
25 	toDegrees()
将参数转化为角度。
26 	toRadians()
将角度转换为弧度。
27 	random()
返回一个随机数。
```



---



# Java Character类

``` Java
char ch = 'a';
or
Character ch = new Character('a');
```

 在某些情况下，Java编译器会自动创建一个Character对象。

例如，将一个char类型的参数传递给需要一个Character类型参数的方法时，那么编译器会自动地将char类型参数转换为Character对象。 这种特征称为装箱，反过来称为拆箱。 

```java
// 装箱
Character ch = 'a';
```

``` 
.isLetter()
.isDigit()
.isWhitespace()
.isUpperCase()
.isLowerCase()
.toUpperCase()
.toLowerCase()
.toString() // 返回长为1的字符串
```



---



# Java String类

### 创建字符串

```java
String greeting = "Hello";
```

``` java
char[] helloArray = { 'H', 'e', 'l', 'l', 'o'};
String helloString = new String(helloArray);  
```

**String 类是不可改变的。一旦创建了String对象，它的值就无法在改变。如果需要对字符串做很多修改，那么可以用 StringBuffer & StringBuilder 类。**

### 获取长度

```java
String hello = "Hello, world!";
int len = hello.length();
```

### concat

```java
str1.concat(str2)
or
str1 + str2
```

### 格式化字符串

``` java
String fs;
fs = String.format("浮点型变量的值为 " +
                   "%f, 整型变量的值为 " +
                   " %d, 字符串变量的值为 " +
                   " %s", floatVar, intVar, stringVar);
```

### 方法

```
1 	char charAt(int index)
返回指定索引处的 char 值。
2 	int compareTo(Object o)
把这个字符串和另一个对象比较。
3 	int compareTo(String anotherString)
按字典顺序比较两个字符串。
4 	int compareToIgnoreCase(String str)
按字典顺序比较两个字符串，不考虑大小写。
5 	String concat(String str)
将指定字符串连接到此字符串的结尾。
6 	boolean contentEquals(StringBuffer sb)
当且仅当字符串与指定的StringBuffer有相同顺序的字符时候返回真。
7 	static String copyValueOf(char[] data)
返回指定数组中表示该字符序列的 String。
8 	static String copyValueOf(char[] data, int offset, int count)
返回指定数组中表示该字符序列的 String。
9 	boolean endsWith(String suffix)
测试此字符串是否以指定的后缀结束。
10 	boolean equals(Object anObject)
将此字符串与指定的对象比较。
11 	boolean equalsIgnoreCase(String anotherString)
将此 String 与另一个 String 比较，不考虑大小写。
12 	byte[] getBytes()
 使用平台的默认字符集将此 String 编码为 byte 序列，并将结果存储到一个新的 byte 数组中。
13 	byte[] getBytes(String charsetName)
使用指定的字符集将此 String 编码为 byte 序列，并将结果存储到一个新的 byte 数组中。
14 	void getChars(int srcBegin, int srcEnd, char[] dst, int dstBegin)
将字符从此字符串复制到目标字符数组。
15 	int hashCode()
返回此字符串的哈希码。
16 	int indexOf(int ch)
返回指定字符在此字符串中第一次出现处的索引。
17 	int indexOf(int ch, int fromIndex)
返回在此字符串中第一次出现指定字符处的索引，从指定的索引开始搜索。
18 	int indexOf(String str)
 返回指定子字符串在此字符串中第一次出现处的索引。
19 	int indexOf(String str, int fromIndex)
返回指定子字符串在此字符串中第一次出现处的索引，从指定的索引开始。
20 	String intern()
 返回字符串对象的规范化表示形式。
21 	int lastIndexOf(int ch)
 返回指定字符在此字符串中最后一次出现处的索引。
22 	int lastIndexOf(int ch, int fromIndex)
返回指定字符在此字符串中最后一次出现处的索引，从指定的索引处开始进行反向搜索。
23 	int lastIndexOf(String str)
返回指定子字符串在此字符串中最右边出现处的索引。
24 	int lastIndexOf(String str, int fromIndex)
 返回指定子字符串在此字符串中最后一次出现处的索引，从指定的索引开始反向搜索。
25 	int length()
返回此字符串的长度。
26 	boolean matches(String regex)
告知此字符串是否匹配给定的正则表达式。
27 	boolean regionMatches(boolean ignoreCase, int toffset, String other, int ooffset, int len)
测试两个字符串区域是否相等。
28 	boolean regionMatches(int toffset, String other, int ooffset, int len)
测试两个字符串区域是否相等。
29 	String replace(char oldChar, char newChar)
返回一个新的字符串，它是通过用 newChar 替换此字符串中出现的所有 oldChar 得到的。
30 	String replaceAll(String regex, String replacement)
使用给定的 replacement 替换此字符串所有匹配给定的正则表达式的子字符串。
31 	String replaceFirst(String regex, String replacement)
 使用给定的 replacement 替换此字符串匹配给定的正则表达式的第一个子字符串。
32 	String[] split(String regex)
根据给定正则表达式的匹配拆分此字符串。
33 	String[] split(String regex, int limit)
根据匹配给定的正则表达式来拆分此字符串。
34 	boolean startsWith(String prefix)
测试此字符串是否以指定的前缀开始。
35 	boolean startsWith(String prefix, int toffset)
测试此字符串从指定索引开始的子字符串是否以指定前缀开始。
36 	CharSequence subSequence(int beginIndex, int endIndex)
 返回一个新的字符序列，它是此序列的一个子序列。
37 	String substring(int beginIndex)
返回一个新的字符串，它是此字符串的一个子字符串。
38 	String substring(int beginIndex, int endIndex)
返回一个新字符串，它是此字符串的一个子字符串。
39 	char[] toCharArray()
将此字符串转换为一个新的字符数组。
40 	String toLowerCase()
使用默认语言环境的规则将此 String 中的所有字符都转换为小写。
41 	String toLowerCase(Locale locale)
 使用给定 Locale 的规则将此 String 中的所有字符都转换为小写。
42 	String toString()
 返回此对象本身（它已经是一个字符串！）。
43 	String toUpperCase()
使用默认语言环境的规则将此 String 中的所有字符都转换为大写。
44 	String toUpperCase(Locale locale)
使用给定 Locale 的规则将此 String 中的所有字符都转换为大写。
45 	String trim()
返回字符串的副本，忽略前导空白和尾部空白。
46 	static String valueOf(primitive data type x)
返回给定data type类型x参数的字符串表示形式。
```



---



# Java StringBuffer 和 StringBuilder 类

和 String 类不同的是，StringBuffer 和 StringBuilder 类的对象能够被多次的修改，并且不产生新的未使用对象。

StringBuilder 类在 Java 5 中被提出，它和 StringBuffer 之间的最大不同在于 StringBuilder 的方法不是**线程安全**的（不能同步访问）。

由于 StringBuilder 相较于 StringBuffer 有速度优势，所以多数情况下建议使用 StringBuilder 类。然而在应用程序要求线程安全的情况下，则必须使用 StringBuffer 类。

```java
StringBuffer sBuffer = new StringBuffer("hello：");
sBuffer.append("111");
sBuffer.append(".222");
sBuffer.append(".333");
System.out.println(sBuffer);  
```

### StringBuffer 方法

``` 
1 	public StringBuffer append(String s)
将指定的字符串追加到此字符序列。
2 	public StringBuffer reverse()
 将此字符序列用其反转形式取代。
3 	public delete(int start, int end)
移除此序列的子字符串中的字符。
4 	public insert(int offset, int i)
将 int 参数的字符串表示形式插入此序列中。
5 	replace(int start, int end, String str)
使用给定 String 中的字符替换此序列的子字符串中的字符。
```

### StringBuilder 方法

```
1 	int capacity()
返回当前容量。
2 	char charAt(int index)
返回此序列中指定索引处的 char 值。
3 	void ensureCapacity(int minimumCapacity)
确保容量至少等于指定的最小值。
4 	void getChars(int srcBegin, int srcEnd, char[] dst, int dstBegin)
将字符从此序列复制到目标字符数组 dst。
5 	int indexOf(String str)
返回第一次出现的指定子字符串在该字符串中的索引。
6 	int indexOf(String str, int fromIndex)
从指定的索引处开始，返回第一次出现的指定子字符串在该字符串中的索引。
7 	int lastIndexOf(String str)
返回最右边出现的指定子字符串在此字符串中的索引。
8 	int lastIndexOf(String str, int fromIndex)
返回 String 对象中子字符串最后出现的位置。
9 	int length()
 返回长度（字符数）。
10 	void setCharAt(int index, char ch)
将给定索引处的字符设置为 ch。
11 	void setLength(int newLength)
设置字符序列的长度。
12 	CharSequence subSequence(int start, int end)
返回一个新的字符序列，该字符序列是此序列的子序列。
13 	String substring(int start)
返回一个新的 String，它包含此字符序列当前所包含的字符子序列。
14 	String substring(int start, int end)
返回一个新的 String，它包含此序列当前所包含的字符子序列。
15 	String toString()
返回此序列中数据的字符串表示形式。
```



---



# Java 数组

### 声明

```java
dataType[] arrayRefVar;   // 首选的方法
或
dataType arrayRefVar[];  // 效果相同，但不是首选方法
```

### 创建数组

```
arrayRefVar = new dataType[arraySize];
dataType[] arrayRefVar = {value0, value1, ..., valuek};
```

### 增强循环

```java
for (double element: myList) {
	System.out.println(element);
}
```

### 多维数组

```
type arrayName = new type[arraylenght1][arraylenght2];
int a[][] = new int[2][3];
```

### Arrays 类

java.util.Arrays 类能方便地操作数组，它提供的所有方法都是静态的。

| 序号 | 方法和说明                                                   |
| ---- | ------------------------------------------------------------ |
| 1    | **public static int binarySearch(Object[] a, Object key)**  				<br>用二分查找算法在给定数组中搜索给定值的对象(Byte,Int,double等)。数组在调用前必须排序好的。如果查找值包含在数组中，则返回搜索键的索引；否则返回 (-(*插入点*) - 1)。 |
| 2    | **public static boolean equals(long[] a, long[] a2)**  				<br>如果两个指定的 long 型数组彼此*相等*，则返回  true。如果两个数组包含相同数量的元素，并且两个数组中的所有相应元素对都是相等的，则认为这两个数组是相等的。换句话说，如果两个数组以相同顺序包含相同的元素，则两个数组是相等的。同样的方法适用于所有的其他基本数据类型（Byte，short，Int等）。 |
| 3    | **public static void fill(int[] a, int val)**  				<br>将指定的 int 值分配给指定 int 型数组指定范围中的每个元素。同样的方法适用于所有的其他基本数据类型（Byte，short，Int等）。 |
| 4    | **public static void sort(Object[] a)**  				<br>对指定对象数组根据其元素的自然顺序进行升序排列。同样的方法适用于所有的其他基本数据类型（Byte，short，Int等）。 |



---



# Java 日期时间

略

### Java 休眠（sleep）

sleep()使当前线程进入停滞状态（阻塞当前线程），让出CPU的使用、目的是不让当前线程独自霸占该进程所获的CPU资源，以留一定时间给其他线程执行的机会。

```java
import java.util.*;
blablabla
Thread.sleep(1000 * 3); // 3 secs
```



### 测量时间

```java
long end = System.currentTimeMillis( );
long diff = end - start;
```



---



# Java 正则表达式

java.util.regex 包主要包括以下三个类：

- Pattern 类：

  pattern 对象是一个正则表达式的编译表示。Pattern 类没有公共构造方法。要创建一个 Pattern 对象，你必须首先调用其公共静态编译方法，它返回一个 Pattern 对象。该方法接受一个正则表达式作为它的第一个参数。

- Matcher 类：

  Matcher 对象是对输入字符串进行解释和匹配操作的引擎。与Pattern 类一样，Matcher 也没有公共构造方法。你需要调用 Pattern 对象的 matcher 方法来获得一个 Matcher 对象。

- PatternSyntaxException：

  PatternSyntaxException 是一个非强制异常类，它表示一个正则表达式模式中的语法错误。

### 捕获组

捕获组是把多个字符当一个单独单元进行处理的方法，它通过对括号内的字符分组来创建。 

例如，正则表达式 (dog) 创建了单一分组，组里包含"d"，"o"，和"g"。 

捕获组是通过从左至右计算其开括号来编号。例如，在表达式（（A）（B（C））），有四个这样的组： 

- ((A)(B(C)))
- (A)
- (B(C))
- (C)

可以通过调用 matcher 对象的 groupCount 方法来查看表达式有多少个分组。groupCount 方法返回一个 int 值，表示matcher对象当前有多个捕获组。 

还有一个特殊的组（group(0)），它总是代表整个表达式。该组不包括在 groupCount 的返回值中。 

``` java
import java.util.regex.Matcher;
import java.util.regex.Pattern;
 
public class RegexMatches
{
    public static void main( String args[] ){
 
      // 按指定模式在字符串查找
      String line = "This order was placed for QT3000! OK?";
      String pattern = "(\\D*)(\\d+)(.*)";
 
      // 创建 Pattern 对象
      Pattern r = Pattern.compile(pattern);
 
      // 现在创建 matcher 对象
      Matcher m = r.matcher(line);
      if (m.find( )) {
         System.out.println("Found value: " + m.group(0) );
         System.out.println("Found value: " + m.group(1) );
         System.out.println("Found value: " + m.group(2) );
         System.out.println("Found value: " + m.group(3) ); 
      } else {
         System.out.println("NO MATCH");
      }
   }
}
```

------

### 正则表达式语法

在其他语言中，`\\` 表示：**我想要在正则表达式中插入一个普通的（字面上的）反斜杠，请不要给它任何特殊的意义。**

在 Java 中，`\\`  表示：**我要插入一个正则表达式的反斜线，所以其后的字符具有特殊的意义。**

所以，在其他的语言中（如Perl），一个反斜杠 `\` 就足以具有转义的作用，而在 Java 中正则表达式中则需要有两个反斜杠才能被解析为其他语言中的转义作用。也可以简单的理解在 Java 的正则表达式中，两个 `\\` 代表其他语言中的一个 `\`，这也就是为什么表示一位数字的正则表达式是 `\\d`，而表示一个普通的反斜杠是 `\\\\`。

| 字符          | 说明                                                         |
| ------------- | ------------------------------------------------------------ |
| \             | 将下一字符标记为特殊字符、文本、反向引用或八进制转义符。例如，"n"匹配字符"n"。"\n"匹配换行符。序列"\\\\"匹配"\\"，"\\("匹配"("。 |
| ^             | 匹配输入字符串开始的位置。如果设置了 **RegExp** 对象的 **Multiline** 属性，^ 还会与"\n"或"\r"之后的位置匹配。 |
| $             | 匹配输入字符串结尾的位置。如果设置了 **RegExp** 对象的 **Multiline** 属性，$ 还会与"\n"或"\r"之前的位置匹配。 |
| *             | 零次或多次匹配前面的字符或子表达式。例如，zo* 匹配"z"和"zoo"。* 等效于 {0,}。 |
| +             | 一次或多次匹配前面的字符或子表达式。例如，"zo+"与"zo"和"zoo"匹配，但与"z"不匹配。+ 等效于 {1,}。 |
| ?             | 零次或一次匹配前面的字符或子表达式。例如，"do(es)?"匹配"do"或"does"中的"do"。? 等效于 {0,1}。 |
| {*n*}         | *n* 是非负整数。正好匹配 *n* 次。例如，"o{2}"与"Bob"中的"o"不匹配，但与"food"中的两个"o"匹配。 |
| {*n*,}        | *n* 是非负整数。至少匹配 *n* 次。例如，"o{2,}"不匹配"Bob"中的"o"，而匹配"foooood"中的所有 o。"o{1,}"等效于"o+"。"o{0,}"等效于"o*"。 |
| {*n*,*m*}     | *m* 和 *n* 是非负整数，其中 *n* <= *m*。匹配至少 *n* 次，至多 *m* 次。例如，"o{1,3}"匹配"fooooood"中的头三个 o。'o{0,1}' 等效于 'o?'。注意：您不能将空格插入逗号和数字之间。 |
| ?             | 当此字符紧随任何其他限定符（*、+、?、{*n*}、{*n*,}、{*n*,*m*}）之后时，匹配模式是"非贪心的"。"非贪心的"模式匹配搜索到的、尽可能短的字符串，而默认的"贪心的"模式匹配搜索到的、尽可能长的字符串。例如，在字符串"oooo"中，"o+?"只匹配单个"o"，而"o+"匹配所有"o"。 |
| .             | 匹配除"\r\n"之外的任何单个字符。若要匹配包括"\r\n"在内的任意字符，请使用诸如"[\s\S]"之类的模式。 |
| (*pattern*)   | 匹配 *pattern* 并捕获该匹配的子表达式。可以使用 **$0…$9** 属性从结果"匹配"集合中检索捕获的匹配。若要匹配括号字符 ( )，请使用"\("或者"\)"。 |
| (?:*pattern*) | 匹配 *pattern* 但不捕获该匹配的子表达式，即它是一个非捕获匹配，不存储供以后使用的匹配。这对于用"or"字符 (\|) 组合模式部件的情况很有用。例如，'industr(?:y\|ies) 是比 'industry\|industries' 更经济的表达式。 |
| (?=*pattern*) | 执行正向预测先行搜索的子表达式，该表达式匹配处于匹配 *pattern*  的字符串的起始点的字符串。它是一个非捕获匹配，即不能捕获供以后使用的匹配。例如，'Windows (?=95\|98\|NT\|2000)'  匹配"Windows 2000"中的"Windows"，但不匹配"Windows  3.1"中的"Windows"。预测先行不占用字符，即发生匹配后，下一匹配的搜索紧随上一匹配之后，而不是在组成预测先行的字符后。 |
| (?!*pattern*) | 执行反向预测先行搜索的子表达式，该表达式匹配不处于匹配 *pattern*  的字符串的起始点的搜索字符串。它是一个非捕获匹配，即不能捕获供以后使用的匹配。例如，'Windows (?!95\|98\|NT\|2000)'  匹配"Windows 3.1"中的 "Windows"，但不匹配"Windows  2000"中的"Windows"。预测先行不占用字符，即发生匹配后，下一匹配的搜索紧随上一匹配之后，而不是在组成预测先行的字符后。 |
| *x*\|*y*      | 匹配 *x* 或 *y*。例如，'z\|food' 匹配"z"或"food"。'(z\|f)ood' 匹配"zood"或"food"。 |
| [*xyz*]       | 字符集。匹配包含的任一字符。例如，"[abc]"匹配"plain"中的"a"。 |
| [^*xyz*]      | 反向字符集。匹配未包含的任何字符。例如，"[^abc]"匹配"plain"中"p"，"l"，"i"，"n"。 |
| [*a-z*]       | 字符范围。匹配指定范围内的任何字符。例如，"[a-z]"匹配"a"到"z"范围内的任何小写字母。 |
| [^*a-z*]      | 反向范围字符。匹配不在指定的范围内的任何字符。例如，"[^a-z]"匹配任何不在"a"到"z"范围内的任何字符。 |
| \b            | 匹配一个字边界，即字与空格间的位置。例如，"er\b"匹配"never"中的"er"，但不匹配"verb"中的"er"。 |
| \B            | 非字边界匹配。"er\B"匹配"verb"中的"er"，但不匹配"never"中的"er"。 |
| \c*x*         | 匹配 *x* 指示的控制字符。例如，\cM 匹配 Control-M 或回车符。*x* 的值必须在 A-Z 或 a-z 之间。如果不是这样，则假定 c 就是"c"字符本身。 |
| \d            | 数字字符匹配。等效于 [0-9]。                                 |
| \D            | 非数字字符匹配。等效于 [^0-9]。                              |
| \f            | 换页符匹配。等效于 \x0c 和 \cL。                             |
| \n            | 换行符匹配。等效于 \x0a 和 \cJ。                             |
| \r            | 匹配一个回车符。等效于 \x0d 和 \cM。                         |
| \s            | 匹配任何空白字符，包括空格、制表符、换页符等。与 [ \f\n\r\t\v] 等效。 |
| \S            | 匹配任何非空白字符。与 [^ \f\n\r\t\v] 等效。                 |
| \t            | 制表符匹配。与 \x09 和 \cI 等效。                            |
| \v            | 垂直制表符匹配。与 \x0b 和 \cK 等效。                        |
| \w            | 匹配任何字类字符，包括下划线。与"[A-Za-z0-9_]"等效。         |
| \W            | 与任何非单词字符匹配。与"[^A-Za-z0-9_]"等效。                |
| \x*n*         | 匹配 *n*，此处的 *n* 是一个十六进制转义码。十六进制转义码必须正好是两位数长。例如，"\x41"匹配"A"。"\x041"与"\x04"&"1"等效。允许在正则表达式中使用 ASCII 代码。 |
| \*num*        | 匹配 *num*，此处的 *num* 是一个正整数。到捕获匹配的反向引用。例如，"(.)\1"匹配两个连续的相同字符。 |
| \*n*          | 标识一个八进制转义码或反向引用。如果 \*n* 前面至少有 *n* 个捕获子表达式，那么 *n* 是反向引用。否则，如果 *n* 是八进制数 (0-7)，那么 *n* 是八进制转义码。 |
| \*nm*         | 标识一个八进制转义码或反向引用。如果 \*nm* 前面至少有 *nm* 个捕获子表达式，那么 *nm* 是反向引用。如果 \*nm* 前面至少有 *n* 个捕获，则 *n* 是反向引用，后面跟有字符 *m*。如果两种前面的情况都不存在，则 \*nm* 匹配八进制值 *nm*，其中 *n* 和 *m* 是八进制数字 (0-7)。 |
| \nml          | 当 *n* 是八进制数 (0-3)，*m* 和 *l* 是八进制数 (0-7) 时，匹配八进制转义码 *nml*。 |
| \u*n*         | 匹配 *n*，其中 *n* 是以四位十六进制数表示的 Unicode 字符。例如，\u00A9 匹配版权符号 (©)。 |

### Matcher 类的方法

##### 索引方法

索引方法提供了有用的索引值，精确表明输入字符串中在哪能找到匹配：

| **序号** | **方法及说明**                                               |
| -------- | ------------------------------------------------------------ |
| 1        | **public int start()**   				<br>返回以前匹配的初始索引。 |
| 2        | **public int start(int group)**  				 <br/>返回在以前的匹配操作期间，由给定组所捕获的子序列的初始索引 |
| 3        | **public int end()**  				<br/>返回最后匹配字符之后的偏移量。 |
| 4        | **public int end(int group)**  				<br/>返回在以前的匹配操作期间，由给定组所捕获子序列的最后字符之后的偏移量。 |

##### 研究方法

 研究方法用来检查输入字符串并返回一个布尔值，表示是否找到该模式： 

| **序号** | **方法及说明**                                               |
| -------- | ------------------------------------------------------------ |
| 1        | **public boolean lookingAt()**   				 <br>尝试将从区域开头开始的输入序列与该模式匹配。 |
| 2        | **public boolean find()**   				<br/>尝试查找与该模式匹配的输入序列的下一个子序列。 |
| 3        | **public boolean find(int start****）**  				<br/>重置此匹配器，然后尝试查找匹配该模式、从指定索引开始的输入序列的下一个子序列。 |
| 4        | **public boolean matches()**   				<br/>尝试将整个区域与模式匹配。 |

##### 替换方法

 替换方法是替换输入字符串里文本的方法： 

| **序号** | **方法及说明**                                               |
| -------- | ------------------------------------------------------------ |
| 1        | **public Matcher appendReplacement(StringBuffer sb, String replacement)**  				<br>实现非终端添加和替换步骤。 |
| 2        | **public StringBuffer appendTail(StringBuffer sb)**  				<br/>实现终端添加和替换步骤。 |
| 3        | **public String replaceAll(String replacement)**   				 <br/>替换模式与给定替换字符串相匹配的输入序列的每个子序列。 |
| 4        | **public String replaceFirst(String replacement)**  				 <br/>替换模式与给定替换字符串匹配的输入序列的第一个子序列。 |
| 5        | **public static String quoteReplacement(String s)**  				<br/>返回指定字符串的字面替换字符串。这个方法返回一个字符串，就像传递给Matcher类的appendReplacement 方法一个字面字符串一样工作。 |

##### start 和 end 方法

 下面是一个对单词 "cat" 出现在输入字符串中出现次数进行计数的例子： 

```java
Matcher m = p.matcher(INPUT); // 获取 matcher 对象
int count = 0;

while(m.find()) {
    count++;
    System.out.println("Match number "+count);
    System.out.println("start(): "+m.start());
    System.out.println("end(): "+m.end());
}
```

##### matches 和 lookingAt 方法

matches 和 lookingAt 方法都用来尝试匹配一个输入序列模式。它们的不同是 matches 要求整个序列都匹配，而lookingAt 不要求。 

lookingAt 方法虽然不需要整句都匹配，但是需要从第一个字符开始匹配。

```java
pattern = Pattern.compile(REGEX);
matcher = pattern.matcher(INPUT);
matcher2 = pattern.matcher(INPUT2);

System.out.println("Current REGEX is: "+REGEX); // foo
System.out.println("Current INPUT is: "+INPUT); // fooooooooooooooooo
System.out.println("Current INPUT2 is: "+INPUT2); // ooooofoooooooooooo


System.out.println("lookingAt(): "+matcher.lookingAt()); // true
System.out.println("matches(): "+matcher.matches()); // false
System.out.println("lookingAt(): "+matcher2.lookingAt()); // false
```

##### replaceFirst 和 replaceAll 方法

replaceFirst 和 replaceAll 方法用来替换匹配正则表达式的文本。不同的是，replaceFirst 替换首次匹配，replaceAll 替换所有匹配。 

##### appendReplacement 和 appendTail 方法

Matcher 类也提供了appendReplacement 和 appendTail 方法用于文本替换： 

``` java
import java.util.regex.Matcher;
import java.util.regex.Pattern;
 
public class RegexMatches
{
   private static String REGEX = "a*b";
   private static String INPUT = "aabfooaabfooabfoobkkk";
   private static String REPLACE = "-";
   public static void main(String[] args) {
      Pattern p = Pattern.compile(REGEX);
      // 获取 matcher 对象
      Matcher m = p.matcher(INPUT);
      StringBuffer sb = new StringBuffer();
      while(m.find()){
         m.appendReplacement(sb,REPLACE);
      }
      m.appendTail(sb);
      System.out.println(sb.toString()); // -foo-foo-foo-kkk
   }
}
```

.appendReplacement(): 将当前匹配子串替换为指定字符串，并且将替换后的子串以及其之前到上次匹配子串之后的字符串段添加到一个 StringBuffer对象里。 

.appendTail(): 将最后一次匹配工作后剩余的字符串添加到一个 StringBuffer 对象里。 

### PatternSyntaxException 类的方法

PatternSyntaxException 是一个非强制异常类，它指示一个正则表达式模式中的语法错误。 

PatternSyntaxException 类提供了下面的方法来帮助我们查看发生了什么错误。 

| **序号** | **方法及说明**                                               |
| -------- | ------------------------------------------------------------ |
| 1        | **public String getDescription()**  				<br>获取错误的描述。 |
| 2        | **public int getIndex()**   				 <br/>获取错误的索引。 |
| 3        | **public String getPattern()**   				<br/>获取错误的正则表达式模式。 |
| 4        | **public String getMessage()**   				<br/>返回多行字符串，包含语法错误及其索引的描述、错误的正则表达式模式和模式中错误索引的可视化指示。 |

