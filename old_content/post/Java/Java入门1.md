---
title: Java入门（1）
date: 2018-11-10 05:15:40.923 -0700
categories: [Java]
tags: [java]

---



Java 入门，part 1

<!--more-->

# Hello World

第一个HelloWorld程序：

```Java

public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello World");
    }
}
```

运行的时候先把Java源文件编译成class字节码文件：

```sh
javac HelloWorld.java
```

运行HelloWorld.class，java后直接跟类名就可以了，不需要.class：

```sh
java HelloWorld
```



# Java 简介

Java有三个体系：

- JavaSE（J2SE）（Java2 Platform Standard Edition，java平台标准版）
-  JavaEE(J2EE)(Java 2 Platform,Enterprise Edition，java平台企业版)
- JavaME(J2ME)(Java 2 Platform Micro Edition，java平台微型版)



# Java 配置

1. 安装JDK（Java SE Development Kit)，JDK里面已经包含了Java的运行环境JRE

2. 在Windows系统下要配置的环境变量：

   - **JAVA_HOME**："C:\Program Files (x86)\Java\jdk1.8.0_91"
   - **CLASSPATH**：".;%JAVA_HOME%\lib\dt.jar;%JAVA_HOME%\lib\tools.jar;"    //记得前面有个"."; 1.5版本以上的JDK不用配置CLASSPATH

   - **PATH**："%JAVA_HOME%\bin;%JAVA_HOME%\jre\bin;"



# Java 基础语法

- **对象**：对象是类的一个实例

- **类**：类是一个模板
- **方法**：一个类可以有很多方法
- **实例变量**：每个对象都有独特的实例变量，对象的状态由这些实例变量的值决定

#### 基本语法

- **Case Sensitive**
- **类名**：首字母大写，驼峰，例如MyFirstJavaClass
- **方法名**：首字母小写，驼峰
- **源文件名**：源文件名和类名相同
- **主方法入口**：从public static void main(String[] args)开始执行

#### Java 变量

- 局部变量：在方法、构造方法或者语句块中定义的变量被称为局部变量。变量声明和初始化都是在方法中，方法结束后，变量就会自动销毁。
- 类变量（静态变量）：成员变量是定义在类中，方法体之外的变量。这种变量在创建对象的时候实例化。成员变量可以被类中方法、构造方法和特定类的语句块访问。
- 成员变量（非静态变量）：类变量也声明在类中，方法体之外，但必须声明为static类型。

非静态变量创建在堆上

#### Java 枚举

```java
class FreshJuice {
   enum FreshJuiceSize{ SMALL, MEDIUM , LARGE }
   FreshJuiceSize size;
}
 
public class FreshJuiceTest {
   public static void main(String []args){
      FreshJuice juice = new FreshJuice();
      juice.size = FreshJuice.FreshJuiceSize.MEDIUM  ;
   }
}
```

#### Java 注释风格

```java
public class HelloWorld {
   /* 这是第一个Java程序
    * 它将打印Hello World
    * 这是一个多行注释的示例
    */
    public static void main(String []args){
       // 这是单行注释的示例
       /* 这个也是单行注释的示例 */
       System.out.println("Hello World"); 
    }
}
```

#### Java源程序与编译型运行的区别

<div style="text-align: left"><img src="/image/JavaYCXYBYXYXQB.png" style="zoom:100%"></div>



# java 对象和类

#### 变量

#### 方法

#### 构造方法

如果没有显式地定义构造方法，编译器会为该类提供默认构造方法。

一个类可以有多个构造方法，名称必须与类同名，但是参数要不一样。

```java
public class Puppy{
    public Puppy(){
    }
 
    public Puppy(String name){
        // 这个构造器仅有一个参数：name
    }
}
```

#### 创建对象

通过使用关键字*new*来创建一个新对象，需要三步

1. **声明**：声明对象，包括对象名称和类型；
2. **实例化**：使用关键字*new*来创建对象；
3. **初始化**：用*new*创建对象时，会自动调用构造方法对对象做初始化。

```
Puppy myPuppy = new Puppy( "tommy" );
```

####访问实例的变量和方法

这里访问的是属于这个实例的成员方法

```java
/* 实例化对象 */
ObjectReference = new Constructor();
/* 访问类中的变量 */
ObjectReference.variableName;
/* 访问类中的方法 */
ObjectReference.methodName();
```

#### 源文件声明规则

- 一个源文件中只能有一个public类，可有多个非public类（要能找到入口main函数）
- 若一个类定义在某个包中，那么package语句要在源文件的首行
- import语句在package语句与类定义之间

#### import语句

例如，下面的命令行将会命令编译器载入java_installation/java/io路径下的所有类：

```java
import java.io.*;
```



# Java 基本数据类型

Java两大数据类型：

- 内置数据类型
- 引用数据类型

#### 内置数据类型

- **byte**：
  - 8位、有符号、以二进制补码表示的整数
  - -128~127，默认 0
  - e.g.: byte a = 100;
- **short**：
  - 16位、有符号、以二进制补码表示的整数
  - -32768~32767，默认 0
- **int**：
  - 32位、有符号、以二进制补码表示的整数
- **long**：
  - 默认 0L
  - e.g.: long a = 100000L; (L不分大小写，但大写不容易混淆)
- **float**：
  - 单精度、32位、符合IEEE 754标准的浮点数
  - 默认值 0.0f
  - e.g.: float f1 = 234.5f
- **double**：
  - 默认 0.0d
- **boolean**：
  - 1bit
  - 取值 true / false
  - 默认值 false
- **char**：
  - 16位Unicode字符
  - 最小值\u0000，最大值\uffff
  - 可以存储任何字符
  - e.g.: char letter = 'A';

```
基本类型：byte 二进制位数：8
包装类：java.lang.Byte
最小值：Byte.MIN_VALUE=-128
最大值：Byte.MAX_VALUE=127

基本类型：short 二进制位数：16
包装类：java.lang.Short
最小值：Short.MIN_VALUE=-32768
最大值：Short.MAX_VALUE=32767

基本类型：int 二进制位数：32
包装类：java.lang.Integer
最小值：Integer.MIN_VALUE=-2147483648
最大值：Integer.MAX_VALUE=2147483647

基本类型：long 二进制位数：64
包装类：java.lang.Long
最小值：Long.MIN_VALUE=-9223372036854775808
最大值：Long.MAX_VALUE=9223372036854775807

基本类型：float 二进制位数：32
包装类：java.lang.Float
最小值：Float.MIN_VALUE=1.4E-45
最大值：Float.MAX_VALUE=3.4028235E38

基本类型：double 二进制位数：64
包装类：java.lang.Double
最小值：Double.MIN_VALUE=4.9E-324
最大值：Double.MAX_VALUE=1.7976931348623157E308

基本类型：char 二进制位数：16
包装类：java.lang.Character
最小值：Character.MIN_VALUE=0
最大值：Character.MAX_VALUE=65535
```

实际上，JAVA中还存在另外一种基本类型void，它也有对应的包装类 java.lang.Void，不过我们无法直接对它们进行操作。

#### 引用数据类型

- 对象、数组都是引用数据类型。

- 所有引用类型的默认值都是null。

- **一个引用变量可以用来引用任何与之兼容的类型**。

- 例子：Site site = new Site("Runoob");

#### java 常量

常量在程序运行时不能被修改。

在 Java 中使用 *final* 来修饰常量，一般常量名全大写：

```java
final double PI = 3.1415927;
```

字面量：

``` java
68
'A' // 字符
'\u0001' // Unicode字符
"Hello World" // 字符串
0144 // 前缀0表示8进制
0x64 // 前缀0x表示16进制
```

转义字符：略

#### 自动类型转换

整型、实型（常量）、字符型数据可以混合运算。运算中，不同类型的数据先转化为同一类型，然后进行运算。

转换从低级到高级：

```
低  ------------------------------------>  高
byte,short,char—> int —> long—> float —> double 
```

转换规则：

1. 不能对boolean类型进行类型转换。

2. 不能把对象类型转换成不相关类的对象。

3. 在把容量大的类型转换为容量小的类型时必须使用强制类型转换。

4. 转换过程中可能导致溢出或损失精度。
5. 浮点数到整数的转换是通过舍弃小数得到，而不是四舍五入。

#### 强制类型转换

1. 条件是转换的数据类型必须是兼容的。

2. 格式：(type)value

#### 隐含强制类型转换

1. 整数的默认类型是 int。

2. 浮点型不存在这种情况，因为在定义 float 类型时必须在数字后面跟上 F 或者 f。 

