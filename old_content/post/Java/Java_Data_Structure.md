---
title: Java Data Structure
date: 2019-02-06 21:03:40.923 -0700
categories: [Java]
tags: [java, data structure]
---

<span></span>

<!--more-->

# 数组（Array）

Declare:

```java
dataType[] arrayRefVar;
```

Create:

```java
arrayRefVar = new dataType[arraySize];
```

Declare and create:

```java
dataType[] arrayRefVar = new dataType[arraySize];
// or
dataType[] arrayRefVar = {value0, value1, ..., valuek};
```

The foreach loops (traverse the complete array sequentially without using an index variable):

```java
for (double element: myList) {
    System.out.println(element);
}
```

Length of the array:

```java
System.out.println(myList.length);
```

The arrays class (*java.util.Arrays*, static methods):

```java
// The array must be sorted prior to making this call.
// This returns index of the search key, if it is contained in the list;
// otherwise, it returns ( – (insertion point + 1)).
public static int binarySearch(Object[] a, Object key)

// Two arrays are considered equal if both arrays contain the same number of elements,
// and all corresponding pairs of elements in the two arrays are equal
public static boolean equals(long[] a, long[] a2)

public static void fill(int[] a, int val)

// Sorts the specified array of objects into an ascending order
public static void sort(Object[] a)
```



---



# String

```java
// string length
String str = "asdf";
System.out.println(str.length());

// string concat
str1.concat(str2);
"My name is ".concat("Zara");
"Hello," + " world" + "!";
    
// string formatting    
System.out.printf("The value of the float variable is " +
                  "%f, while the value of the integer " +
                  "variable is %d, and the string " +
                  "is %s", floatVar, intVar, stringVar);
```

```java
int length()

char charAt(int index)

int indexOf([int ch | String str]{, int from Index})
int lastIndexOf([int ch | String str]{, int from Index})

String substring(int beginIndex{, int endIndex})

String[] split(String regex)

int compareTo(String anotherString)
boolean equals(Object anObject)

String concat(String str)

boolean startsWith(String prefix)
boolean endsWith(String suffix)
 	
char[] toCharArray()
 	 	
String toLowerCase()
String toUpperCase()
 	 	 	
String trim()
 	
byte getBytes()
byte[] getBytes(String charsetName)
 	 	
int hashCode()
 	 	
boolean matches(String regex)
String replace(char oldChar, char newChar)
String replaceAll(String regex, String replacement)
```





# Map

Several methods throw a *NoSuchElementException* when no items exist in the invoking map.

A *ClassCastException* is thrown when an object is incompatible with the elements in a map.

A *NullPointerException* is thrown if an attempt is made to use a null object and null is not allowed in the map.

An *UnsupportedOperationException* is thrown when an attempt is made to change an unmodifiable map.

Methods (not static):

``` java
void clear( )

boolean containsKey(Object k)

boolean containsValue(Object v)

// Returns a Set that contains the entries in the map.
// The set contains objects of type Map.Entry.
// This method provides a set-view of the invoking map.
Set entrySet( )

// Returns true **if obj is a Map** and contains the same entries.
boolean equals(Object obj)

Object get(Object k)

int hashCode( )

boolean isEmpty( )

Set keySet( )

// Puts an entry in the invoking map, overwriting any previous value associated with the key. 
// Returns null if the key did not already exist.
// Otherwise, the previous value linked to the key is returned.
Object put(Object k, Object v)

void putAll(Map m)

Object remove(Object k)

int size( ) 	

Collection values( )
```

**Map** has its implementation in various classes like **HashMap** (*java.util.HashMap):

```java
Map m1 = new HashMap(); 
// Map<String, String> m1 = new HashMap<>();
m1.put("Zara", "8");
```

Get elem from a map:

```java
if (map.containsKey(elem)) {
    System.out.println(map.get())
}
```



---



# Set

Methods (not static):

```
add( )
clear( )
contains( )
isEmpty( )
iterator( )
remove( )
size( )
```

Set has its implementation in various classes like HashSet, TreeSet, LinkedHashSet:

```java
int count[] = {34, 22, 10, 60, 30, 22};
Set<Integer> set = new HashSet<Integer>();
for(int i = 0; i < 5; i++) {
    set.add(count[i]);
}
```



---



# SortedSet

The SortedSet interface extends Set and declares the behavior of a set sorted in an ascending order.

Several methods throw a *NoSuchElementException* when no items are  contained in the invoking set. 

A *ClassCastException* is thrown when an  object is incompatible with the elements in a set.

A *NullPointerException* is thrown if an attempt is made to use a null object and null is not allowed in the set.

```java
// Returns the invoking sorted set's comparator.
// If the natural ordering is used for this set, null is returned.
Comparator comparator( )
Object first( )
Object last( )
// Returns a SortedSet containing those elements **less than** end that are contained in the invoking sorted set.
// Just a reference
SortedSet headSet(Object end)
// Returns a SortedSet that contains those elements **greater than or equal to** start that are contained in the sorted set.
SortedSet tailSet(Object start)
SortedSet subSet(Object start, Object end)
```

SortedSet have its implementation in various classes like TreeSet.

```java
SortedSet set = new TreeSet(); 
set.add("b");
set.add("c");
set.add("a");

Iterator it = set.iterator();
while (it.hasNext()) {
    Object element = it.next();
    System.out.println(element.toString());
}
```

