---
title: Java入门（3）
date: 2018-11-17 21:49:40.923 -0700
categories: [Java]
tags: [java]
---



Java 入门，part 3

<!--more-->



# Java 方法

### 命名规则

-  方法的名字的第一个单词应以小写字母作为开头，后面的单词则用大写字母开头写，不使用连接符。例如：addPerson。
-  下划线可能出现在 JUnit 测试方法名称中用以分隔名称的逻辑组件。一个典型的模式是：test\<MethodUnderTest\>_\<state\>，例如 testPop_emptyStack。 

### 方法的重载

创建有相同名字但参数不同的方法。

Java编译器根据方法签名判断哪个方法应该被调用。

重载的方法必须拥有不同的参数列表。你不能仅仅依据修饰符或者返回类型的不同来重载方法。

### 构造方法

构造方法没有返回值。

通常会使用构造方法给一个类的实例变量赋初值，或者执行其它必要的步骤来创建一个完整的对象。

不管你是否自定义构造方法，所有的类都有构造方法，因为Java自动提供了一个默认构造方法，它把所有成员初始化为0。

一旦你定义了自己的构造方法，默认构造方法就会失效。

### 可变参数

方法的可变参数的声明如下所示：

``` java
typeName... parameterName
```

e.g.:

``` java
public static void printMax( double... numbers) {
	System.out.println(numbers.length)
}
```

## finalize() 方法

Java 允许定义这样的方法，它在对象被垃圾收集器析构(回收)之前调用，这个方法叫做 finalize( )，它用来清除回收对象。 

例如，你可以使用 finalize() 来确保一个对象打开的文件被关闭了。 

在 finalize() 方法里，你必须指定在对象销毁时候要执行的操作。 

finalize() 一般格式是：

```java
protected void finalize() {
    // 在这里终结代码
}
```

关键字 protected 是一个限定符，它确保 finalize() 方法不会被该类以外的代码调用。 

当然，Java 的内存回收可以由 JVM 来自动完成。如果你手动使用，则可以使用上面的方法。  



# Java 流(Stream)、文件(File)和IO

### 读取控制台输入

 Java 的控制台输入由 `System.in` 完成。 

为了获得一个绑定到控制台的字符流，你可以把 `System.in` 包装在一个 BufferedReader 对象中来创建一个字符流。 

下面是创建 BufferedReader 的基本语法：

``` java
BufferedReader br = new BufferedReader(new 
                      InputStreamReader(System.in));
```

BufferedReader 对象创建后，我们便可以使用 read() 方法从控制台读取一个字符，或者用 readLine() 方法读取一个字符串。 

### 从控制台读取多字符输入

从 BufferedReader 对象读取一个字符要使用 read() 方法，它的语法如下： 

``` java
int read( ) throws IOException
// c = (char) br.read();
```

每次调用 read() 方法，它从输入流读取一个字符并把该字符作为整数值返回。 当流结束的时候返回 -1。该方法抛出 IOException。 

### 从控制台读取字符串

 从标准输入读取一个字符串需要使用 BufferedReader 的 readLine() 方法。 

``` java
String readLine( ) throws IOException
// str = br.readLine();
```

### 控制台输出

 在此前已经介绍过，控制台的输出由 print( ) 和 println() 完成。这些方法都由类 PrintStream 定义，System.out 是该类对象的一个引用。 

PrintStream 继承了 OutputStream类，并且实现了方法 write()。这样，write() 也可以用来往控制台写操作。 

PrintStream 定义 write() 的最简单格式如下所示：

```java
void write(int byteval)
```

该方法将 byteval 的**低八位字节**写到流中。

``` java
System.out.write('\n');
```

**注意：**write() 方法不经常使用，因为 print() 和 println() 方法用起来更为方便。

### 读写文件

一个流被定义为一个数据序列。输入流用于从源读取数据，输出流用于向目标写数据。 

下图是一个描述输入流和输出流的类层次图。

<div style="text-align: left"><img src="/image/JavaIOStream.png" style="zoom:100%"></div>

### FileInputStream

可以使用字符串类型的文件名来创建一个输入流对象来读取文件：

``` java
InputStream f = new FileInputStream("C:/java/hello");
```

也可以使用一个文件对象来创建一个输入流对象来读取文件。我们首先得使用 File() 方法来创建一个文件对象：

``` java
File f = new File("C:/java/hello");
InputStream out = new FileInputStream(f);
```

| **序号** | **方法及描述**                                               |
| -------- | ------------------------------------------------------------ |
| 1        | **public void close() throws IOException{}**  				<br>关闭此文件输入流并释放与此流有关的所有系统资源。抛出IOException异常。 |
| 2        | **protected void finalize()throws IOException {}**  				<br/>这个方法清除与该文件的连接。确保在不再引用文件输入流时调用其 close 方法。抛出IOException异常。 |
| 3        | **public int read(int r)throws IOException{}**  				<br/>这个方法从 InputStream 对象读取指定字节的数据。返回为整数值。返回下一字节数据，如果已经到结尾则返回-1。 |
| 4        | **public int read(byte[] r) throws IOException{}**  				<br/>这个方法从输入流读取r.length长度的字节。返回读取的字节数。如果是文件结尾则返回-1。 |
| 5        | **public int available() throws IOException{}**  				<br/>返回下一次对此输入流调用的方法可以不受阻塞地从此输入流读取的字节数。返回一个整数值。 |

除了 InputStream 外，还有一些其他的输入流，更多的细节参考下面链接：

-  [ByteArrayInputStream](http://www.runoob.com/java/java-bytearrayinputstream.html)

  - ```java
    ByteArrayInputStream bArray = new ByteArrayInputStream(byte [] a);
    ```

  - ``` java
    ByteArrayInputStream bArray = new ByteArrayInputStream(byte []a, 
                                                           int off, 
                                                           int len)
    ```

- [DataInputStream](http://www.runoob.com/java/java-datainputstream.html)

  - ``` java
    DataInputStream dis = new DataInputStream(InputStream in);
    ```

### FileOutputStream

该类用来创建一个文件并向文件中写数据。

如果该流在打开文件进行输出前，目标文件不存在，那么该流会创建该文件。

有两个构造方法可以用来创建 FileOutputStream 对象。

使用字符串类型的文件名来创建一个输出流对象：

``` java
OutputStream f = new FileOutputStream("C:/java/hello")
```

也可以使用一个文件对象来创建一个输出流来写文件。我们首先得使用File()方法来创建一个文件对象：

``` java
File f = new File("C:/java/hello");
OutputStream f = new FileOutputStream(f);
```

| **序号** | **方法及描述**                                               |
| -------- | ------------------------------------------------------------ |
| 1        | **public void close() throws IOException{}**  				<br>关闭此文件输入流并释放与此流有关的所有系统资源。抛出IOException异常。 |
| 2        | **protected void finalize()throws IOException {}**  				<br/>这个方法清除与该文件的连接。确保在不再引用文件输入流时调用其 close 方法。抛出IOException异常。 |
| 3        | **public void write(int w)throws IOException{}**  				<br/>这个方法把指定的字节写到输出流中。 |
| 4        | **public void write(byte[] w)**  				<br/>把指定数组中w.length长度的字节写到OutputStream中。 |

除了OutputStream外，还有一些其他的输出流，更多的细节参考下面链接：

-  [ByteArrayOutputStream](http://www.runoob.com/java/java-bytearrayoutputstream.html)
-  [DataOutputStream](http://www.runoob.com/java/java-dataoutputstream.html)

### 例子

1. 二进制写入

   ```java
   import java.io.*;
    
   public class fileStreamTest {
       public static void main(String args[]) {
           try {
               byte bWrite[] = { 11, 21, 3, 40, 5 };
               OutputStream os = new FileOutputStream("test.txt");
               for (int x = 0; x < bWrite.length; x++) {
                   os.write(bWrite[x]); // writes the bytes
               }
               os.close();
    
               InputStream is = new FileInputStream("test.txt");
               int size = is.available();
    
               for (int i = 0; i < size; i++) {
                   System.out.print((char) is.read() + "  ");
               }
               is.close();
           } catch (IOException e) {
               System.out.print("Exception");
           }
       }
   }
   ```

2. UTF-8编码（**区别就是在stream外面再套了一层writer，可以转编码、缓冲**）

   ``` java
   
   //文件名 :fileStreamTest2.java
   import java.io.*;
    
   public class fileStreamTest2 {
       public static void main(String[] args) throws IOException {
    
           File f = new File("a.txt");
           FileOutputStream fop = new FileOutputStream(f);
           // 构建FileOutputStream对象,文件不存在会自动新建
    
           OutputStreamWriter writer = new OutputStreamWriter(fop, "UTF-8");
           // 构建OutputStreamWriter对象,参数可以指定编码,默认为操作系统默认编码,windows上是gbk
    
           writer.append("中文输入");
           // 写入到缓冲区
    
           writer.append("\r\n");
           // 换行
    
           writer.append("English");
           // 刷新缓存冲,写入到文件,如果下面已经没有写入的内容了,直接close也会写入
    
           writer.close();
           // 关闭写入流,同时会把缓冲区内容写入文件,所以上面的注释掉
    
           fop.close();
           // 关闭输出流,释放系统资源
    
           FileInputStream fip = new FileInputStream(f);
           // 构建FileInputStream对象
    
           InputStreamReader reader = new InputStreamReader(fip, "UTF-8");
           // 构建InputStreamReader对象,编码与写入相同
    
           StringBuffer sb = new StringBuffer();
           while (reader.ready()) {
               sb.append((char) reader.read());
               // 转成char加到StringBuffer对象中
           }
           System.out.println(sb.toString());
           reader.close();
           // 关闭读取流
    
           fip.close();
           // 关闭输入流,释放系统资源
    
       }
   }
   
   ```

------

### 文件和I/O

 还有一些关于文件和I/O的类，我们也需要知道：

-  [File Class(类)](http://www.runoob.com/java/java-file.html)
-  [FileReader Class(类)](http://www.runoob.com/java/java-filereader.html)
-  [FileWriter Class(类)](http://www.runoob.com/java/java-filewriter.html)

### Java中的目录

##### 创建目录

 File类中有两个方法可以用来创建文件夹：

- **mkdir( )**方法创建一个文件夹，成功则返回true，失败则返回false。失败表明File对象指定的路径已经存在，或者由于整个路径还不存在，该文件夹不能被创建。
- **mkdirs()**方法创建一个文件夹和它的所有父文件夹。

``` java
import java.io.File;
 
public class CreateDir {
    public static void main(String args[]) {
        String dirname = "/tmp/user/java/bin";
        File d = new File(dirname);
        // 现在创建目录
        d.mkdirs();
    }
}
```

**注意：** Java 在 UNIX 和 Windows 自动按约定分辨文件路径分隔符。如果你在 Windows 版本的 Java 中使用分隔符 (/) ，路径依然能够被正确解析。

##### 读取目录

 一个目录其实就是一个 File 对象，它包含其他文件和文件夹。 

如果创建一个 File 对象并且它是一个目录，那么调用 isDirectory() 方法会返回 true。 

可以通过调用该对象上的 list() 方法，来提取它包含的文件和文件夹的列表。 

下面展示的例子说明如何使用 list() 方法来检查一个文件夹中包含的内容：

```java
import java.io.File;
 
public class DirList {
    public static void main(String args[]) {
        String dirname = "/tmp";
        File f1 = new File(dirname);
        if (f1.isDirectory()) {
            System.out.println("目录 " + dirname);
            String s[] = f1.list();
            for (int i = 0; i < s.length; i++) {
                File f = new File(dirname + "/" + s[i]);
                if (f.isDirectory()) {
                    System.out.println(s[i] + " 是一个目录");
                } else {
                    System.out.println(s[i] + " 是一个文件");
                }
            }
        } else {
            System.out.println(dirname + " 不是一个目录");
        }
    }
}
```

##### 删除目录或文件

删除文件可以使用  **java.io.File.delete()** 方法。

以下代码会删除目录 **/tmp/java/**，**需要注意的是当删除某一目录时，必须保证该目录下没有其他文件才能正确删除，否则将删除失败**。所以下面的代码是递归的。

测试目录结构：

``` java
import java.io.File;
 
public class DeleteFileDemo {
    public static void main(String args[]) {
        // 这里修改为自己的测试目录
        File folder = new File("/tmp/java/");
        deleteFolder(folder);
    }
 
    // 删除文件及目录
    public static void deleteFolder(File folder) {
        File[] files = folder.listFiles();
        if (files != null) {
            for (File f : files) {
                if (f.isDirectory()) {
                    deleteFolder(f);
                } else {
                    f.delete();
                }
            }
        }
        folder.delete();
    }
}
```



---



# Java  Scanner 类

java.util.Scanner 是 Java5 的新特征，我们可以通过 Scanner 类来获取用户的输入。 

下面是创建 Scanner 对象的基本语法：

``` java
Scanner s = new Scanner(System.in);
```

通过 Scanner 类的 next() 与 nextLine() 方法获取输入的字符串，在读取前我们一般需要
使用 hasNext 与 hasNextLine 判断是否还有输入的数据：

``` java
import java.util.Scanner; 
 
public class ScannerDemo {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        // 从键盘接收数据
 
        // next方式接收字符串
        System.out.println("next方式接收：");
        // 判断是否还有输入
        if (scan.hasNext()) {
            String str1 = scan.next();
            System.out.println("输入的数据为：" + str1);
        }
        scan.close();
    }
}
```

```
import java.util.Scanner; 
 
public class ScannerDemo {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        // 从键盘接收数据
 
        // next方式接收字符串
        System.out.println("next方式接收：");
        // 判断是否还有输入
        if (scan.hasNext()) {
            String str1 = scan.next();
            System.out.println("输入的数据为：" + str1);
        }
        scan.close();
    }
}
```

### next() 与 nextLine() 区别

next():

- 一定要读取到有效字符后才可以结束输入。
- 对输入有效字符之前遇到的空白，next() 方法会自动将其去掉。 
- 只有输入有效字符后才将其后面输入的空白作为分隔符或者结束符。 
- next() 不能得到带有空格的字符串。

nextLine()： 

- 以Enter为结束符,也就是说 nextLine()方法返回的是输入回车之前的所有字符。 
- 可以获得空白。

### 读数

如果要输入 int 或 float 类型的数据，在 Scanner 类中也有支持，但是在输入之前最好先使用 hasNextXxx() 方法进行验证，再使用 nextXxx() 来读取：

```
scan.hasNextInt();
scan.nextInt();
```



---



# Java 异常处理

- **检查性异常：**最具代表的检查性异常是用户错误或问题引起的异常，这是程序员无法预见的。例如要打开一个不存在文件时，一个异常就发生了，这些异常在编译时不能被简单地忽略。
- **运行时异常：** 运行时异常是可能被程序员避免的异常。与检查性异常相反，运行时异常可以在编译时被忽略。
- **错误：** 错误不是异常，而是脱离程序员控制的问题。错误在代码中通常被忽略。例如，当栈溢出时，一个错误就发生了，它们在编译也检查不到的。

### Exception 类的层次

所有的异常类是从 java.lang.Exception 类继承的子类。 

Exception 类是 Throwable 类的子类。除了Exception类外，Throwable还有一个子类Error 。 

Java 程序通常不捕获错误。错误一般发生在严重故障时，它们在Java程序处理的范畴之外。 

Error 用来指示运行时环境发生的错误。 

例如，JVM 内存溢出。一般地，程序不会从错误中恢复。 

异常类有两个主要的子类：IOException 类和 RuntimeException 类。

<div style="text-align: left"><img src="/image/JavaException.jpg" style="zoom:100%"></div>

### Java 内置异常类

非检查性异常:

ArithmeticException、ArrayIndexOutOfBoundsException、IndexOutOfBoundsException ......

检查性异常类:

ClassNotFoundException、CloneNotSupportedException、InterruptedException ......

###  异常方法 

  下面的列表是 Throwable 类的主要方法:

| **序号** | **方法及说明**                                               |
| -------- | ------------------------------------------------------------ |
| 1        | **public String getMessage()**  				<br>返回关于发生的异常的详细信息。这个消息在Throwable 类的构造函数中初始化了。 |
| 2        | **public Throwable getCause()**  				<br/>返回一个Throwable 对象代表异常原因。 |
| 3        | **public String toString()**  				<br/>使用getMessage()的结果返回类的串级名字。 |
| 4        | **public void printStackTrace()**  				<br/>打印toString()结果和栈层次到System.err，即错误输出流。 |
| 5        | **public StackTraceElement [] getStackTrace()**  				<br/>返回一个包含堆栈层次的数组。下标为0的元素代表栈顶，最后一个元素代表方法调用堆栈的栈底。 |
| 6        | **public Throwable fillInStackTrace()**  				<br/>用当前的调用栈层次填充Throwable 对象栈层次，添加到栈层次任何先前信息中。 |

### 捕获异常

使用 try 和 catch 关键字可以捕获异常。try/catch 代码块放在异常可能发生的地方。 

try/catch代码块中的代码称为保护代码，使用 try/catch 的语法如下： 

``` java
try
{
   // 程序代码
}catch(ExceptionName e1)
{
   //Catch 块
}
```

``` java
try{
   // 程序代码
}catch(异常类型1 异常的变量名1){
  // 程序代码
}catch(异常类型2 异常的变量名2){
  // 程序代码
}catch(异常类型2 异常的变量名2){
  // 程序代码
}
```

### throws/throw 关键字： 

如果一个方法没有捕获到一个检查性异常，那么该方法必须使用 throws 关键字来声明。throws 关键字放在方法签名的尾部。 

也可以使用 throw 关键字抛出一个异常，无论它是新实例化的还是刚捕获到的。 

下面方法的声明抛出一个 RemoteException 异常：

``` java
import java.io.*;
public class className
{
   public void withdraw(double amount) throws RemoteException,
                              InsufficientFundsException
   {
       // Method implementation
   }
   //Remainder of class definition
}
```

### finally关键字

finally 关键字用来创建在 try 代码块后面执行的代码块。 

无论是否发生异常，finally 代码块中的代码总会被执行。 

在 finally 代码块中，可以运行清理类型等收尾善后性质的语句。 

finally 代码块出现在 catch 代码块最后，语法如下：

```
try{
  // 程序代码
}catch(异常类型1 异常的变量名1){
  // 程序代码
}catch(异常类型2 异常的变量名2){
  // 程序代码
}finally{
  // 程序代码
}
```

注意下面事项：

- catch 不能独立于 try 存在。
- 在 try/catch 后面添加 finally 块并非强制性要求的。
- try 代码后不能既没 catch 块也没 finally 块。
- try, catch, finally 块之间不能添加任何代码。

### 声明自定义异常

在 Java 中你可以自定义异常。编写自己的异常类时需要记住下面的几点。

- 所有异常都必须是 Throwable 的子类。
- 如果希望写一个检查性异常类，则需要继承 Exception 类。
- 如果你想写一个运行时异常类，那么需要继承 RuntimeException 类。

```java
class MyException extends Exception{
}
```

```java
// 文件名InsufficientFundsException.java
import java.io.*;
 
//自定义异常类，继承Exception类
public class InsufficientFundsException extends Exception
{
  //此处的amount用来储存当出现异常（取出钱多于余额时）所缺乏的钱
  private double amount;
  public InsufficientFundsException(double amount)
  {
    this.amount = amount;
  } 
  public double getAmount()
  {
    return amount;
  }
}
```

﻿﻿**在java的异常类体系中,Error和RuntimeException是非检查型异常，其他的都是检查型异常**

**所有方法都可以在不声明throws的情况下抛出RuntimeException及其子类 **

**不可以在不声明的情况下抛出非RuntimeException**

### 通用异常

在Java中定义了两种类型的异常和错误。

- **JVM(Java虚拟机)** **异常：**由 JVM 抛出的异常或错误。例如：NullPointerException 类，ArrayIndexOutOfBoundsException 类，ClassCastException 类。
- **程序级异常：**由程序或者API程序抛出的异常。例如 IllegalArgumentException 类，IllegalStateException 类。