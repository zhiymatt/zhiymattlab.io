---
title: Java入门（5）
date: 2018-11-18 16:03:40.923 -0700
categories: [Java]
tags: [java]
---



Java 入门，part 5



<!--more-->



# Java 包(package)

### 包的作用

- 把功能相似或相关的类或接口组织在同一个包中，方便类的查找和使用。
- 如同文件夹一样，包也采用了**树形目录的存储方式**，可以避免名字冲突。 (也就是说，文件目录的结构对应到package的结构)
- 包也限定了访问权限，拥有包访问权限的类才能访问某个包中的类。

Java 使用包（package）这种机制是为了防止命名冲突，访问控制，提供搜索和定位类（class）、接口、枚举（enumerations）和注释（annotation）等。 

包语句的语法格式为：

``` java
package pkg1[．pkg2[．pkg3…]];
```

如果一个`Something.java`文件的内容：
``` java
package net.java.util;
public class Something{
   ...
}
```
那么它的路径应该是 `net/java/util/Something.java` 这样保存的。

如果一个源文件中没有使用包声明，那么其中的类，函数，枚举，注释等将被放在一个无名的包（unnamed package）中。

### import 关键字

``` java
import package1[.package2…].(classname|*);
```

### package 的目录结构 

类放在包中会有两种主要的结果：
- 包名成为类名的一部分，正如我们前面讨论的一样。
- 包名必须与相应的字节码所在的目录结构相吻合。

编译之后的 .class 文件应该和 .java 源文件一样，它们放置的目录应该跟包的名字对应起来。但是，并不要求 .class 文件的路径跟相应的 .java 的路径一样。你可以分开来安排源码和类的目录。

```
<path-one>\sources\com\runoob\test\Runoob.java
<path-two>\classes\com\runoob\test\Google.class
```

编译选项： `-d <目录>`，用于指定存放生成的类文件的位置 
```
javac -d . Something.java
```

类目录的绝对路径叫做 **class path**。设置在系统变量 **CLASSPATH** 中。编译器和 java 虚拟机通过将 package 名字加到 class path 后来构造 .class 文件的路径。

\<path- two\>\classes 是 class path，package 名字是 com.runoob.test,而编译器和  JVM 会在  \<path-two\>\classes\com\runoob\test 中找 .class 文件。

一个 class path 可能会包含好几个路径，多路径应该用分隔符分开。默认情况下，编译器和 JVM 查找当前目录。JAR 文件按包含 Java 平台相关的类，所以他们的目录默认放在了 class path 中。

### 设置 CLASSPATH 系统变量

用下面的命令显示当前的CLASSPATH变量：

- Windows 平台（DOS 命令行下）：C:\> set CLASSPATH
- UNIX 平台（Bourne shell 下）：# echo $CLASSPATH

删除当前CLASSPATH变量内容：

- Windows 平台（DOS 命令行下）：C:\> set CLASSPATH=
- UNIX 平台（Bourne shell 下）：# unset CLASSPATH; export CLASSPATH

设置CLASSPATH变量:

- Windows 平台（DOS 命令行下）： C:\> set CLASSPATH=C:\users\jack\java\classes
- UNIX 平台（Bourne shell 下）：# CLASSPATH=/home/jack/java/classes; export CLASSPATH