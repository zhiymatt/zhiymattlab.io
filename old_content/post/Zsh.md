---
title: Zsh
date: 2018-12-19 01:08:40.923 -0700
categories: [Shell]
tags: [shell, zsh]
---

Install Zsh @ Ubuntu (Windows Subsystem)

<!--more-->

1. 先更新系统，下载git
    ```shell
    sudo apt-get update
    sudo apt-get upgrade
    sudo apt-get install git
    ```
    
2. 安装zsh，检查有没有安装成功
    ```shell
    sudo apt-get install zsh
    cat /etc/shells
    ```

3. 安装oh-my-zsh
    ```shell
    sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
    ```

4. 更改默认terminal
    ```shell
    chsh -s /bin/zsh
    ```

5. 更改设置

    ```
    ZSH_THEME="ys"
    ```

    