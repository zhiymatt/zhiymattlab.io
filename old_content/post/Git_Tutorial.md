---
title: Git Tutorial
date: 2018-11-12 10:02:40.923 -0700
categories: [Git]
tags: [git]
---





Git 入门教程，for reference。



<!--more-->



# 参与开源项目的流程

1. 找到想参与的项目，按Fork在自己的账号下克隆一个仓库
2. 把自己账号下的仓库`clone`到本地
3. 修改完后，`push`到自己的仓库
4. 在Github上发起一个`pull request`，等待反馈



---



# 生成新的 SSH key pair

1. 生成 `ED25519 SSH key pair`

    ```
    ssh-keygen -t ed25519 -C "email@example.com"
    ```

    或者生成 `RSA SSH key pair`

    ```
    ssh-keygen -o -t rsa -b 4096 -C "email@example.com"
    ```

2. 复制新生成的 **public** `SSH key`，并添加到Github/Gitlab的账号设置里

3. 测试

    ```
    ssh -T git@gitlab.com
    ```



---



# 账号设置

```
git config --global user.name "Your Name"
git config --global user.email "email@example.com"
```

注意`git config`命令的`--global`参数，用了这个参数，表示你这台机器上所有的Git仓库都会使用这个配置，当然也可以对某个仓库指定不同的用户名和Email地址。



---



# 创建 repository

版本库 = 仓库 = repository

1. 创建空`repo`

    ```
    git init
    ```

2. 编辑

3. 把所有文件添加到仓库

    ```
    git add .
    ```

    或者只添加一个文件

    ```
    git add some_file
    ```

4. 把添加的文件提交到仓库

    ```
    git commit -m "some comments"
    ```



---



# 查看

1. 查看 `repo`当前的状态

    ```
    git status
    ```

2. 查看对某个文件做过什么未提交的修改

    ```
    git diff some_file
    ```

3. 查看历史记录

    ```
    git log
    ```

4. 查看之前输入的命令

    ```
    git reflog
    ```



---




# 时光机

`HEAD`表示当前版本，`HEAD^`表示上个版本，`HEAD^^`表示上上个版本，`HEAD~100`表示上100个版本。

1. 要回退到上一个版本：

    ```
    git reset --hard HEAD^
    ```

2. 再输入`git log`时，已经已经看不到HEAD之后的那个`commit`了，但如果能知道它的`commit id`的话，还可以回到那个版本：

    ```
    git reset --hard commit_id
    ```

    `commit id`是通过SHA1计算出来的、用十六进制表示的、非常大的数字。这里不一定要完整输入`commit id`，只输入前几位就可以了，`git`会自动匹配



### 工作区和缓存区

##### 工作区 (Working Directory)

在电脑里看到的目录

##### 版本库 (Repository)

隐藏的目录`.git`；

`repo` 里存了很多东西，包括称为 `stage`或者`index`的暂存区，还有Git自动创建的第一个分支`master`，以及指向`master`的`HEAD`指针。

第一步`git add`实际上是把文件修改添加到暂存区；

第二步`git commit`实际上是把暂存区的所有内容提交到当前分支。

<div style="text-align: left"><img src="/image/git_workingdir_repo.jpg" style="zoom:100%"></div>



### 管理修改

Git跟踪并管理的是修改，而非文件。

比如，

第一次修改 -> `git add` -> 第二次修改 -> `git commit`

Git管理的是修改，当用`git add`命令后，在工作区的第一次修改被放入暂存区，准备提交，但是，在工作区的第二次修改并没有放入暂存区，所以，`git commit`只负责把暂存区的修改提交了，也就是第一次的修改被提交了，第二次的修改不会被提交。

提交后，用`git diff HEAD -- readme.txt`命令可以查看工作区和版本库里面最新版本的区别。



### 撤销修改

##### 撤销工作区中的修改

撤销对工作区中`xxx`文件的修改：

```
git checkout -- xxx
```

这里有两种情况：

- 一种是`xxx`自修改后还没有被放到暂存区，现在，撤销修改就回到和版本库一模一样的状态；

- 一种是`xxx`已经添加到暂存区后，又作了修改，现在，撤销修改就回到添加到暂存区后的状态。

即，最近原则。

注意，这里的` -- `不能漏掉，否则就是“切换到xxx分支”的意思。

##### 撤销暂存区中的修改

已经`git add`把修改提交到暂存区：

```
git reset HEAD xxx
```

unstage可以把修改重新放回工作区

##### 撤销版本库中的修改

如果只是`commit`了，还没有`push`，可以在本地做版本回退



### 删除文件

```
git rm xxx
```

在工作区删除`xxx`文件之后，

- 可以`git rm`再`git commit`，把它从版本库里删除
- 可以`git checkout -- xxx`，恢复误删的文件



---



# 远程库

### 添加远程库

本地已创建了一个Git仓库，又想在GitHub创建一个Git仓库，并且让两者保持远程同步。

1. 在Github创建一个空的远程库

2. 在本地的Git仓库运行：

    ```
    git remote add origin git@github.com:username/repo_name.git
    ```

    添加后，远程库的名字就是origin。

3. 把本地库的内容推送到远程库上：

    ```
    git push -u origin master
    ```

    由于远程库是空的，第一次推送`master`分支时，加上了`-u`参数，Git不但会把本地的`master`分支内容推送的远程新的`master`分支，还会把本地的`master`分支和远程的`master`分支关联起来，在以后的推送或者拉取时就可以简化命令。

    ```
    -u, --set-upstream     set upstream for git pull/status
    ```

4. 之后，通过这个命令把本地库的修改提交到远程库

    ```
    git push origin master
    ```


### 从远程库克隆

```
git clone git@github.com:username/repo_name.git
```



---



# 分支管理



### 创建与合并分支

一条时间线就是一个分支，

在前面的例子里，`HEAD`指向的不是提交，而是指向`master`，`master`才是指向提交的。

每次`commit`，`master`分支都会向前移动一步，所以是一个这样的结构：

```
commit 1
commit 2
commit 3
commit 4 <-- master <-- HEAD
```

如果创建一个叫做`dev`的分支，Git就新建了一个指针叫做`dev`，和`master`指向相同的提交，再把`HEAD`指向`dev`，表示当前分支在`dev`上。之后，对工作区的修改和提交就是针对`dev`分支了。

如果只修改了`dev`，而`master`没有修改，想要把`dev`合并到`master`上，只需要把`master`指向`dev`的当前提交，就完成了合并。

合并完成后，可以删除`dev`指针，就相当于删除了`dev`分支。

实战：

1. 创建`dev`分支，并切换过去：

    ```
    git checkout -b dev
    ```

    `-b`参数的意思是创建并切换，相当于两条命令：

    ```
    git branch dev
    git checkout dev
    ```

2. 查看当前所在的分支：

    ```
    git branch
    ```

3. ......

4. 在`master`分支上的时候，把`dev`分支上的修改合并到`master`分支上：

    ```
    git merge dev
    ```

5. 删除dev分支

    ```
    git branch -d dev
    ```



### 解决冲突

如果`master`和`dev`分支上都有各自的提交，则不能做`Fast-forward`快速合并；如果两个提交修改了同一个文件，就会有`CONFLICT`，需要手动解决分支在进行merge。

```
$ git merge dev
blablabla提示有冲突
$ 手动修改有冲突的文件，保存
$ git add conflict_file
$ git commit -m "conflict fixed"
$ git branch -d dev
```

如果发现conflict之后不想merge了：

```
git merge --abort
```



### 分支管理策略

因为合并分支的时候，如果用了`Fast-forward`模式，就只是处理了指针，所以在这种模式下，删除分支后，会丢掉分支信息。

如果禁用`Fast-forward`模式，那么Git在`merge`的时候就会生成一个新的`commit`，从分支历史上就可以看出分支信息。

禁用`Fast-forward`的`merge`：

```
git merge --no-ff -m "merge with no-ff" dev
```

这里`merge`的时候，因为生成了一个新的`commit`，所以这里要加入comment。



### Bug分支

如果在`dev`分支上的工作还没做完，但是急着修复一个代号101的bug，所以要创建一个新的`issue-101`分支，这种情况下，可以用`stash`把当前的working dir临时保存下来，等以后恢复现场后继续工作：

```
git stash
```

处理好bug之后，回到`dev`分支。之后查看之前临时保存下来的工作现场：

```
git stash list
```

输出大概是这样：

```
stash@{0}: WIP on dev: f52c633 add merge
```

之后，有两种恢复方法：

- 恢复之后，stash的内容还暂时保留下来，之后，再根据需要，手动删除stash内容：

  ```
  git stash apply stash@{0}
  git stash drop stash@{0}
  ```

- 恢复的同时把stash内容也删掉：

  ```
  git stash pop
  ```



### Feature 分支

如果对这个新做好的`feature-test`分支上的新feature不满意，可以不merge。如果要删掉这个分支，也ok。

如果用`-d`参数，会提示`feature-test`还没有被merge：

```
$ git branch -d feature-test
error: The branch 'feature-test' is not fully merged.
```

可以用`-D`参数做强行删除：

```
git branch -D feature-test
```



### 多人协作 & 抓取分支

查看远程仓库的信息：

 ```
$ git remote
origin
 ```

推送到远程仓库的`dev`分支：

```
git push origin dev
```

`clone`的时候，只抓取了远程库上的`master`分支。如果需要在本地做`dev`分支的开发，可以在本地创建一个新`dev`分支，并把这个本地的`dev`分支和远程的`dev`分支关联起来：

```
git checkout -b dev origin/dev
```

之后，流程还是一样的。如果要`push`的话，

```
git push origin dev
```

但是，有可能其他人的最新提交和刚才试图提交的文件有冲突，于是就需要先把最新的提交`pull`下来，在本地合并，解决冲突，再重新`push`:

```
git pull
// 如果此时提示no tracking information
// 可能需要先执行本地dev分支与远程origin/dev分支的链接：
// git branch --set-upstream-to=origin/dev dev
// 之后再重新pull
手动解决合并冲突blablabla
git commit -m "fix env conflict"
git push origin dev
```



### Rebase

略



### 其他

添加submodule：

```
git submodule add https://github.com/databricks/Spark-The-Definitive-Guide.git  Spark-The-Definitive-Guide
```





---



参考了"https://www.liaoxuefeng.com/wiki/0013739516305929606dd18361248578c67b8067c8c017b000"