---
title: Linux Find
date: 2019-01-30 10:09:40.923 -0700
categories: [Shell]
tags: [shell, linux]
---

Find specific pattern in file name / content

<!--more-->

Find specific pattern in the content of files:

```sh
grep -rnw '/path/to/somewhere/' -e 'pattern'
```

Find specific pattern in the file-name:

```sh
find . -name testfile.txt
```

