---
title: Compile Hadoop Project
date: 2019-01-30 10:08:40.923 -0700
categories: [Hadoop]
tags: [hadoop]
---

<span></span>

<!--more-->

```bash
# compile
hadoop com.sun.tools.javac.Main WordCount.java
# pack into a jar file
hadoop jar wc.jar WordCount /user/joe/wordcount/input /user/joe/wordcount/output
# Applications can specify a comma separated list of paths which would be present in the current working directory of the task using the option -files. The -libjars option allows applications to add jars to the classpaths of the maps and reduces. The option -archives allows them to pass comma separated list of archives as arguments. These archives are unarchived and a link with name of the archive is created in the current working directory of tasks. More details about the command line options are available at Commands Guide.
hadoop jar hadoop-mapreduce-examples-<ver>.jar wordcount -files cachefile.txt -libjars mylib.jar -archives myarchive.zip input output
```