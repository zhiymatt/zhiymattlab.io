---
title: Sum-Product Networks
date: 2019-02-05 23:08:40.923 -0700
categories: [ML, Paper]
tags: [ml, paper]
---

SPNs are directed acyclic graphs with variables as leaves, sums and
products as internal nodes, and weighted edges.

<!--more-->

# Introduction

The key limiting factor in graphical model inference and learning is the
complexity of the partition function. We thus ask the question: what are
general conditions under which the partition function is tractable? The answer
leads to a new kind of deep architecture, which we call sum-product networks
(SPNs). SPNs are directed acyclic graphs with variables as leaves, sums and
products as internal nodes, and weighted edges. **We show that if an SPN is
complete and consistent it represents the partition function and all marginals
of some graphical model, and give semantics to its nodes**. Essentially all
tractable graphical models can be cast as SPNs, but SPNs are also strictly more
general. We then propose learning algorithms for SPNs, based on backpropagation
and EM. Experiments show that inference and learning with SPNs can be both
**faster** and **more accurate** than with standard deep networks. For example, SPNs
perform image completion better than state-of-the-art deep networks for this
task. SPNs also have intriguing potential connections to the architecture of
the cortex.

# Limitation

Sum Product Networks (SPNs) are a recently developed class of deep generative
models which compute their associated unnormalized density functions using a
special type of arithmetic circuit. When certain sufficient conditions, called
the decomposability and completeness conditions (or "D&C" conditions), are
imposed on the structure of these circuits, marginal densities and other useful
quantities, which are typically intractable for other deep generative models,
can be computed by what amounts to a single evaluation of the network (which is
a property known as "validity"). However, the effect that the D&C conditions
have on the capabilities of D&C SPNs is not well understood.
In this work we analyze the D&C conditions, expose the various connections
that D&C SPNs have with multilinear arithmetic circuits, and consider the
question of how well they can capture various distributions as a function of
their size and depth. **Among our various contributions is a result which
establishes the existence of a relatively simple distribution with fully
tractable marginal densities which cannot be efficiently captured by D&C SPNs
of any depth, but which can be efficiently captured by various other deep
generative models**. We also show that with each additional layer of depth
permitted, the set of distributions which can be efficiently captured by D&C
SPNs grows in size. This kind of "depth hierarchy" property has been widely
conjectured to hold for various deep models, but has never been proven for any
of them. Some of our other contributions include a new characterization of the
D&C conditions as sufficient and necessary ones for a slightly strengthened
notion of validity, and various state-machine characterizations of the types of
computations that can be performed efficiently by D&C SPNs.



# In Short 

- They are **not very powerful models when they have any tractability**. In 
    general the deep learning community prefers to work with very powerful 
    models even if they are intractable.

- The main advantage of SPNs is that, unlike graphical models, SPNs are 
    **more traceable over high treewidth models** without requiring approximate 
    inference. Furthermore, SPNs are shown to **capture uncertainty over their
    inputs in a convincing manner**, yielding robust anomaly detection.

# References

[Sum-Product Networks: A New Deep Architecture](https://arxiv.org/abs/1202.3732)

[On the Expressive Efficiency of Sum Product Networks](https://arxiv.org/abs/1411.7717)

[question: Sum-Product Nets](https://www.reddit.com/r/MachineLearning/comments/3v4ued/question_sumproduct_nets/cxli2vy/)

[Deep Learning for Anomaly Detection: A Survey](https://arxiv.org/abs/1901.03407)