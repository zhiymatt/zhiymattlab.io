---
title: Windows（Host）和Ubuntu（VM）共享文件夹
date: 2019-02-02 19:53:40.923 -0700
categories: [Windows, Linux]
tags: [windows, linux, virtualbox]
---

<span></span>

<!--more-->

1. 在Ubuntu虚拟机里安装VirtualBox的增强功能（设备 --> 安装增强功能...）

2. 在Windows Host里面创建文件夹，用于存放要共享的文件

3. VirtualBox里选择"设备 --> 共享文件夹 --> 共享文件夹.."

4. 在设置菜单里面，点击右侧的+号，添加共享文件夹

    - ”文件夹路径“选择刚才创建的文件夹
    - ”文件夹名称“默认
    - 勾选”固定挂载“，不要勾选”只读分配”和“自动挂载”

5. 在Ubuntu的虚拟机的终端里面，在“/mnt/”路径下创建一个文件夹，名字任意

6. 在Terminal里面进行挂载

    ```sh
    mount -t vboxsf dir_in_windows /mnt/dir_in_ubuntu
    ```

7. 文件 /etc/rc.local中（用root用户）追加如下命令，可以实现自动挂载
    ```sh
    mount -t vboxsf dir_in_windows /mnt/dir_in_ubuntu
    ```