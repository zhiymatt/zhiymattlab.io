---
title: Spark Tutorial (1)
date: 2018-12-27 20:08:40.923 -0700
categories: [Spark]
tags: [spark]
---

Spark: Installation, Interactive Analysis, Self-Contained Applications

<!--more-->

# Installation

Download from https://spark.apache.org/downloads.html.

Spark Release: 2.4.0 (Nov 02 2018)<br>
Package Type: Pre-built for Apache Hadoop 2.7 and later<br>

# Spark Archetype

Not Needed

https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22org.apache.spark%22

```
groupId: org.apache.spark
artifactId: spark-core_2.11
version: 2.4.0
```

# Interactive Analysis

```bash
./bin/pyspark

>>> textFile = spark.read.text("README.md")
>>> textFile.count()  # Number of rows in this DataFrame
>>> linesWithSpark = textFile.filter(textFile.value.contains("Spark"))
```

# Self-Contained Applications (Java)

#### Create Project
IntelliJ IDEA → new → Project → maven → (without archetype) → next → ...

#### pom.xml

- add dependencies: spark, spark-sql
- change maven's default JDK compiler version: maven-compiler-plugin[configuration]source/target=1.8. (otherwise "-source 1.5 中不支持 lambda 表达式 [ERROR]   (请使用 -source 8 或更高版本以启用 lambda 表达式)")

```xml
<project>
    <groupId>com.zhiymatt.spark</groupId>
    <artifactId>SimpleApp</artifactId>
    <modelVersion>4.0.0</modelVersion>
    <name>Simple App</name>
    <packaging>jar</packaging>
    <version>1.0</version>
    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.0</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                </configuration>
            </plugin>
        </plugins>
    </build>
    <dependencies>
        <dependency> <!-- Spark dependency -->
            <groupId>org.apache.spark</groupId>
            <artifactId>spark-sql_2.11</artifactId>
            <version>2.4.0</version>
        </dependency>
        <dependency>
            <groupId>org.apache.spark</groupId>
            <artifactId>spark-sql_2.11</artifactId>
            <version>2.4.0</version>
        </dependency>
    </dependencies>
</project>
```

# SimpleApp.java

```java
/* SimpleApp.java */
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.Dataset;

public class SimpleApp {
    public static void main(String[] args) {
        String logFile = "/opt/spark-2.4.0-bin-hadoop2.7/README.md"; // Should be some file on your system
        SparkSession spark = SparkSession.builder().appName("Simple Application").getOrCreate();
        Dataset<String> logData = spark.read().textFile(logFile).cache();

        long numAs = logData.filter(s -> s.contains("a")).count();
        long numBs = logData.filter(s -> s.contains("b")).count();

            System.out.println("Lines with a: " + numAs + ", lines with b: " + numBs);

        spark.stop();
    }
}
```

# Compile in bash

```bash
# compile
mvn clean package
# submit and run
/opt/spark-2.4.0-bin-hadoop2.7/bin/spark-submit --class "SimpleApp" --master local[4] target/SimpleApp-1.0.jar 
```

# Compile in IntelliJ IDEA

Project Structure → Project Setting → Artifacts → add → JAR → From modules with dependencies → Filled in **Main Class**, set **extract to the target JAR** → OK

Run/Debug Configurations → Configuration → VM Options: **-Dspark.master=local[4]**

Build → Build Artifacts

Run/Debug!

---

# References

[Spark Quick Start](https://spark.apache.org/docs/latest/quick-start.html)