---
title: Maven Tutorial (3)
date: 2018-12-22 22:08:40.923 -0700
categories: [Java]
tags: [java, maven]
---

Maven Tutorial (3)

<!--more-->

# Maven构建配置文件 

略

# Maven插件
Maven 是一个执行插件的框架，每一个任务实际上是由插件完成的。Maven 插件通常用于：

- 创建 jar 文件
- 创建 war 文件 
- 编译代码文件
- 进行代码单元测试
- 创建项目文档
- 创建项目报告

一个插件通常提供了一组目标，可使用以下语法来执行：

```bash
mvn [plugin-name]:[goal-name]
```

例如，一个 Java 项目可以使用 Maven 编译器插件来编译目标，通过运行以下命令编译

```bash
mvn compiler:compile
```

##### 插件类型

Maven 提供以下两种类型插件：

| 类型 | 描述 |
| ------ | ------ |
| 构建插件 | 在生成过程中执行，并在 pom.xml 中的<build/> 元素进行配置 |
| 报告插件 | 在网站生成期间执行，在 pom.xml 中的 <reporting/> 元素进行配置 |

以下是一些常见的插件列表：
| 插件 | 描述 |
| --- | --- |
| clean |	编译后的清理目标，删除目标目录 |
| compiler |	编译 Java 源文件 |
| surefile |	运行JUnit单元测试，创建测试报告 |
| jar |	从当前项目构建 JAR 文件 |
| war |	从当前项目构建 WAR 文件 |
| javadoc |	产生用于该项目的 Javadoc |
| antrun |	从构建所述的任何阶段运行一组 Ant 任务 |

我们使用 maven-antrun-plugin 插件在例子中来在控制台打印数据。创建一个 pom.xml 文件，内容如下： 

```bash
<project xmlns="http://maven.apache.org/POM/4.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
    http://maven.apache.org/xsd/maven-4.0.0.xsd">
<modelVersion>4.0.0</modelVersion>
<groupId>com.companyname.projectgroup</groupId>
<artifactId>project</artifactId>
<version>1.0</version>
<build>
<plugins>
   <plugin>
   <groupId>org.apache.maven.plugins</groupId>
   <artifactId>maven-antrun-plugin</artifactId>
   <version>1.1</version>
   <executions>
      <execution>
         <id>id.clean</id>
         <phase>clean</phase>
         <goals>
            <goal>run</goal>
         </goals>
         <configuration>
            <tasks>
               <echo>clean phase</echo>
            </tasks>
         </configuration>
      </execution>     
   </executions>
   </plugin>
</plugins>
</build>
</project>
```

接下来，打开命令控制台，并转到包含 pom.xml 的文件夹并执行以下命令 mvn 命令。

```bash
C:\MVN\project>mvn clean
```

Maven 将开始处理并显示清洁周期/阶段，如下图中输出：

```
[INFO] Scanning for projects...
[INFO] -----------------------------------------------------------------
[INFO] Building Unnamed - com.companyname.projectgroup:project:jar:1.0
[INFO]    task-segment: [post-clean]
[INFO] -----------------------------------------------------------------
[INFO] [clean:clean {execution: default-clean}]
[INFO] [antrun:run {execution: id.clean}]
[INFO] Executing tasks
     [echo] clean phase
[INFO] Executed tasks
[INFO] -----------------------------------------------------------------
[INFO] BUILD SUCCESSFUL
[INFO] -----------------------------------------------------------------
[INFO] Total time: < 1 second
[INFO] Finished at: Sat Jul 07 13:38:59 IST 2012
[INFO] Final Memory: 4M/44M
[INFO] -----------------------------------------------------------------
```

上面的例子说明了以下关键概念：

- 插件可在 pom.xml 使用的 plugin 元素来指定； 
- 每个插件可以有多个目标；
- 从插件应使用它的相位元素开始处理定义阶段。这里已经使用 clean 阶段；
- 可以通过将它们绑定到插件的目标来执行配置任务。这里已经绑定 echo 任务到 maven-antrun-plugin 的运行目标；  
- 就这样，Maven将处理其余部分。如果没有可用的本地存储库，它会下载这个插件；

# Maven创建Java项目

Maven使用 **archetype** 来创建项目。要创建一个简单的 Java 应用程序，我们使用 maven-archetype-quickstart 插件。

让我们打开命令控制台，进入到 C:\MVN 目录并执行以下命令 mvn 命令。 

```bash
mvn archetype:generate
-DgroupId=com.companyname.bank 
-DartifactId=consumerBanking 
-DarchetypeArtifactId=maven-archetype-quickstart 
-DinteractiveMode=false
```

Maven会开始处理，并建立完整的 Java应用程序项目结构。 

```
INFO] Scanning for projects...
[INFO] Searching repository for plugin with prefix: 'archetype'.
[INFO] -----------------------------------------------------------------
[INFO] Building Maven Default Project
[INFO]    task-segment: [archetype:generate] (aggregator-style)
[INFO] -----------------------------------------------------------------
[INFO] Preparing archetype:generate
[INFO] No goals needed for project - skipping
[INFO] [archetype:generate {execution: default-cli}]
[INFO] Generating project in Batch mode
[INFO] -----------------------------------------------------------------
[INFO] Using following parameters for creating project 
 from Old (1.x) Archetype: maven-archetype-quickstart:1.0
[INFO] -----------------------------------------------------------------
[INFO] Parameter: groupId, Value: com.companyname.bank
[INFO] Parameter: packageName, Value: com.companyname.bank
[INFO] Parameter: package, Value: com.companyname.bank
[INFO] Parameter: artifactId, Value: consumerBanking
[INFO] Parameter: basedir, Value: C:MVN
[INFO] Parameter: version, Value: 1.0-SNAPSHOT
[INFO] project created from Old (1.x) Archetype in dir: C:MVNconsumerBanking
[INFO] -----------------------------------------------------------------
[INFO] BUILD SUCCESSFUL
[INFO] -----------------------------------------------------------------
[INFO] Total time: 14 seconds
[INFO] Finished at: Tue Jul 10 15:38:58 IST 2012
[INFO] Final Memory: 21M/124M
[INFO] -----------------------------------------------------------------
```

现在进入到目录。将看到创建了一个 Java应用程序项目，并命名为 consumerBanking（如：artifactId 指定）。

 用上面的例子中，我们可以了解到以下关键概念
文件夹结构 	描述
consumerBanking 	包括 src 目录和 pom.xml
src/main/java 	包含封装结构下的 Java 代码的文件 (com/companyName/bank)
src/main/test 	包含封装结构下的文本Java 测试代码文件 (com/companyName/bank)
src/main/resources 	它包含图片/属性文件（在上面的例子中需要手动创建这个结构） 