---
title: Java Swing中的layout
date: 2018-11-21 23:08:40.923 -0700
categories: [Java]
tags: [java, swing]
---

边界布局（BorderLayout）、流式布局（FlowLayout）、网格布局（GridLayout）、盒子布局（BoxLayout）、空布局（null）、CardLayout（卡片布局）、GridBagLayout（网格包布局）【最强大】

<!--more-->

---

Java Swing有五种常用的layout：

- 边界布局（BorderLayout）

- 流式布局（FlowLayout）

- 网格布局（GridLayout）

- 盒子布局（BoxLayout）

- 空布局（null）

还有其他两种布局，分别是GridBagLayout（网格包布局）、CardLayout（卡片布局）

注意：JFrame和JDialog默认布局为BorderLayout，JPanel和Applet默认布局为FlowLayout

---

# BorderLayout

类似这样的效果：
<img src="/image/Java_BorderLayout.png" style="width:30%" class="leftalign">

这种布局管理器分为东、南、西、北、中心五个方位。北和南的组件可以在水平方向上拉伸；而东和西的组件可以在垂直方向上拉伸；中心的组件可同时在水平和垂直方向上同时拉伸，从而填充所有剩余空间。在使用BorderLayout的时候，如果容器的大小发生变化，其变化规律为:组件的相对位置不变，大小发生变化。例如容器变高了，则North、South 区域不变，West、Center、East区域变高；如果容器变宽了，West、East区域不变，North、Center、South区域变宽。不一定所有的区域都有组件，如果四周区域（West、East、North、South区域）没有组件，则由Center区域去补充，但是如果 Center区域没有组件，则保持空白。BorderLayout是RootPaneContainer(JInternalFrame、JDialog、JFrame、JWindow)的默认布局管理器。

```
import java.awt.*; 
　　　　public class buttonDir{ 
　　　　　public static void main(String args[]){ 
　　　　　　Frame f = new Frame("BorderLayout"); 
　　　　　　f.setLayout(new BorderLayout()); 
　　　　　　f.add("North", new Button("North"); 
　　　　　　//第一个参数表示把按钮添加到容器的North区域 
　　　　　　f.add("South", new Button("South"); 
　　　　　　//第一个参数表示把按钮添加到容器的South区域 
　　　　　　f.add("East", new Button("East");  
　　　　　　//第一个参数表示把按钮添加到容器的East区域 
　　　　　　f.add("West", new Button("West"); 
　　　　　　//第一个参数表示把按钮添加到容器的West区域 
　　　　　　f.add("Center", new Button("Center"); 
　　　　　　//第一个参数表示把按钮添加到容器的Center区域 
　　　　　　f.setSize(200,200); 
　　　　　　f.setVisible(true);  
　　　　　} 
　　　　}
```

---

# FlowLayout

该布局称为流式布局管理器，是从左到右，中间放置，一行放不下就换到另外一行。一行能放置多少组件取决于窗口的宽度。默认组件是居中对齐，可以通过FlowLayout(intalign)函数来指定对齐方式，默认情况下是居中（FlowLayout.CENTER）。FlowLayout为小应用程序(Applet)和面板(Panel)的默认布局管理器。其构造函数示例为:

``` java
    FlowLayout()  //生成一个默认的流式布局，组件在容器里居中，每个组件之间留下5个像素的距离。 
    FlowLayout(int alinment) //可以设定每行组件的对齐方式。
    FlowLayout(int alignment , int horz , int vert) //设定对齐方式并设定组件水平和垂直的距离。
```

当容器的大小发生变化时，用FlowLayout管理的组件会发生变化。其变化规律是:组件的大小不变，但是相对位置会发生变化。

---

# CardLayout 

这种布局管理器能够帮助用户处理两个以至更多的成员共享同一显示空间，它把容器分成许多层，每层的显示空间占据整个容器大小，但是每层只允许放置一个组件，当然每层都可以利用Panel来实现复杂的用户界面。CardLayout就象一副叠得整整齐齐的扑克牌一样，有54张牌，但是你只能看见最上面的一张牌，一张牌就相当于布局管理器中的一层。 所有的组件像卡片一样叠在一起，每时每刻都只能显示其中一张卡片。CardLayout常用到切换界面。例如，点击App的Menu之后或者某个Button之后，主界面会切换到另外一个界面，这个时候就需要CardLayout。其实现过程如下:

首先，定义面板，为个个面板设置不同的布局，并根据需要在每个面板中放置组件:

``` java
    panelOne.setLayout(new FlowLayout); 
    panelTwo.setLayout(new GridLayout(2,1));
```

再设置主面板:

``` java
    CardLayout card = new CardLayout(); 
    panelMain.setLayout(card); 
```

下一步将开始准备好的面板添加到主面板:

``` java
    panelMain.add("red panel",panelOne); 
    panelMain.add("blue panel",panelOne); 
```

add()方法带有两个参数，第一个为String类型用来表示面板标题，第二个为Panel对象名称。

完成以上步骤以后，必须给用户提供在卡片之间进行选择的方法。一个常用的方法是每张卡片都包含一个按钮。通常用来控制显示哪张面板。

actionListener被添加到按钮。actionPerformed()方法可定义显示哪张卡片:

``` java
    card.next(panelMain);           //下一个 
    card.previous(panelMain);       //前一个 
    card.first(panelMain);          //第一个 
    card.last(panelMain);           //最后一个
    card.show(panelMain,"red panel"); //特定面板  
```

---

# GridLayout

这种布局是网格式的布局，窗口改变的时候，组件的大小也会随之改变。**每个单元格的大小一样**，而且放置组件时，只能从左到右、由上到下的顺序填充，用户不能任意放置组件。如果改变大小， GridLayout将相应地改变每个网格的大小，以使各个网格尽可能地大，占据Container容器全部的空间。

用构造函数划分出网格的行数和列数，

``` java
    new GridLayout(行数，列数)；
```

构造函数里的行数和列数能够有一个为零，但是不能都为零。当容器里增加控件时候，**容器内将向0的那个方向增长**。例如，如果是如下语句：

``` java
	GridLayout layout= new GridLayout（0，1）;
    //在增加控件时，会保持一个列的情况下，不断把行数增长.
```

`java.awt.GridBagConstraints` 中的`insets(0,0,0,0)`的参数具体指的是：规定一个控件显示区的空白区。

如果控件显示的`inset`为`(10,5,20,0)`，那么控件到显示区北边距离为10，西边为5，南边为20，东边为0控件会比显示区小。

如果inset为负，控件会超出显示区，使容器中各个组件呈网格状布局，平均占据容器的空间。当所有组件大小相同时用此布局。其构造函数为: 

``` java
    GridLayout() 
    GridLayout(int row,int col) 
    GridLayout(int row,int col,int horz,int vert)  
```

---

# BoxLayout

BoxLayout布局能够允许将控件按照**X轴（从左到右）或者Y轴（从上到下）**方向来摆放，而且沿着主轴能够设置不同尺寸。 构造BoxLayout对象时，有两个参数，例如：

``` java
    Public BoxLayout（Container target，int axis）；
```

Target参数是表示当前管理的容器，axis是指哪个轴，有两个值：BoxLayout.X_AXIS和BoxLayout.Y_AXIS。

示例代码如下：

``` java
    JPanel  jpanel=new JPanel();
    Jpanel.setLayout(new BoxLayout(jpanel,BoxLayout.Y_AXIS);
    TextArea  testArea=new TextArea(4,20);
    JButton   button=new JButton(“this is a button”);
    jpanel.add(testArea);
    jpanel.add(button);
```

容纳testArea和button的容器，对他们沿Y轴（从上往下）放置，并且文本域和按纽左对齐，也就是两个控件的最左端在同一条线上:

``` java
    testArea.setAlignmentX(Component.LEFT_ALIGNMENT);
    button. setAlignmentX(Component.LEFT_ALIGNMENT);
```

容纳testArea和button的容器，对他们采用沿Y轴（从上往下）放置，并且文本域最左端和按纽的最右端在同一条线上:

``` java
    testArea.setAlignmentX(Component.LEFT_ALIGNMENT);
    button. setAlignmentX(Component.RIGHT_ALIGNMENT);
```

setAlignmentX（left，right）只有在布局是BoxLayout.Y_AXIS才效，而setAlignmentY（top，button）在布局为BoxLayout.X_AXIS才效果。

组件对齐一般来说：

- 所有top-to-bottom BoxLayout object 应该有相同的 X alignment。
- 所有left-to-right Boxlayout应该有相同的 Y alignment
- setAlignmentX 和setAlignmentY 可以实现对齐。

---

# GridBagLayout

可以完成**复杂**的布局，而且IDE对它有足够的支持，是一个**很强大**的Layout。不过它过于**复杂**，在此布局中，组件大小不必相同。可以采用以下代码容器获得一个GridBagLayout：

``` java
    GridBagLayout gb=new GridBagLayout(); 
    ContainerName.setLayout(gb);
```

要使用网格包布局，还必须有其一个辅助类，GridBagContraints。它包含GridBagLayout类用来定位及调整组件大小所需要的全部信息。使用步骤如下:

1. 创建网格包布局的一个实例,并将其定义为当前容器的布局管理器
2. 创建GridBagContraints的一个实例 
3. 为组件设置约束. 
4. 通过方法统治布局管理器有关组件及其约束等信息 
5. 将组件添加到容器
6.  对各个将被显示的组件重复以上步骤

GridBagContraints类的成员变量列表如下:

1. **gridx, gridy**

    - 指定组件放在哪个单元中。其值应该设为常数CridBagConstraints.RELATIVE 。然后按标准顺序将组件加入网格包布局。从左到右，从上到下。 

2. **gridwidth, gridheight**

	- 指定组件将占用几行几列

3. **weightx, weighty**
	
	- 指定在一个GridBagLayout中应如何分配空间。缺省为0。

4. **ipadx, ipady**

	- 指定组件的最小宽度和高度。可确保组件不会过分收缩。 

5. **fill**

	- 指定在单元大于组件的情况下，组件如何填充此单元，缺省为组件大小不变，以下为静态数据成员列表，它们是fill变量的值。
		* GridBagConstraints.NONE     不改变组件大小 
		* GridBagConstraints.HORIZONTAL   增加组件宽度，使其水平填充显示区域 
		* GridBagConstraints.VERTICAL     增加组件高度，使其垂直填充显示区域 
		* GridBagConstraints.BOTH         使组件填充整个显示区域 

6. **anchor**
  - 如果不打算填充可以通过anchor指定将组件放置在单元中的位置，缺省为将其放在单元的中部。可使用以下静态成员: 
  	* GridBagConstraints.CENTER 
  	* GridBagConstraints.NORTH 
  	* GridBagConstraints.EAST 
  	* GridBagConstraints.WEST 
  	* GridBagConstraints.SOUTH 
  	* GridBagConstraints.NORTHEAST 
  	* GridBagConstraints.SOUTHEAST 
  	* GridBagConstraints.NORTHWEST 
  	* GridBagConstraints.SOUTHWEST 
  - 使用setConstraints()方法可以设置各组件约束。

GridBagLayout是是在GridLayout的基础上发展起来的，是五种布局策略中使用**最复杂，功能最强大**的一种，它是在GridLayout的基础上发展起来的。因为GridBagLayout中每个网格都相同大小并且强制组件与网格大小相同，使得容器中的每个组件也都是相同的大小，显得很不自然，而且组件假如容器中必须按照固定的行列顺序，不够灵活。在GridBagLayout中，可以**为每个组件指定其包含的网格个数**，组件可以保留原来的大小，可以以任意顺序随意地加入容器的任意位置，从而实现真正自由地**安排容器中每个组件的大小和位置**。