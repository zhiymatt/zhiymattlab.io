---
title: Large-Scale Study of Curiosity-Driven Learning
date: 2019-02-09 23:17:40.923 -0700
categories: [ML, RL, Paper]
tags: [ml, rl, paper, curiosity]
---

arXiv:1808.04355

<!--more-->

# Abstract

Reinforcement learning algorithms rely on carefully engineering environment rewards that are extrinsic to the agent. However, annotating each environment with hand-designed, dense rewards is not scalable, motivating the need for developing reward functions that are intrinsic to the agent.  Curiosity is a type of intrinsic reward function which uses prediction error as reward signal. In this paper: (a) We perform the first large-scale study of purely curiosity-driven learning, i.e. without any extrinsic rewards,  across54standard benchmark environments,  including the Atari game suite.  Our results show surprisingly good performance, and a high degree of alignment between the intrinsic curiosity objective and the hand-designed extrinsic rewards of many game environments.  (b) We investigate the effect of using different feature spaces for computing prediction error and show that random features are sufficient for many popular RL game benchmarks, but learned features appear to generalize better (e.g.  to novel game levels in Super Mario Bros.). (c) We demonstrate limitations of the prediction-based rewards in stochastic setups.

# Paper & Note

<object data="/pdf/Large-Scale Study of Curiosity-Driven Learning.pdf" type="application/pdf" width="800" height="1000">
    <p>It appears you don't have a PDF plugin for this browser.
    No biggie... you can <a href="test.pdf">click here to
    download the PDF file.</a></p>
</object>

# Game-play Videos and Code

[Large-Scale_Curiosity](https://pathak22.github.io/large-scale-curiosity/)

# References

[Large-Scale Study of Curiosity-Driven Learning](https://arxiv.org/abs/1808.04355)

