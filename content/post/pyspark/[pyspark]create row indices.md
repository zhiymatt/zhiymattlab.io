---
title: "[PySpark] Create Row Indices for DataFrame"
date: 2019-05-14 23:28:40.923 -0700
categories: [Spark]
tags: []
---

Create row indices for a DataFrame in PySpark.

<!--more-->

### Create monotonically Increasing Row Indices

- Not necessarily continuous.
- Can run in parallel.

```python
from pyspark.sql.functions import monotonicallyIncreasingId
res = df.withColumn("id", monotonicallyIncreasingId())
```

### Create Continuous Increasing Row Indices

- Index starts from 1.
- Run much slower.

```python
# Sort and create index for each row. Index from 1
w = Window.orderBy("Donor ID", "Donation Received Date")
sorted_donors_with_k_donations_df = donors_with_k_donations_df. \
                                    withColumn("Index", F.row_number().over(w))
```