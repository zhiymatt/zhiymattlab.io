---
title: "[PySpark] Notes about PySpark Variable Broadcast"
date: 2019-05-14 23:39:40.923 -0700
categories: [Spark]
tags: []
---

- Problems with UDF
- Size Limit

<!--more-->

### Problems with UDF

- UDF should be put in global environment, otherwise error "It appears that you
are attempting to reference SparkContext from a broadcast" may appear.

- Don't pass a broadcasted variable as a UDF argument, just reference it from
the function using "bc_var.values" inside the function body.

- Don't use **self.bc_var** in lambda map function.

### Size Limit

There is a limit on the size of the broadcasted variable in Spark.

Spark use a scala array to store the broadcast elements, since the max Integer
of Scala is 2*10^9, so the total string bytes is 2*2*10^9 = 4GB.