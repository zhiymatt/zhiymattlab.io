---
title: "[PySpark] UDF"
date: 2019-05-14 23:15:40.923 -0700
categories: [Spark]
tags: []
---

A more compact way to create UDF in PySpark

<!--more-->

### Register with F.UDf

```python
def sparse_to_array(v):
  v = DenseVector(v)
  new_array = list([float(x) for x in v])
  return new_array

sparse_to_array_udf = F.udf(sparse_to_array, T.ArrayType(T.FloatType()))
```

### Use Annotation

```python
@udf(ArrayType(MapType(IntegerType(), FloatType())))
def sparse_to_map(v):
    zipped = zip(v.indices, v.values)
    return list([{int(x): float(y)} for (x, y) in zipped])
```
