---
title: "[PySpark] Iterate DataFrame"
date: 2019-05-14 22:46:40.923 -0700
categories: [Spark]
tags: []
---

Iterate through rows of a Spark DataFrame in Python.

<!--more-->

### Convert to RDD --> map

```python
# Convert a Spark MLib SparseVector to Python Map in each row of the dataframe
def sparse_to_map(v):
    zipped = zip(v.indices, v.values)
    res = list([Counter({int(x): float(y) for (x, y) in zipped})])
    return res

sp = data.select("a", "b", "c").rdd \
         .map(lambda x: (x[0], (sparse_to_map(x[1]), x[2]))) \
         .reduceByKey(lambda x, y: ([x[0][0] + y[0][0]], x[1])) \
         .collect()
# If need to convert back to DataFrame, could use ".toDF(previous df.columns)"
```

### UDF + .withColumn

```python
def sparse_to_array(v):
  v = DenseVector(v)
  new_array = list([float(x) for x in v])
  return new_array

sparse_to_array_udf = F.udf(sparse_to_array, T.ArrayType(T.FloatType()))

df = df.withColumn('features_array', sparse_to_array_udf('features'))
```