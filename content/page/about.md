---
title: About me
subtitle: 
comments: false
---

Incoming Machine Learning Engineer @ Alibaba;

Former ML Intern @ Tencent;

Former Research Intern @ USTC.LINKE;

M.S. CS @ USC;

B.S. CS @ USTC.