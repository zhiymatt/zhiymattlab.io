<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <link href="//gmpg.org/xfn/11" rel="profile">
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta name="generator" content="Hugo 0.49.2" />

  
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>Denoising Criterion for Variational Auto-Encoder Framework &middot; zhiymatt Blog</title>

  
  <link type="text/css" rel="stylesheet" href="https://zhiymatt.gitlab.io/css/print.css" media="print">
  <link type="text/css" rel="stylesheet" href="https://zhiymatt.gitlab.io/css/poole.css">
  <link type="text/css" rel="stylesheet" href="https://zhiymatt.gitlab.io/css/syntax.css">
  <link type="text/css" rel="stylesheet" href="https://zhiymatt.gitlab.io/css/hyde.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abril+Fatface|PT+Sans:400,400i,700">


  
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="https://zhiymatt.gitlab.io/apple-touch-icon-144-precomposed.png">
  <link rel="shortcut icon" href="https://zhiymatt.gitlab.io/favicon.png">

  
  <link href="" rel="alternate" type="application/rss+xml" title="zhiymatt Blog" />

  <style>

    img.leftalign
    {
        display: inline;
        vertical-align: top;
        float: none;
    }

    
    code.has-jax {
        font: inherit;
        font-size: 100%;
        background: inherit;
        border: inherit;
        color: #515151;
    }
    
</style>


<script type="text/javascript"
        async
        src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
MathJax.Hub.Config({
  tex2jax: {
    inlineMath: [['$','$'], ['\\(','\\)']],
    displayMath: [['$$','$$'], ['\[\[','\]\]']],
    processEscapes: true,
    processEnvironments: true,
    skipTags: ['script', 'noscript', 'style', 'textarea', 'pre'],
    TeX: { equationNumbers: { autoNumber: "AMS" },
         extensions: ["AMSmath.js", "AMSsymbols.js"] }
  }
});

MathJax.Hub.Queue(function() {
    var all = MathJax.Hub.getAllJax(), i;
    for(i = 0; i < all.length; i += 1) {
        all[i].SourceElement().parentNode.className += ' has-jax';
    }
});
</script>

</head>

  <body class=" ">
  <aside class="sidebar">
  <div class="container sidebar-sticky">
    <div class="sidebar-about">
      <a href="https://zhiymatt.gitlab.io/"><h1>zhiymatt Blog</h1></a>
      <p class="lead">
      
      </p>
    </div>

    <nav>
      <ul class="sidebar-nav">
        <li><a href="https://zhiymatt.gitlab.io/">Home</a> </li>
        <li><a href=""> Blog </a></li><li><a href="https://zhiymatt.gitlab.io/page/about/"> About </a></li><li><a href="https://zhiymatt.gitlab.io/categories"> Categories </a></li><li><a href="https://zhiymatt.gitlab.io/tags"> Tags </a></li>
      </ul>
    </nav>

    <p>&copy; 2019. All rights reserved. </p>
  </div>
</aside>

    <main class="content container">
    <div class="post">
  <h1>Denoising Criterion for Variational Auto-Encoder Framework</h1>
  <time datetime=2019-02-01T20:13:40-0700 class="post-date">Fri, Feb 1, 2019</time>
  <p>arXiv:1511.06406</p>

<p></p>

<hr />

<h1 id="abstract">Abstract</h1>

<p>Denoising autoencoders (DAE) are trained to reconstruct their clean inputs with noise injected at the input level, while variational autoencoders (VAE) are trained with noise injected in their stochastic hidden layer, with a regularizer that encourages this noise injection. In this paper, we show that injecting noise both in input and in the stochastic hidden layer can be advantageous and we propose a modified variational lower bound as an improved objective function in this setup. When input is corrupted, then the standard VAE lower bound involves marginalizing the encoder conditional distribution over the input noise, which makes the training criterion intractable. Instead, we propose a modified training criterion which corresponds to a tractable bound when input is corrupted. Experimentally, we find that the proposed denoising variational autoencoder (DVAE) yields better average log-likelihood than the VAE and the importance weighted autoencoder on the MNIST and Frey Face datasets.</p>

<hr />

<h1 id="introduction">Introduction</h1>

<p>Variational inference has been a core component of approximate Bayesian inference along with the Markov chain Monte Carlo (MCMC) method. It has been popular to many researchers and practitioners because <strong>the problem of learning an intractable posterior distribution is formulated as an optimization problem which has many advantages compared to MCMC</strong>:</p>

<ul>
<li>can take advantage of many advanced optimization tools</li>
<li>the training by optimization is usually faster than the MCMC sampling</li>
<li>unlike MCMC where it is difficult to decide when to finish the sampling, the stopping criterion in variational inference is clear.</li>
</ul>

<p>VAE:</p>

<ul>
<li>flexible enough to <strong>accurately model the true posterior distribution</strong></li>
<li>each dimension of the latent variable is assumed to be independent each other (i.e., there are factorized) and modeled by a <strong>univariate Gaussian distribution</strong> whose parameters are obtained by a nonlinear projection of the input using a neural network</li>
</ul>

<p>Denoising criterion:</p>

<ul>
<li>the input is corrupted by adding some noise and the model is asked to recover the original input.</li>
<li>plays an important role in achieving good generalization performance</li>
</ul>

<hr />

<h1 id="background">Background</h1>

<p><strong>Variational inference</strong> is an approximate inference method where the goal is to approximate the intractable posterior distribution <code>$p(z|x)$</code>, by a tractable approximate distribution <code>$q_\phi(z)$</code>. <code>$x$</code> is the observation and <code>$z \in R^D$</code> is the model parameters or latent variables.</p>

<p>To keep it tractable, the approximate distributions are limited to a restricted family of distributions <code>$q_\phi\in Q$</code> parameterized by variational parameters <code>$\phi$</code>. For example, in the mean-field variational inference, the distributions in <code>$Q$</code> treat all dependent variables as independent, i.e., <code>$q(z) = \prod q_d(z_d)$</code>.</p>

<p>The basic idea of obtaining the optimal approximate distribution <code>$q_{\phi^*} \in Q$</code> is to find the variational parameter <code>$\phi^*$</code> that minimizes the KL divergence between the approximate distribution and the target distribution.</p>

<p>The KL divergence itself involves the intractable target distribution, instead of directly minimizing the KL divergence, we can bypass it by decomposing the marginal log-likelihood as follows:</p>

<p><code>$$\log p(x) = E_{q_\phi(z)}[\log \frac{p(x, z)}{q_\phi(z)}] + KL(q_\phi(z) || p(z|x)).​$$</code></p>

<p>Because the marginal log-likelihood <code>$\log p(x)$</code> is independent of the variational distribution <code>$q_\phi$</code> (which means  <code>$\log p(x)$</code>  is fixed when we only change <code>$q_\phi$</code>) and the KL term is non-negative, <strong>instead of minimizing the KL divergence, we can maximize the first term</strong>, called the variational lower bound, which is the same as minimizing the KL divergence term.</p>

<h3 id="variational-autoencoders">Variational Autoencoders</h3>

<p>With the VAE, the posterior distribution is defined as <code>$p_\theta(z|x) \propto  p_\theta (x|z) p(z)$</code>.</p>

<p>We use a parameterized distribution to define the observation model <code>$p_\theta(x|z)$</code>. A typical choice for the parameterized distribution is to use a neural network where the input is <code>$z$</code> and output a parametric distribution over <code>$x$</code>, such as the Gaussian or Bernoulli, depending on the data type.</p>

<ul>
<li><p>Generative network &ndash;&gt; <code>$p_\theta (x|z)$</code></p></li>

<li><p>Inference network, recognition network &ndash;&gt; <code>$p_\phi (z|x)$</code></p></li>
</ul>

<div>
$$
\begin{align}
\log p_\theta (x) &\ge E_{q_\phi} (z|x) [\log \frac{p_\theta (x, z)}{q_\phi (z|x)}] \\
&= E_{q_\phi} (z|x) [\log p_\theta (x|z)]  - KL(q_\phi (z|x) || p(z))
\end{align}
$$
</div>

<p>Note that in the above Eqn., we can interpret the first term as a reconstruction accuracy through an autoencoder with noise (because <code>$q_\phi$</code> is an approximation) injected in the hidden layer that is the output of the inference network, and the second term as a regularizer which enforces the approximate posterior to be close to the prior and maximizes the entropy of the injected noise.</p>

<p>The earlier approaches to train this type of model were based on the variational EMGalgorithm. However, with the VAE it is possible to apply the backpropagation on the variational parameter <code>$\phi$</code> by using the re-parameterization trick, considering <code>$z$</code> as a function of i.i.d. noise and of the output of the encoder.</p>

<hr />

<h1 id="denoising-criterion-in-variational-framework">Denoising Criterion in Variational Framework</h1>

<p>With the denoising autoencoder criterion, the input is corrupted according to some noise distribution, and the model needs to learn to reconstruct the original input or maximize the log-probability of the clean input <code>$x$</code>, given the corrupted input <code>$\tilde{x}$</code></p>

<p><strong>Proposition 1.</strong> Let <code>$q_\phi (z|\tilde{x})$</code> be a Gaussian distribution such that <code>$q_\phi (z|\tilde{x}) = N(z|\mu_\phi (\tilde{x}), \sigma_\phi (\tilde{x}))$</code> where <code>$\mu_\phi(\tilde{x})$</code> and <code>$\sigma_\phi (\tilde{x})$</code> are non-linear functions of <code>$\tilde{x}$</code>. Let <code>$p(\tilde{x}|x)$</code> be a known corruption distribution around <code>$x$</code>. Then,
<code>$$E_{p(\tilde{x}|x)} [q_\phi (z|\tilde{x})] = \int_{\tilde{x}} q_\phi (z|\tilde{x}) p(\tilde{x}|x) d\tilde{x}$$</code>
is a mixture of Gaussian.</p>

<p>Depending on whether the distribution is over a continuous or discrete variables, the integral in the above Equation can be replaced by a summation. It is instructive to consider the distribution over discrete domain to see that Equation has a form of mixture of Gaussian.</p>

<p>We can see this corruption procedure as adding a stochastic layer to the bottom (early stage of the neural nets) of the inference network. For example, we can define a corruption network <code>$p_\pi(\tilde{x}|x)$</code> which is a neural network where the input is <code>$x$</code> and the output is stochastic units (e.g., Gaussian or Bernoulli distributions). Then, it is also possible to learn the parameter <code>$\pi$</code> of the corruption network by backpropagation using the reparameterization trick.</p>

<pre><code class="language-python"># The (almost) only difference between vanilla VAE and denoising VAE is that 
# we need to add noise to the input of the denoising VAE encoder in training time

# Add noise to X		
X_noise = X + noise_factor * tf.random_normal(tf.shape(X))		
X_noise = tf.clip_by_value(X_noise, 0., 1.)		
z_mu, z_logvar = Q(X_noise)

# There no explicit parameter \pi in the implementation. The parameters of the decoder imply param \pi
</code></pre>

<h3 id="the-denoising-variational-lower-bound">The Denoising Variational Lower Bound</h3>

<p>Integrating the denoising criterion into the variational auto-encoding framework is equivalent to having  a stochastic layer at the bottom of the inference network.</p>

<p>Then estimating the variational lower bound becomes intractable because <code>$E_{p(\tilde{x}|x)}[q_\phi (z|\tilde{x})]$</code> requires integrating out the noise <code>$\tilde{x}$</code> for a corruption distribution (cuz we don&rsquo;t know the distribution of <code>$x$</code>).</p>

<p><strong>Lemma 1.</strong> <em>Consider an approximate posterior distribution of the following form:</em></p>

<div>$$q_\Phi (z|x) = \int_{z'} q_\varphi (z|z') q_\psi (z'|x) dz'$$</div>

<p><em>here, we use <code>$\Phi = \{\varphi, \psi\}$</code>. Then, given <code>$p_\theta (x,z) = p_\theta (x|z) p(z)$</code>, we obtain the following inequality:</em></p>

<div>$$\log p_\theta(x) \ge E_{q_\Phi} (z|x) [\log \frac{p_\theta (x, z)}{q_\varphi (z|z')}] \ge E_{q_\Phi} (z|x) [\log \frac{p_\theta (x, z)}{q_\Phi (z|x)}] $$</div>

<p>Note that <code>$q_\psi (z'|x)$</code> can be either parametric or non-parametric distribution.</p>

<p><strong>Prove</strong>:</p>

<p><img src="https://zhiymatt.gitlab.io/image/DVAE_lemma_0.png" style="width:100%" class="leftalign">
<img src="https://zhiymatt.gitlab.io/image/DVAE_lemma_1.png" style="width:100%" class="leftalign"></p>

<p><strong>Jensen&rsquo;s Inequality</strong>: <em>Measure-theoretic and probabilistic form</em></p>

<p><em>Let <code>$(\Omega, A, \mu)$</code> be a probability space, such that <code>$\mu(\Omega) = 1$</code>. If <code>$g$</code> is a real-valued function that is <code>$\mu-integrable$</code>, and if <code>$\varphi$</code> is a convex function on the real line, then:</em></p>

<div>$$\phi(\int_\Omega g\ d\mu) \le \int_\Omega \phi \circ g\ d\mu$$</div>

<p><strong>Theorem 1.</strong> <em>Consider an approximate posterior distribution of the following form</em></p>

<div>$$q_\Phi (z|x) = \int_{z^1 \dots z^{L-1}} q_{\phi^L} (z|z^{L-1})\dots q_{\phi^1} (z^1|x)\ dz^1 \dots dz^{L-1}$$</div>

<p><em>Then, given <code>$p_\theta (x, z) = p_\theta (x|z) p(z)$</code>, we obtain the following inequality:</em></p>

<div>$$\log p_\theta (x) \ge E_{q_\Phi (z|x)} [\log \frac{p_\theta (x, z)}{\prod^{L-1}_{i=1} q_{\phi^i} (z^{i+1} |z^i)}] \ge E_{q_\Phi (z|x)} [\log \frac{p_\phi (x, z)}{q_{\Phi} (z |z)}]$$</div>

<p><em>where <code>$z=z^L$</code> and <code>$x = z^1$</code>.</em></p>

<p>Theorem 1 illustrates that <strong>adding more stochastic layers gives tighter lower bound</strong>.</p>

<p>We now use Lemma 1 to derive the <em>denoising variational lower bound</em>. For the approximate distribution <code>$\tilde{q}_\phi (z|x) = \int q_\phi (z|\tilde{x}) p(\tilde{x}|x) d\tilde{x}$</code>, we can write the standard variational lower bound as follows:</p>

<div>$$\log p_\theta (x) \ge E_{\tilde{q}_\phi (z|x)} [\log \frac{p_\theta (x, z)}{q_\phi (z|\tilde{x})}] \stackrel{def}{=} \cal{L}_{dvae} \ge E_{\tilde{q}_\phi (z|x)} [\log \frac{p_\theta (x, z)}{\tilde{q}_\phi (z|x)}] \stackrel{def}{=} \cal{L}_{cvae}$$
</div>
Note that the `$p_\theta(x, z)$` in the numerator of the above equation is a function of `$x$` not `$\tilde{x}$`. **That is given corrupted input `$\tilde{x}$` (in the denominator), the `$\cal{L}_{dvae}$` objective tries to reconstruct  the original input `$x$` not the corrupted input `$\tilde{x}$`**. This denoising criterion is different from the popular data augmentation approach where the model tries to reconstruct the corrupted input.

<div>$$\log p_\theta (x) \ge \cal{L}_{dvae} \ge \cal{L}_{cvae}$$</div>

<p>Note that the above does not necessarily mean that <code>$\cal{L}_{dvae} \ge \cal{L}_{vae}$</code> where <code>$\cal{L}_{vae}$</code> is the lower bound of VAE with Gaussian distribution in the inference network. This is because <code>$\tilde{q}_\theta (z|x)$</code> depends on a corruption distribution while <code>$q_\theta(z|x)$</code> does not. <code>$\cal{L}_{dvae}$</code> can be a tighter lower bound or looser lower bound that <code>$\cal{L}_{vae}$</code> (depend on the chosen corruption distribution).</p>

<p>Note also the <code>$\tilde{q}_\theta (z|x)​$</code> has the capacity to cover a much broader class of distributions than <code>$q_\theta (z|x)$</code>.</p>

<p>For <code>$\cal{L}_{dvae}$</code>, it is important to choose a sensible corruption distribution.</p>

<p><strong>Proposition 2.</strong> *Maximizing <code>$\cal{L}_{dvae}$</code> is equivalent to minimizing the following objective</p>

<div>$$E_{p(\tilde{x}|x)} [ KL(\tilde{q}_\phi (z|\tilde{x}) || p(z|x))].$$</div>

<p><em>In other words, <code>$\log p_\theta (x) = \cal{L}_{dvae} + E_{p(\tilde{x}|x)} [ KL(\tilde{q}_\phi (z|\tilde{x}) || p(z|x))]$</code>.</em></p>

<p>This illustrates that maximizing <code>$\cal{L}_{dvae}$</code> is equivalent to minimizing the expectation of the KL between the true posterior distribution and approximate posterior distribution <em>over all noised inputs</em> from <code>$p(\tilde{x}|x)$</code>. <strong>Resulting in a more robust training of the inference network to unseen data points</strong>.</p>

<h3 id="training-procedure">Training Procedure</h3>

<p>The training procedure of DVAE can be seen as a special case of optimizing the following objective which can be easily approximated be Monte Carlo sampling.</p>

<div>$$\cal_{dvae} = E_{q(z|\tilde{x})} E_{p(\tilde{x}|x)} [log \frac{p_\theta (x,z)}{q_\theta (z|\tilde{x})}]  \approx \frac{1}{MK} \sum_{m=1}^M \sum_{k=1}^K \log \frac{p_\theta (x, z^{(k|m)})}{q_\theta (z^{(k|m)} |\tilde{x}^{(m)})}$$</div>

<p>Where <code>$\tilde{x}^{(m)} \sim p(\tilde{x}|x)$</code> and <code>$z^{(k|m)} \sim q_\theta (z|\tilde{x}^{(m)})$</code>. <code>$M$</code> is sample size for each data point <code>$x$</code> in the training set. <code>$K$</code> is the sample size of samples <code>$z$</code>.</p>

<h1 id="notes">Notes</h1>

<p>The model is more sensitive to the <em>level</em> of the noise rather than the type.</p>

<p>The choice of sample sizes <code>$M$</code>: increasing the sample size helps to converge faster in terms of the number of epochs and converge ot better log-likelihood. However,  increasing sample size requires more computation. Thus, in practice using <code>$M=1$</code> seems a reasonable choice.</p>

<h1 id="conclusions">Conclusions</h1>

<p>The main result of the paper was to introduce the denoising variational lower bound which, provided a sensible corruption function, can be tighter than the standard variational lower bound on noisy inputs. &ldquo;We&rdquo; claimed that <strong>this training criterion makes it possible to learn more flexible and robust approximate posterior distributions such as the misture of Gaussian than the standard training method without corruption</strong> (For example, suppose that <code>$p(z|x)$</code> consists of multiple modes. Then, <code>$\tilde(q)_\theta (z|x$</code> has the potential of modeling more than a single mode, whhereas it is impossible to model multiple modes of <code>$p(z|x)$</code> from <code>$q_\theta(z|x)$</code> regardless of which lower bound of <code>$\log p_\theta (x)$</code> is used as the objective function) (<em>Refer to Proposition 1.</em>)</p>

<hr />

<h1 id="references">References</h1>

<p><a href="https://arxiv.org/abs/1511.06406">Denoising Criterion for Variational Auto-Encoding Framework</a></p>
</div>


    </main>

    
  </body>
</html>
